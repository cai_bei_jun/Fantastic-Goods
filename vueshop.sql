/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : vueshop

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2020-05-24 10:28:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_ad
-- ----------------------------
DROP TABLE IF EXISTS `app_ad`;
CREATE TABLE `app_ad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of app_ad
-- ----------------------------
INSERT INTO `app_ad` VALUES ('18', '1', '图片2', '1484285334.jpg');
INSERT INTO `app_ad` VALUES ('17', '1', '图片1', '1484285302.jpg');

-- ----------------------------
-- Table structure for app_address
-- ----------------------------
DROP TABLE IF EXISTS `app_address`;
CREATE TABLE `app_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `cellphone` varchar(15) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `area` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1310 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_address
-- ----------------------------
INSERT INTO `app_address` VALUES ('1309', '593299496', '11', '13998989832', '北京市', '北京市', '22', '0', '东城区');
INSERT INTO `app_address` VALUES ('2', '593299496', '小王', '13546576676', '河北省', '石家庄市', '12', '0', '长安区');
INSERT INTO `app_address` VALUES ('1', '593299496', '张三', '13912239330', '上海市', '上海市', '1112', '0', '黄浦区');

-- ----------------------------
-- Table structure for app_admin
-- ----------------------------
DROP TABLE IF EXISTS `app_admin`;
CREATE TABLE `app_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of app_admin
-- ----------------------------
INSERT INTO `app_admin` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- ----------------------------
-- Table structure for app_ad_group
-- ----------------------------
DROP TABLE IF EXISTS `app_ad_group`;
CREATE TABLE `app_ad_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of app_ad_group
-- ----------------------------
INSERT INTO `app_ad_group` VALUES ('1', '主图切换');
INSERT INTO `app_ad_group` VALUES ('2', '移动');

-- ----------------------------
-- Table structure for app_attr
-- ----------------------------
DROP TABLE IF EXISTS `app_attr`;
CREATE TABLE `app_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '自己的父id',
  `name` varchar(255) DEFAULT NULL COMMENT '属性和值',
  `ftype` char(8) DEFAULT NULL COMMENT '表单类型',
  `cid` int(11) DEFAULT NULL COMMENT '栏目id',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `pid` (`pid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1087 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of app_attr
-- ----------------------------
INSERT INTO `app_attr` VALUES ('948', '0', '品牌', 'select', '496', '0');
INSERT INTO `app_attr` VALUES ('949', '948', 'ONLY', 'checkbox', '496', '0');
INSERT INTO `app_attr` VALUES ('950', '948', '韩都衣舍', 'checkbox', '496', '0');
INSERT INTO `app_attr` VALUES ('953', '0', '裙长', 'select', '496', '0');
INSERT INTO `app_attr` VALUES ('954', '953', '中长款', 'select', '496', '0');
INSERT INTO `app_attr` VALUES ('955', '953', '中裙', 'select', '496', '0');
INSERT INTO `app_attr` VALUES ('956', '953', '短裙', 'select', '496', '0');
INSERT INTO `app_attr` VALUES ('957', '953', '长裙', 'select', '496', '0');
INSERT INTO `app_attr` VALUES ('958', '0', '组合形式', 'checkbox', '496', '0');
INSERT INTO `app_attr` VALUES ('959', '958', '单件', 'checkbox', '496', '0');
INSERT INTO `app_attr` VALUES ('960', '958', '两件套', 'checkbox', '496', '0');
INSERT INTO `app_attr` VALUES ('961', '958', '三件套', 'checkbox', '496', '0');
INSERT INTO `app_attr` VALUES ('962', '958', '超短裙', 'checkbox', '496', '0');
INSERT INTO `app_attr` VALUES ('963', '958', 'ZARA', 'checkbox', '496', '0');
INSERT INTO `app_attr` VALUES ('964', '0', '品牌', 'select', '497', '0');
INSERT INTO `app_attr` VALUES ('965', '964', '美动态', 'select', '497', '0');
INSERT INTO `app_attr` VALUES ('966', '964', '恒源祥', 'select', '497', '0');
INSERT INTO `app_attr` VALUES ('967', '0', '选购热点', 'select', '497', '0');
INSERT INTO `app_attr` VALUES ('968', '967', '加肥加大', 'select', '497', '0');
INSERT INTO `app_attr` VALUES ('969', '967', '藏肉', 'select', '497', '0');
INSERT INTO `app_attr` VALUES ('970', '967', '修身显瘦', 'select', '497', '0');
INSERT INTO `app_attr` VALUES ('971', '0', '品牌', 'select', '498', '0');
INSERT INTO `app_attr` VALUES ('972', '971', '李宁', 'select', '498', '0');
INSERT INTO `app_attr` VALUES ('973', '971', '阿迪达斯', 'select', '498', '0');
INSERT INTO `app_attr` VALUES ('974', '0', '品牌', 'select', '499', '0');
INSERT INTO `app_attr` VALUES ('975', '974', '波司登', 'select', '499', '0');
INSERT INTO `app_attr` VALUES ('976', '974', 'adidas', 'select', '499', '0');
INSERT INTO `app_attr` VALUES ('977', '0', '衣长', 'select', '499', '0');
INSERT INTO `app_attr` VALUES ('978', '977', '中长款', 'select', '499', '0');
INSERT INTO `app_attr` VALUES ('979', '977', '常规', 'select', '499', '0');
INSERT INTO `app_attr` VALUES ('980', '977', '长款', 'select', '499', '0');
INSERT INTO `app_attr` VALUES ('981', '977', '短款', 'select', '499', '0');
INSERT INTO `app_attr` VALUES ('982', '977', '超短', 'select', '499', '0');
INSERT INTO `app_attr` VALUES ('983', '0', '品牌', 'select', '500', '0');
INSERT INTO `app_attr` VALUES ('984', '983', '波司登', 'select', '500', '0');
INSERT INTO `app_attr` VALUES ('985', '983', '森马', 'select', '500', '0');
INSERT INTO `app_attr` VALUES ('986', '0', '款式', 'select', '500', '0');
INSERT INTO `app_attr` VALUES ('987', '986', '修身小脚', 'select', '500', '0');
INSERT INTO `app_attr` VALUES ('988', '986', '合体直筒', 'select', '500', '0');
INSERT INTO `app_attr` VALUES ('989', '986', '铅笔裤', 'select', '500', '0');
INSERT INTO `app_attr` VALUES ('990', '986', '宽松直筒', 'select', '500', '0');
INSERT INTO `app_attr` VALUES ('991', '986', '直筒', 'select', '500', '0');
INSERT INTO `app_attr` VALUES ('992', '0', '品牌', 'select', '501', '0');
INSERT INTO `app_attr` VALUES ('993', '992', '苹果', 'select', '501', '0');
INSERT INTO `app_attr` VALUES ('994', '992', '戴尔', 'select', '501', '0');
INSERT INTO `app_attr` VALUES ('995', '992', 'TinkPad', 'select', '501', '0');
INSERT INTO `app_attr` VALUES ('996', '0', '屏幕尺寸', 'select', '501', '0');
INSERT INTO `app_attr` VALUES ('997', '996', '15.6英寸', 'select', '501', '0');
INSERT INTO `app_attr` VALUES ('998', '996', '13英寸', 'select', '501', '0');
INSERT INTO `app_attr` VALUES ('999', '996', '14英寸', 'select', '501', '0');
INSERT INTO `app_attr` VALUES ('1006', '0', '颜色', 'texts', '496', '1');
INSERT INTO `app_attr` VALUES ('1007', '0', '尺寸', 'texts', '496', '1');
INSERT INTO `app_attr` VALUES ('1009', '0', '颜色', 'texts', '501', '1');
INSERT INTO `app_attr` VALUES ('1010', '0', '屏幕尺寸', 'texts', '501', '1');
INSERT INTO `app_attr` VALUES ('1011', '0', '颜色', 'texts', '500', '1');
INSERT INTO `app_attr` VALUES ('1012', '0', '腰围', 'texts', '500', '1');
INSERT INTO `app_attr` VALUES ('1013', '0', '颜色', 'texts', '497', '1');
INSERT INTO `app_attr` VALUES ('1014', '0', '尺码', 'texts', '497', '1');
INSERT INTO `app_attr` VALUES ('1015', '0', '颜色', 'texts', '498', '1');
INSERT INTO `app_attr` VALUES ('1016', '0', '尺寸', 'texts', '498', '1');
INSERT INTO `app_attr` VALUES ('1017', '0', '颜色', 'texts', '499', '1');
INSERT INTO `app_attr` VALUES ('1018', '0', '尺寸', 'texts', '499', '1');
INSERT INTO `app_attr` VALUES ('1019', '0', '颜色', 'texts', '502', '1');
INSERT INTO `app_attr` VALUES ('1020', '964', '筱岚', 'select', '497', '0');
INSERT INTO `app_attr` VALUES ('1021', '964', '娇茹妮', 'select', '497', '0');
INSERT INTO `app_attr` VALUES ('1022', '964', '香后', 'select', '497', '0');
INSERT INTO `app_attr` VALUES ('1023', '971', '烟花烫', 'select', '498', '0');
INSERT INTO `app_attr` VALUES ('1043', '1041', '白色', 'select', '525', '0');
INSERT INTO `app_attr` VALUES ('1026', '0', '尺码', 'select', '524', '0');
INSERT INTO `app_attr` VALUES ('1027', '1026', '38', 'select', '524', '0');
INSERT INTO `app_attr` VALUES ('1028', '1026', '37', 'select', '524', '0');
INSERT INTO `app_attr` VALUES ('1029', '1026', '36', 'select', '524', '0');
INSERT INTO `app_attr` VALUES ('1030', '1026', '35', 'select', '524', '0');
INSERT INTO `app_attr` VALUES ('1042', '1041', '黑色', 'select', '525', '0');
INSERT INTO `app_attr` VALUES ('1041', '0', '颜色', 'select', '525', '1');
INSERT INTO `app_attr` VALUES ('1034', '0', '颜色', 'select', '523', '1');
INSERT INTO `app_attr` VALUES ('1035', '1034', '红色', 'select', '523', '0');
INSERT INTO `app_attr` VALUES ('1036', '1034', '黑色', 'select', '523', '0');
INSERT INTO `app_attr` VALUES ('1037', '0', '尺码', 'select', '523', '1');
INSERT INTO `app_attr` VALUES ('1038', '1037', '36', 'select', '523', '0');
INSERT INTO `app_attr` VALUES ('1039', '1037', '37', 'select', '523', '0');
INSERT INTO `app_attr` VALUES ('1040', '1037', '38', 'select', '523', '0');
INSERT INTO `app_attr` VALUES ('1044', '1041', '紫色', 'select', '525', '0');
INSERT INTO `app_attr` VALUES ('1045', '0', '尺码', 'select', '525', '1');
INSERT INTO `app_attr` VALUES ('1046', '1045', 'S', 'select', '525', '0');
INSERT INTO `app_attr` VALUES ('1047', '1045', 'M', 'select', '525', '0');
INSERT INTO `app_attr` VALUES ('1048', '1045', 'L', 'select', '525', '0');
INSERT INTO `app_attr` VALUES ('1049', '1045', 'XL', 'select', '525', '0');
INSERT INTO `app_attr` VALUES ('1050', '1045', 'XXL', 'select', '525', '0');
INSERT INTO `app_attr` VALUES ('1051', '0', '颜色', 'select', '526', '1');
INSERT INTO `app_attr` VALUES ('1052', '1051', '蓝色', 'select', '526', '0');
INSERT INTO `app_attr` VALUES ('1053', '1051', '黑色', 'select', '526', '0');
INSERT INTO `app_attr` VALUES ('1054', '1051', '白色', 'select', '526', '0');
INSERT INTO `app_attr` VALUES ('1055', '0', '尺码', 'select', '526', '1');
INSERT INTO `app_attr` VALUES ('1056', '1055', '28', 'select', '526', '0');
INSERT INTO `app_attr` VALUES ('1057', '1055', '29', 'select', '526', '0');
INSERT INTO `app_attr` VALUES ('1058', '1055', '30', 'select', '526', '0');
INSERT INTO `app_attr` VALUES ('1059', '1055', '31', 'select', '526', '0');
INSERT INTO `app_attr` VALUES ('1060', '0', '颜色', 'select', '527', '1');
INSERT INTO `app_attr` VALUES ('1061', '1060', '玫瑰金', 'select', '527', '0');
INSERT INTO `app_attr` VALUES ('1062', '1060', '银色', 'select', '527', '0');
INSERT INTO `app_attr` VALUES ('1063', '1060', '黄色', 'select', '527', '0');
INSERT INTO `app_attr` VALUES ('1068', '0', '网络类型', 'texts', '504', '1');
INSERT INTO `app_attr` VALUES ('1067', '0', '版本', 'texts', '504', '1');
INSERT INTO `app_attr` VALUES ('1069', '0', '机身颜色', 'texts', '504', '1');
INSERT INTO `app_attr` VALUES ('1070', '0', '地区', 'select', '504', '0');
INSERT INTO `app_attr` VALUES ('1071', '1070', '北京', 'select', '504', '0');
INSERT INTO `app_attr` VALUES ('1072', '1070', '上海', 'select', '504', '0');
INSERT INTO `app_attr` VALUES ('1073', '1070', '广州', 'select', '504', '0');
INSERT INTO `app_attr` VALUES ('1074', '0', '品牌', 'checkbox', '504', '0');
INSERT INTO `app_attr` VALUES ('1075', '1074', 'vivo', 'checkbox', '504', '0');
INSERT INTO `app_attr` VALUES ('1076', '1074', '小米', 'checkbox', '504', '0');

-- ----------------------------
-- Table structure for app_columns
-- ----------------------------
DROP TABLE IF EXISTS `app_columns`;
CREATE TABLE `app_columns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `c_names` varchar(255) DEFAULT NULL,
  `num` bigint(15) DEFAULT NULL,
  `parentpath` varchar(255) DEFAULT '',
  `image` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=538 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of app_columns
-- ----------------------------
INSERT INTO `app_columns` VALUES ('492', '0', '潮流女装', '20170106122122', '|0|', '1484287695.png');
INSERT INTO `app_columns` VALUES ('493', '0', '品牌男装', '20170106122135', '|0|', '1484287842.png');
INSERT INTO `app_columns` VALUES ('494', '0', '电脑办公', '20170106122201', '|0|', '1484287985.png');
INSERT INTO `app_columns` VALUES ('495', '0', '手机数码', '20170106122212', '|0|', '1484288118.png');
INSERT INTO `app_columns` VALUES ('496', '492', '裙装', '20170106123553', '|0|,|492|', null);
INSERT INTO `app_columns` VALUES ('497', '492', '上装', '20170106123744', '|0|,|492|', null);
INSERT INTO `app_columns` VALUES ('498', '492', '下装', '20170106123752', '|0|,|492|', '');
INSERT INTO `app_columns` VALUES ('499', '493', '羽绒服', '20170106123854', '|0|,|493|', null);
INSERT INTO `app_columns` VALUES ('500', '493', '休闲裤', '20170106123914', '|0|,|493|', '');
INSERT INTO `app_columns` VALUES ('501', '494', '笔记本电脑', '20170106123931', '|0|,|494|', '');
INSERT INTO `app_columns` VALUES ('502', '494', '电脑配件', '20170106123946', '|0|,|494|', null);
INSERT INTO `app_columns` VALUES ('503', '495', '摄影摄像', '20170106124840', '|0|,|495|', '');
INSERT INTO `app_columns` VALUES ('504', '495', '手机', '20170106124900', '|0|,|495|', null);
INSERT INTO `app_columns` VALUES ('506', '0', '图书', '20170214103953', '|0|', null);
INSERT INTO `app_columns` VALUES ('507', '0', '家居家纺', '20170214104010', '|0|', null);
INSERT INTO `app_columns` VALUES ('510', '0', '食品生鲜', '20170214104112', '|0|', null);
INSERT INTO `app_columns` VALUES ('514', '0', '医药保健', '20170214104201', '|0|', null);
INSERT INTO `app_columns` VALUES ('518', '0', '玩具乐器', '20170214104522', '|0|', null);
INSERT INTO `app_columns` VALUES ('520', '0', '酒水饮料', '20170214104617', '|0|', null);
INSERT INTO `app_columns` VALUES ('521', '0', '化妆彩妆', '20170214104639', '|0|', null);
INSERT INTO `app_columns` VALUES ('522', '0', '钟表珠宝', '20200514153202', '|0|', null);
INSERT INTO `app_columns` VALUES ('523', '492', '潮流女鞋', '20180424152150', '|0|,|492|', null);
INSERT INTO `app_columns` VALUES ('525', '493', '短袖', '20180424155641', '|0|,|493|', null);
INSERT INTO `app_columns` VALUES ('526', '493', '牛仔裤', '20180424160234', '|0|,|493|', '');
INSERT INTO `app_columns` VALUES ('527', '494', '台式机', '20180424165703', '|0|,|494|', null);

-- ----------------------------
-- Table structure for app_fav
-- ----------------------------
DROP TABLE IF EXISTS `app_fav`;
CREATE TABLE `app_fav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(20) DEFAULT NULL,
  `gid` int(11) NOT NULL DEFAULT '0',
  `times` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1143 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of app_fav
-- ----------------------------
INSERT INTO `app_fav` VALUES ('1', '593299496', '286026274', '2020-05-15 16:12:44');

-- ----------------------------
-- Table structure for app_goods
-- ----------------------------
DROP TABLE IF EXISTS `app_goods`;
CREATE TABLE `app_goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `qid` int(11) NOT NULL DEFAULT '0',
  `parentid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT '0.00',
  `goodspara` varchar(255) DEFAULT NULL COMMENT '属性参数的值',
  `freight` float(11,2) NOT NULL DEFAULT '0.00',
  `bodys` longtext,
  `dates` varchar(20) DEFAULT NULL,
  `num` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`) USING BTREE,
  KEY `qid` (`qid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=171 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of app_goods
-- ----------------------------
INSERT INTO `app_goods` VALUES ('68', '286026274', '496', 'ONLY冬装新品雪纺拼接流苏腰带长款连衣裙女', '399.00', '|949|,|955|,|960|,|961|', '6.00', '裙装裙装1裙装裙装1裙装裙装1裙装裙装1裙装裙装1裙装裙装1裙装裙装1', '2020-05-10', '0');
INSERT INTO `app_goods` VALUES ('69', '124555205', '496', '韩都衣舍2016秋新款时尚拼接色宽松显瘦气质长款长袖连衣裙', '128.00', '|949|,|956|,|959|,|960|,|961|', '0.00', '韩都衣舍2016秋新款时尚拼接色宽松显瘦气质长款长袖连衣裙韩都衣舍2016秋新款时尚拼接色宽松显瘦气质长款长袖连衣裙韩都衣舍2016秋新款时尚拼接色宽松显瘦气质长款长袖连衣裙韩都衣舍2016秋新款时尚拼接色宽松显瘦气质长款长袖连衣裙韩都衣舍2016秋新款时尚拼接色宽松显瘦气质长款长袖连衣裙', '2020-05-10', '0');
INSERT INTO `app_goods` VALUES ('70', '704407997', '496', '韩都衣舍2017韩版女装春装新款木耳边卡通刺绣显瘦连衣裙', '118.00', '|949|,|955|,|960|,|961|,|962|,|963|', '9.00', '裙装裙装3裙<span style=\\\"color:#008000;\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf裙装裙装3裙<span style=\\\"color: rgb(0, 128, 0);\\\">装裙装3裙装裙装3</span>sdfsdfdsf', '2020-05-10', '0');
INSERT INTO `app_goods` VALUES ('73', '941801102', '497', '美动态胖妹妹春装打底裙2017新款加肥加大码女装胖mm显瘦连衣裙', '139.00', '|966|,|970|', '0.00', '美动态胖妹妹春装打底裙2017新款加肥加大码女装胖mm显瘦连衣裙美动态胖妹妹春装打底裙2017新款加肥加大码女装胖mm显瘦连衣裙美动态胖妹妹春装打底裙2017新款加肥加大码女装胖mm显瘦连衣裙美动态胖妹妹春装打底裙2017新款加肥加大码女装胖mm显瘦连衣裙美动态胖妹妹春装打底裙2017新款加肥加大码女装胖mm显瘦连衣裙美动态胖妹妹春装打底裙2017新款加肥加大码女装胖mm显瘦连衣裙美动态胖妹妹春装打底裙2017新款加肥加大码女装胖mm显瘦连衣裙美动态胖妹妹春装打底裙2017新款加肥加大码女装胖mm显瘦连衣裙', '2020-04-13', '0');
INSERT INTO `app_goods` VALUES ('74', '252173006', '497', '美动态胖妹妹秋冬2016新款大码女装200斤胖mm显瘦印花直筒连衣裙', '159.00', '|965|,|968|', '10.00', '美动态胖妹妹秋冬2016新款大码女装200斤胖mm显瘦印花直筒连衣裙美动态胖妹妹秋冬2016新款大码女装200斤胖mm显瘦印花直筒连衣裙美动态胖妹妹秋冬2016新款大码女装200斤胖mm显瘦印花直筒连衣裙美动态胖妹妹秋冬2016新款大码女装200斤胖mm显瘦印花直筒连衣裙美动态胖妹妹秋冬2016新款大码女装200斤胖mm显瘦印花直筒连衣裙美动态胖妹妹秋冬2016新款大码女装200斤胖mm显瘦印花直筒连衣裙美动态胖妹妹秋冬2016新款大码女装200斤胖mm显瘦印花直筒连衣裙美动态胖妹妹秋冬2016新款大码女装200斤胖mm显瘦印花直筒连衣裙', '2020-05-13', '0');
INSERT INTO `app_goods` VALUES ('75', '766946433', '498', '李宁运动裤男长裤 2016秋冬款篮球系列修身直筒卫裤运动套装下装', '109.00', '|972|', '0.00', '李宁运动裤男长裤 2016秋冬款篮球系列修身直筒卫裤运动套装下装李宁运动裤男长裤 2016秋冬款篮球系列修身直筒卫裤运动套装下装李宁运动裤男长裤 2016秋冬款篮球系列修身直筒卫裤运动套装下装李宁运动裤男长裤 2016秋冬款篮球系列修身直筒卫裤运动套装下装李宁运动裤男长裤 2016秋冬款篮球系列修身直筒卫裤运动套装下装李宁运动裤男长裤 2016秋冬款篮球系列修身直筒卫裤运动套装下装李宁运动裤男长裤 2016秋冬款篮球系列修身直筒卫裤运动套装下装李宁运动裤男长裤 2016秋冬款篮球系列修身直筒卫裤运动套装下装', '2020-04-13', '0');
INSERT INTO `app_goods` VALUES ('76', '534523517', '498', '国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装', '188.00', '|973|', '0.00', '国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装国家队全英赛比赛 正品李宁羽毛球女羽毛球裙 短裙 裙裤春夏下装', '2020-05-13', '0');
INSERT INTO `app_goods` VALUES ('77', '330520519', '498', '阿迪达斯2016秋季新款女子运动休闲下装针织短裤', '257.00', '|972|', '0.00', '阿迪达斯2016秋季新款女子运动休闲下装针织短裤阿迪达斯2016秋季新款女子运动休闲下装针织短裤阿迪达斯2016秋季新款女子运动休闲下装针织短裤阿迪达斯2016秋季新款女子运动休闲下装针织短裤阿迪达斯2016秋季新款女子运动休闲下装针织短裤阿迪达斯2016秋季新款女子运动休闲下装针织短裤阿迪达斯2016秋季新款女子运动休闲下装针织短裤阿迪达斯2016秋季新款女子运动休闲下装针织短裤阿迪达斯2016秋季新款女子运动休闲下装针织短裤阿迪达斯2016秋季新款女子运动休闲下装针织短裤', '2020-05-13', '0');
INSERT INTO `app_goods` VALUES ('78', '613094524', '499', 'Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服', '759.00', '|975|,|980|', '0.00', 'Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服Bosideng/波司登2016新款90%鹅绒男轻薄中长款连帽羽绒服', '2020-04-16', '0');
INSERT INTO `app_goods` VALUES ('79', '808830978', '499', '波司登2016新款加厚男士羽绒服中长款带帽保暖冬外套', '799.00', '|975|,|978|', '0.00', '波司登2016新款加厚男士羽绒服中长款带帽保暖冬外套波司登2016新款加厚男士羽绒服中长款带帽保暖冬外套波司登2016新款加厚男士羽绒服中长款带帽保暖冬外套波司登2016新款加厚男士羽绒服中长款带帽保暖冬外套波司登2016新款加厚男士羽绒服中长款带帽保暖冬外套波司登2016新款加厚男士羽绒服中长款带帽保暖冬外套波司登2016新款加厚男士羽绒服中长款带帽保暖冬外套', '2020-04-16', '0');
INSERT INTO `app_goods` VALUES ('80', '827013226', '499', '阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套', '798.00', '|975|,|979|', '0.00', '阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套阿迪达斯羽绒服男 2016冬季大码保暖运动服休闲连帽外套', '2020-04-20', '0');
INSERT INTO `app_goods` VALUES ('161', '454635074', '500', '男装棉麻休闲裤男夏季短裤男潮七分裤中裤宽松大码库子大裤衩日系', '69.00', '|984|,|987|', '0.00', '材质成分: 聚酯纤维45% 亚麻35% 棉20%货号: KZ3739销售渠道类型: 纯电商(只在线上销售)品牌: 始丽弹力: 微弹厚薄: 薄基础风格: 时尚都市面料: 平纹布<div>&nbsp;</div>', '2020-04-25', '0');
INSERT INTO `app_goods` VALUES ('162', '634071836', '500', '成人五分裤海边度假短裤男士潮流沙滩库子2018新款大裤衩夏装悠闲', '79.00', '', '0.00', '材质成分:&nbsp;棉100%货号:&nbsp;L632-L893销售渠道类型:&nbsp;纯电商(只在线上销售)品牌:&nbsp;TWN&nbsp;Towerfox/塔狐弹力:&nbsp;微弹厚薄:&nbsp;常规基础风格:&nbsp;青春流行&nbsp;', '2020-05-09', '0');
INSERT INTO `app_goods` VALUES ('163', '265947808', '525', '新款短袖男士夏季3d立体图案体恤猴子搞怪大猩猩个性t恤大码衣服', '26.00', '', '0.00', '<div>印花主题:&nbsp;3D效果厚薄:&nbsp;常规袖长:&nbsp;短袖领型:&nbsp;圆领颜色:&nbsp;深棕色&nbsp;猴子背心&nbsp;褐色&nbsp;口哨猴子T恤&nbsp;黑色&nbsp;大狗哈士奇&nbsp;姜黄色&nbsp;黄色中狗T恤&nbsp;黄色&nbsp;黄色小狗T恤&nbsp;白色&nbsp;白底金花T恤&nbsp;黑色金&nbsp;斑马条纹T恤&nbsp;天蓝色&nbsp;钻石太空T恤&nbsp;浅灰色&nbsp;刺绣美女T恤&nbsp;蒙眼女短袖&nbsp;掐屁股短袖&nbsp;紫发女短袖&nbsp;纹身男鄙视短袖&nbsp;狼头短袖&nbsp;红猪短袖&nbsp;单只哈士奇短袖&nbsp;惊讶表情短袖&nbsp;摸胸短袖&nbsp;白色&nbsp;乳白色&nbsp;银色&nbsp;灰色&nbsp;杏色&nbsp;浅棕色&nbsp;咖啡色&nbsp;香槟色&nbsp;柠檬黄&nbsp;驼色&nbsp;米白色&nbsp;栗色&nbsp;深棕色&nbsp;褐色&nbsp;黄色&nbsp;桔色&nbsp;浅灰色&nbsp;巧克力色&nbsp;深灰色&nbsp;深卡其布色尺码:&nbsp;4XL&nbsp;5XL码&nbsp;S&nbsp;M&nbsp;L&nbsp;XL&nbsp;2XL&nbsp;3XL款式细节:&nbsp;印花服饰工艺:&nbsp;免烫处理品牌:&nbsp;其它/other袖型:&nbsp;常规花型图案:&nbsp;动物图案版型:&nbsp;修身适用季节:&nbsp;夏季上市时间:&nbsp;2016年适用场景:&nbsp;其他休闲适用对象:&nbsp;青少年基础风格:&nbsp;青春流行细分风格:&nbsp;潮&nbsp;</div>', '2020-05-13', '0');
INSERT INTO `app_goods` VALUES ('84', '541081261', '501', 'Apple/苹果 MacBook Air 13.3英寸笔记本电脑 i5 8G 128G', '6988.00', '|995|,|998|', '0.00', 'Apple/苹果 MacBook Air 13.3英寸笔记本电脑 i5 8G 128GApple/苹果 MacBook Air 13.3英寸笔记本电脑 i5 8G 128GApple/苹果 MacBook Air 13.3英寸笔记本电脑 i5 8G 128GApple/苹果 MacBook Air 13.3英寸笔记本电脑 i5 8G 128GApple/苹果 MacBook Air 13.3英寸笔记本电脑 i5 8G 128GApple/苹果 MacBook Air 13.3英寸笔记本电脑 i5 8G 128GApple/苹果 MacBook Air 13.3英寸笔记本电脑 i5 8G 128G', '2020-05-09', '0');
INSERT INTO `app_goods` VALUES ('85', '219746948', '501', 'Apple/苹果 15 英寸：MacBook Pro Multi-Touch Bar 和 Touch ID 2.7GHz 处理器 512GB 存储容量', '21488.00', '|993|,|997|', '0.00', 'Apple/苹果 15 英寸：MacBook Pro Multi-Touch Bar 和 Touch ID 2.7GHz 处理器 512GB 存储容量Apple/苹果 15 英寸：MacBook Pro Multi-Touch Bar 和 Touch ID 2.7GHz 处理器 512GB 存储容量Apple/苹果 15 英寸：MacBook Pro Multi-Touch Bar 和 Touch ID 2.7GHz 处理器 512GB 存储容量Apple/苹果 15 英寸：MacBook Pro Multi-Touch Bar 和 Touch ID 2.7GHz 处理器 512GB 存储容量Apple/苹果 15 英寸：MacBook Pro Multi-Touch Bar 和 Touch ID 2.7GHz 处理器 512GB 存储容量Apple/苹果 15 英寸：MacBook Pro Multi-Touch Bar 和 Touch ID 2.7GHz 处理器 512GB 存储容量Apple/苹果 15 英寸：MacBook Pro Multi-Touch Bar 和 Touch ID 2.7GHz 处理器 512GB 存储容量', '2020-05-09', '0');
INSERT INTO `app_goods` VALUES ('86', '482059023', '501', 'Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售', '4199.00', '|994|,|997|', '0.00', 'Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售Dell/戴尔 灵越15(5567) Ins15-1545超轻薄学生i5游戏笔记本预售', '2020-05-09', '0');
INSERT INTO `app_goods` VALUES ('87', '452529780', '501', 'Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本', '6299.00', '|995|,|997|', '0.00', 'Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本Dell/戴尔 灵越15(7559) Ins15P-2749 15.6英寸i7独显游戏笔记本', '2020-05-09', '0');
INSERT INTO `app_goods` VALUES ('88', '224184070', '501', 'ThinkPad E460 E460 20ETA00DCD i5独显联想笔记本电脑商务办公', '4499.00', '|995|,|998|', '0.00', 'ThinkPad E460 E460 20ETA00DCD i5独显联想笔记本电脑商务办公ThinkPad E460 E460 20ETA00DCD i5独显联想笔记本电脑商务办公ThinkPad E460 E460 20ETA00DCD i5独显联想笔记本电脑商务办公ThinkPad E460 E460 20ETA00DCD i5独显联想笔记本电脑商务办公ThinkPad E460 E460 20ETA00DCD i5独显联想笔记本电脑商务办公ThinkPad E460 E460 20ETA00DCD i5独显联想笔记本电脑商务办公ThinkPad E460 E460 20ETA00DCD i5独显联想笔记本电脑商务办公ThinkPad E460 E460 20ETA00DCD i5独显联想笔记本电脑商务办公ThinkPad E460 E460 20ETA00DCD i5独显联想笔记本电脑商务办公', '2020-05-09', '0');
INSERT INTO `app_goods` VALUES ('89', '954387826', '502', '罗技G102炫彩有线游戏鼠标有线竞技电脑配件机械鼠标编程LOL包邮', '169.00', '', '0.00', '罗技G102炫彩有线游戏鼠标有线竞技电脑配件机械鼠标编程LOL包邮罗技G102炫彩有线游戏鼠标有线竞技电脑配件机械鼠标编程LOL包邮罗技G102炫彩有线游戏鼠标有线竞技电脑配件机械鼠标编程LOL包邮罗技G102炫彩有线游戏鼠标有线竞技电脑配件机械鼠标编程LOL包邮', '2020-05-09', '0');
INSERT INTO `app_goods` VALUES ('90', '774265843', '502', '微软Surface Pro3无线蓝牙鼠标超薄充电省电Pro4平板电脑配件book', '40.00', '', '0.00', '微软Surface Pro3无线蓝牙鼠标超薄充电省电Pro4平板电脑配件book微软Surface Pro3无线蓝牙鼠标超薄充电省电Pro4平板电脑配件book微软Surface Pro3无线蓝牙鼠标超薄充电省电Pro4平板电脑配件book微软Surface Pro3无线蓝牙鼠标超薄充电省电Pro4平板电脑配件book微软Surface Pro3无线蓝牙鼠标超薄充电省电Pro4平板电脑配件book微软Surface Pro3无线蓝牙鼠标超薄充电省电Pro4平板电脑配件book微软Surface Pro3无线蓝牙鼠标超薄充电省电Pro4平板电脑配件book', '2020-05-09', '0');
INSERT INTO `app_goods` VALUES ('110', '704909428', '496', '小黑裙套装', '150.00', '|949|,|954|,|960|', '0.00', '            ', '2020-04-16', '0');
INSERT INTO `app_goods` VALUES ('100', '512829844', '496', '连衣裙法式复古修身性感侧开叉雪纺小黑裙 ', '100.00', '', '10.00', '                <p>连衣裙法式复古修身性感侧开叉雪纺小黑裙</p><p><img src=\\\"http://localhost:8080/fantastic goods/server/uploadfiles/5ec8ba6c1cead.jpg\\\" alt=\\\"5ec8ba6c1cead.jpg\\\"><img src=\\\"http://localhost:8080/fantastic goods/server/uploadfiles/5ec8ba5b5ec8d.jpg\\\" alt=\\\"5ec8ba5b5ec8d.jpg\\\"></p>        ', '2020-04-16', '0');
INSERT INTO `app_goods` VALUES ('170', '646959794', '503', 'Canon/佳能 XC15 4K新概念专业高清摄像机 XC10升级版', '13840.00', '', '0.00', '<div class=\\\"tm-clear tb-hidden tm_brandAttr\\\" id=\\\"J_BrandAttr\\\" data-spm-anchor-id=\\\"a220o.1000855.0.i0.495667ad5zofpm\\\"><div class=\\\"name\\\"><em><img src=\\\"/fantastic goods/server./uploadfiles/5ec8c77c22a5e.png\\\" alt=\\\"5ec8c77c22a5e.png\\\">产品参数：</em></div></div><ul id=\\\"J_AttrUL\\\"><li title=\\\"Canon/佳能 XC15\\\">产品名称：Canon/佳能 XC15</li><li id=\\\"J_attrBrandName\\\" title=\\\"&nbsp;Canon/佳能\\\">品牌:&nbsp;Canon/佳能</li><li title=\\\"&nbsp;XC15\\\">型号:&nbsp;XC15</li><li title=\\\"&nbsp;闪存式DV\\\">存储介质:&nbsp;闪存式DV</li><li title=\\\"&nbsp;800万以上\\\" data-spm-anchor-id=\\\"a220o.1000855.0.i1.495667ad5zofpm\\\">数码像素:&nbsp;800万以上</li><li title=\\\"&nbsp;CMOS\\\">感光元件:&nbsp;CMOS</li><li title=\\\"&nbsp;3\\\">屏幕尺寸:&nbsp;3</li><li title=\\\"&nbsp;双重防抖\\\">防抖性能:&nbsp;双重防抖</li><li title=\\\"&nbsp;1.0英寸\\\">传感器尺寸:&nbsp;1.0英寸</li></ul>    ', '2020-05-23', '0');
INSERT INTO `app_goods` VALUES ('114', '427387635', '497', '筱岚2018春装新款纯棉刺绣休闲宽松大码女士体恤长袖T恤女', '79.00', '|1020|,|970|', '10.00', '<ul id=\\\"parameter-brand\\\"><li title=\\\"筱岚（XIAOLAN）\\\">品牌：&nbsp;筱岚（XIAOLAN）</li></ul><ul><li title=\\\"筱岚2018春装新款纯棉刺绣休闲宽松大码女士体恤长袖T恤女 绿色 155/80A/S\\\">商品名称：筱岚2018春装新款纯棉刺绣休闲宽松大码女士体恤长袖T恤女 绿色 155/80A/S</li><li title=\\\"16841845525\\\">商品编号：16841845525</li><li title=\\\"筱岚宾堡专卖店\\\">店铺：&nbsp;筱岚宾堡专卖店</li><li title=\\\"500.00g\\\">商品毛重：500.00g</li><li title=\\\"867\\\">货号：867</li><li title=\\\"S，M，L，XL，XXL\\\">尺码：S，M，L，XL，XXL</li><li title=\\\"休闲，韩版\\\">风格：休闲，韩版</li><li title=\\\"棉/丝光棉\\\">主要材质：棉/丝光棉</li><li title=\\\"圆领\\\">领型：圆领</li><li title=\\\"图案，刺绣\\\">流行元素：图案，刺绣</li><li title=\\\"多色系\\\">颜色：多色系</li><li title=\\\"长袖\\\">袖长：长袖</li><li title=\\\"常规袖\\\">袖型：常规袖</li><li title=\\\"25-29周岁\\\">适用年龄：25-29周岁</li><li title=\\\"经典，青春\\\">图案文化：经典，青春</li><li title=\\\"字母\\\">图案：字母</li><li title=\\\"常规款\\\">衣长：常规款</li><li title=\\\"宽松型\\\">版型：宽松型</li><li title=\\\"2017秋季\\\">上市时间：2017秋季</li></ul><img alt=\\\"\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t9160/182/1940857773/211137/f6709013/59c0c4d1Nb1e5a0dd.jpg\\\" style=\\\"width: 100%;\\\" /><br /><img alt=\\\"\\\" src=\\\"https://img30.360buyimg.com/popWareDetail/jfs/t9484/276/1861504440/171212/357f94e9/59c0c4edN538a395e.jpg\\\" style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif; margin: 0px; padding: 0px; border: 0px; vertical-align: middle; width: 100%;\\\" /><br /><img alt=\\\"\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t9103/132/1860238157/180148/c058d611/59c0c4eaNe8114e72.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif; width: 100%;\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t8161/203/1844775620/249524/baeaf275/59c0c4ebN0b9490a5.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif; width: 100%;\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t8140/238/1901321689/185167/e50289e9/59c0c4dbNa9dae745.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif; width: 100%;\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t8245/252/1865083614/165347/3cab2c5c/59c0c4efN10c377ee.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif; width: 100%;\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t8146/269/1887052289/174712/24564d20/59c0c4f0N2805cf2e.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif; width: 100%;\\\" />', '2020-04-15', '0');
INSERT INTO `app_goods` VALUES ('115', '207686760', '497', 'S-4XL纯棉短袖T恤女2018夏季新款女装上衣大码宽松字母印花半袖小衫', '79.00', '|1020|,|970|', '0.00', '商品名称：筱岚 S-4XL纯棉短袖T恤女2018夏季新款女装上衣大码宽松字母印花半袖小衫 红色 XL<br />商品编号：25853014944<br />店铺： 筱岚宾堡专卖店<br />商品毛重：350.00g<br />货号：nx5566<br />风格：甜美，OL通勤，简约<br />主要材质：棉/丝光棉<br />领型：圆领<br />流行元素：印花<br />袖长：短袖<br />袖型：常规袖<br />适用年龄：18-24周岁<br />图案：字母<br />版型：宽松型<br />衣长：常规款<br />上市时间：2018夏季<br /><br /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" id=\\\"1\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t15562/308/2244232596/364380/16878edd/5a9beceeNe4aba21a.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif; width: 100%;\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" id=\\\"1\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t19132/171/594158196/213182/507d4aae/5a9bed0dN1b12c26f.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;width:100%\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" id=\\\"1\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t15961/175/2273401920/688970/b3382fd0/5a9bed0dN160d1c02.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;width:100%\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" id=\\\"1\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t19594/227/650006136/642812/6d87bc2f/5a9bed0dNff1d2e0b.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;width:100%\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" id=\\\"1\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t15691/109/2305707724/334560/f96c348b/5a9bed2cN50cce189.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;width:100%\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" id=\\\"1\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t15619/113/2279772716/535637/65464627/5a9bed31N039ad22a.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;width:100%\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" id=\\\"1\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t18319/297/619508176/546166/95c9e93c/5a9bed3eN1f685e48.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;width:100%\\\" />', '2020-04-15', '0');
INSERT INTO `app_goods` VALUES ('116', '396797732', '497', '短袖t恤女夏装韩版2017新款学生宽松大码百搭绑带', '29.90', '|1021|,|969|', '0.00', '商品名称：娇茹妮 短袖t恤女夏装韩版2017新款学生宽松大码百搭绑带 中袖体恤女上衣 黑色 M 90-115斤<br />商品编号：11773225440<br />店铺： 正德弘服饰专营店<br />商品毛重：200.00g<br />商品产地：中国大陆<br />货号：079<br />尺码：M，L，XL，XXL<br />版型：宽松型<br />主要材质：棉/丝光棉<br />领型：圆领<br />流行元素：印花<br />风格：韩版<br />颜色：黑色系，白色系，粉色系<br />袖长：中袖<br />袖型：常规袖<br />适用年龄：18-24周岁<br />图案文化：青春<br />图案：字母<br />衣长：常规款<br />弹力：高弹<br />上市时间：2018夏季<br /><br /><p style=\\\"margin: 0px; padding: 0px; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\"><img align=\\\"absmiddle\\\" alt=\\\"\\\" size=\\\"748x589\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t4711/330/1181042015/102183/26e140b4/58d922a5N224fb080.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; width: 100%;\\\" /></p><p style=\\\"margin: 0px; padding: 0px; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\"><img align=\\\"absmiddle\\\" alt=\\\"\\\" size=\\\"750x761\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t5035/227/15856593/141958/52decc65/58d922a8Ncde70112.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; width: 100%;\\\" /><img align=\\\"absmiddle\\\" alt=\\\"\\\" size=\\\"750x349\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t4867/239/16600415/64987/7e597641/58d922a8Nfd301b66.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; width: 100%;\\\" /><img align=\\\"absmiddle\\\" alt=\\\"\\\" size=\\\"750x488\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t4834/227/17223520/89860/1d811d1b/58d922a8N77640c5f.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle;width: 100%;\\\" /><img align=\\\"absmiddle\\\" alt=\\\"\\\" size=\\\"750x748\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t4837/226/16688246/138210/324652c6/58d922a8Ncb65418a.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; width: 100%;\\\" /></p><p style=\\\"margin: 0px; padding: 0px; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\">&nbsp;</p><p style=\\\"margin: 0px; padding: 0px; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\">&nbsp;</p><p style=\\\"margin: 0px; padding: 0px; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\"><img align=\\\"absmiddle\\\" alt=\\\"\\\" size=\\\"750x773\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t4993/264/15361208/128688/bf798197/58d922a8N34299444.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; width: 100%;\\\" /></p><p style=\\\"margin: 0px; padding: 0px; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\">&nbsp;</p><p style=\\\"margin: 0px; padding: 0px; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\">&nbsp;</p><p style=\\\"margin: 0px; padding: 0px; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\"><img align=\\\"absmiddle\\\" alt=\\\"\\\" size=\\\"750x748\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t4861/225/15264682/116001/5b090306/58d922a8Ned514044.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; width: 100%;\\\" /></p><div>&nbsp;</div>', '2020-04-15', '0');
INSERT INTO `app_goods` VALUES ('117', '552370716', '497', '夏装新款韩版时尚织带字母印花七分袖破洞T恤女宽松百搭学生上衣', '29.90', '|1021|,|970|', '0.00', '商品名称：娇茹妮 夏装新款韩版时尚织带字母印花七分袖破洞T恤女宽松百搭学生上衣 红色 M<br />商品编号：12432215697<br />店铺： 正德弘服饰专营店<br />商品毛重：200.00g<br />商品产地：中国大陆<br />货号：788<br />尺码：XXL<br />版型：宽松型<br />主要材质：棉/丝光棉<br />领型：圆领<br />流行元素：印花<br />颜色：红色系<br />袖长：七分袖<br />袖型：常规袖<br />衣长：常规款<br />上市时间：2017夏季<br /><br /><br /><div><div><div><div>&nbsp;</div><div><table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" style=\\\"margin:0px;width:100%;\\\">\r\n<tbody>\r\n	<tr><td style=\\\"width: 20%;\\\"><img alt=\\\"\\\" size=\\\"198x159\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t5398/231/1752548429/7651/76dec3db/59137800N6d3584db.jpg\\\" /></td><td><table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" style=\\\"margin:0px;width:100%;\\\">\r\n	<tbody>\r\n		<tr bgcolor=\\\"#484848\\\"><td>尺码</td><td>胸围</td><td>袖长</td><td>衣长</td><td>肩宽</td></tr><tr bgcolor=\\\"#f7f7f7\\\"><td>M</td><td>92</td><td>20</td><td>58</td><td>46</td></tr><tr><td>L</td><td>96</td><td>21</td><td>60</td><td>48</td></tr><tr bgcolor=\\\"#f7f7f7\\\"><td>XL</td><td>100</td><td>22</td><td>62</td><td>50</td></tr><tr bgcolor=\\\"#f7f7f7\\\"><td>XXL</td><td>104</td><td>23</td><td>64</td><td><p>52</p></td></tr>\r\n	</tbody>\r\n	</table></td></tr>\r\n</tbody>\r\n</table></div></div></div><p style=\\\"margin: 0px; padding: 0px; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\">&nbsp;</p><p style=\\\"margin: 0px; padding: 0px; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif; text-align: center;\\\"><img align=\\\"absmiddle\\\" alt=\\\"\\\" size=\\\"750x750\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t5332/14/1683433768/248657/489344a6/59137801Na052ecdb.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; width: 100%;\\\" /><img align=\\\"absmiddle\\\" alt=\\\"\\\" size=\\\"750x750\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t5401/279/1708006599/281440/fc05b81b/59137801N652e8c2e.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; width: 100%;\\\" /><img align=\\\"absmiddle\\\" alt=\\\"\\\" size=\\\"750x750\\\" src=\\\"https://img10.360buyimg.com/imgzone/jfs/t5083/170/1723976704/239532/172798ab/591377fdNdf64f102.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle;width: 100%;\\\" /></p></div><br />', '2020-04-15', '0');
INSERT INTO `app_goods` VALUES ('118', '183044524', '497', '香后T恤女2018夏季新款短袖韩版修身', '89.00', '|1022|,|970|', '0.00', '商品名称：香后T恤女2018夏季新款短袖韩版修身女士字母印花打底衫时尚百搭休闲大码上衣88862 黑色 M<br />商品编号：25827631799<br />店铺： 香后旗舰店<br />商品毛重：300.00g<br />尺码：S，M，L，XL，XXL<br />版型：修身型<br />主要材质：其它<br />领型：V领<br />流行元素：其它<br />风格：休闲，简约，韩版<br />颜色：多色系<br />袖长：短袖<br />袖型：常规袖<br />适用年龄：25-29周岁<br />图案文化：其它<br />图案：其它<br />衣长：常规款<br />弹力：微弹<br />上市时间：2018夏季<br /><br /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;\\\" /><img alt=\\\"\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t18163/290/665314177/249888/81f785bf/5a9d1472N2b2f88db.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;width:100%\\\" /><br style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;width:100%\\\" /><img alt=\\\"\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t18841/127/646685415/178271/6d916624/5a9d1473Naf4103b4.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; vertical-align: middle; color: rgb(102, 102, 102); font-family: tahoma, arial, &quot;Microsoft YaHei&quot;, &quot;Hiragino Sans GB&quot;, u5b8bu4f53, sans-serif;width:100%\\\" />', '2020-05-16', '0');
INSERT INTO `app_goods` VALUES ('119', '196472467', '498', '2018春夏新款女装简约修身休闲破洞贴花牛仔短裤', '98.00', '|1023|', '0.00', '商品名称：烟花烫2018春夏新款女装简约修身休闲破洞贴花牛仔短裤女 醉浅 蓝色 L预售37天<br />商品编号：25946563443<br />店铺： 烟花烫官方旗舰店<br />商品毛重：500.00g<br />货号：3814190<br />腰型：中腰<br />版型：修身型<br />主要材质：棉<br />流行元素：刺绣<br />适用年龄：25-29周岁<br />款式：牛仔短裤<br />厚薄：常规<br />类型：短裤<br />弹力：无弹力<br />上市时间：2018春季<br /><br /><br /><img alt=\\\"\\\" src=\\\"https://img30.360buyimg.com/popWaterMark/jfs/t17356/228/698874867/229003/6ce19e77/5aa1eba5Nc1426dcf.jpg\\\" width=\\\"100%\\\" /><br />', '2020-05-16', '0');
INSERT INTO `app_goods` VALUES ('120', '226362889', '498', '扇百戏局杂技团2017秋季新款拼接撞色褶皱蓬蓬裙荷叶边半身裙', '288.00', '|973|', '10.00', '密扇百戏局杂技团2017秋季新款拼接撞色褶皱蓬蓬裙荷叶边半身裙<div>印花贴布绣 荷叶边拼接 标语提花织带</div>', '2020-05-16', '0');
INSERT INTO `app_goods` VALUES ('121', '715885098', '498', '大码女装胖mm下装2018春季新款显瘦袜1200D微压加大码连裤打底袜', '28.00', '|1023|', '5.00', '<h3 class=\\\"tb-main-title\\\" data-spm-anchor-id=\\\"2013.1.iteminfo.i0.19ae6d7fbcDfNu\\\" data-title=\\\"大码女装胖mm下装2018春季新款显瘦袜1200D微压加大码连裤打底袜\\\" style=\\\"margin: 0px; padding: 0px; font-size: 16px; min-height: 21px; line-height: 21px; color: rgb(60, 60, 60); font-family: tahoma, arial, &quot;Hiragino Sans GB&quot;, 宋体, sans-serif;\\\">大码女装胖mm下装2018春季新款显瘦袜1200D微压加大码连裤打底袜</h3>', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('122', '753424386', '498', '2018夏季新款A字中裙半身裙女复古小格子高腰荷叶边不规则鱼尾裙', '255.00', '|1023|', '0.00', '<div>2018夏季新款A字中裙半身裙女复古小格子高腰荷叶边不规则鱼尾裙</div>', '2020-05-10', '0');
INSERT INTO `app_goods` VALUES ('123', '785391832', '498', '2018春秋新款网纱a字裙女复古长款半身裙宽松高腰韩版黑色伞裙夏', '79.00', '|973|', '5.00', '2018春秋新款网纱a字裙女复古长款半身裙宽松高腰韩版黑色伞裙夏2<div>&nbsp;</div>', '2020-05-10', '0');
INSERT INTO `app_goods` VALUES ('124', '873512747', '498', '蕾丝半身裙女2018春夏新款韩版修身包臀a字纯色不规则裙 CM81004', '118.00', '|972|', '20.00', '蕾丝半身裙女2018春夏新款韩版修身包臀a字纯色不规则裙&nbsp;CM81004', '2020-05-10', '0');
INSERT INTO `app_goods` VALUES ('125', '546511537', '523', '小清新高跟鞋春季2018新款夏季女鞋少女韩版百搭细跟凉鞋学生鞋子', '118.00', '', '20.00', '小清新高跟鞋春季2018新款夏季女鞋少女韩版百搭细跟凉鞋学生鞋子', '2020-05-15', '0');
INSERT INTO `app_goods` VALUES ('126', '205476485', '523', '2018新款韩版高跟鞋女凉鞋夏细跟尖头一字扣猫跟鞋包头百搭磨砂皮', '200.00', '', '10.00', '2018新款韩版高跟鞋女凉鞋夏细跟尖头一字扣猫跟鞋包头百搭磨砂皮', '2020-05-10', '0');
INSERT INTO `app_goods` VALUES ('127', '189918736', '523', '2018夏季新款韩版百搭高跟鞋女显瘦细跟黑色工作鞋金属扣露趾凉鞋', '300.00', '', '20.00', '2018夏季新款韩版百搭高跟鞋女显瘦细跟黑色工作鞋金属扣露趾凉鞋<div>&nbsp;</div>', '2020-05-10', '0');
INSERT INTO `app_goods` VALUES ('128', '767430600', '523', '雪兰黛2018春季新款高跟鞋尖头细跟性感鞋子女韩版透气纱网女单鞋 ', '280.00', '', '10.00', '雪兰黛2018春季新款高跟鞋尖头细跟性感鞋子女韩版透气纱网女单鞋&nbsp;', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('129', '944196216', '523', '小白鞋女2018春夏季新款韩版百搭平底学生原宿ulzzang帆布鞋板鞋', '288.00', '', '10.00', '小白鞋女2018春夏季新款韩版百搭平底学生原宿ulzzang帆布鞋板鞋<div>&nbsp;</div>', '2020-05-10', '0');
INSERT INTO `app_goods` VALUES ('130', '417197395', '523', '老爹鞋女韩版ulzzang原宿百搭网鞋透气网面内增高运动鞋网鞋夏季', '255.00', '', '15.00', '老爹鞋女韩版ulzzang原宿百搭网鞋透气网面内增高运动鞋网鞋夏季', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('131', '981541541', '523', '欧美尖头蝴蝶结拖鞋女夏外穿2018新款绸缎面细跟凉拖半拖鞋穆勒鞋', '25.50', '', '10.00', '欧美尖头蝴蝶结拖鞋女夏外穿2018新款绸缎面细跟凉拖半拖鞋穆勒鞋', '2020-05-10', '0');
INSERT INTO `app_goods` VALUES ('132', '143208071', '523', '高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带', '12.80', '', '20.00', '高跟鞋女2018新款春季单鞋仙女甜美链子尖<span style=\\\"color:#ffa500;\\\">头防水台细跟女鞋一字带高跟鞋女2018新款春季单鞋仙女甜美</span>链子尖头防水台细跟女鞋一字带高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带<br /><br />高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带高<span style=\\\"color:#0000ff;\\\"><span style=\\\"font-size:16px;\\\">跟鞋女2018新款春季单鞋仙女甜美链子尖头</span></span>防水台细跟女鞋一字带高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带<br /><br /><img alt=\\\"\\\" src=\\\"http://vueshop.glbuys.com/uploadfiles/1524556409.jpg\\\" style=\\\"width: 100%;\\\" /><br /><br /><img alt=\\\"\\\" src=\\\"http://vueshop.glbuys.com/uploadfiles/1524556419.jpg\\\" style=\\\"width: 100%;\\\" /><br />', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('133', '371226276', '525', '骆驼男装 2018夏季新款圆领印花修身上衣 青年休闲微弹短袖T恤', '122.00', '', '50.00', '骆驼男装 2018夏季新款圆领印花修身上衣 青年休闲微弹短袖T恤', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('134', '708147276', '525', '夏季新款短袖t恤男士韩版潮流v领男装上衣服青年修身半截袖体恤衫', '88.00', '', '0.00', '夏季新款短袖t恤男士韩版潮流v领男装上衣服青年修身半截袖体恤衫', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('135', '684004338', '526', '2017新款裤子男韩版潮流九分运动裤哈伦休闲裤男修身小脚男士裤子', '81.87', '', '0.00', '2017新款裤子男韩版潮流九分运动裤哈伦休闲裤男修身小脚男士裤子', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('136', '984973799', '526', '牛仔裤男修身小脚韩版潮流2017宽松休闲直筒弹力男士九分裤牛子裤', '78.00', '', '0.00', '<div>&nbsp;牛仔裤男韩版潮流2017修身小脚男士宽松黑色百搭直筒九分裤牛子裤&nbsp;&nbsp;</div>', '2020-05-19', '0');
INSERT INTO `app_goods` VALUES ('137', '179723152', '526', '破洞牛仔裤男韩版潮流2017修身小脚男士九分裤直筒宽松帅气牛子裤', '79.00', '', '0.00', '破洞牛仔裤男韩版潮流2017修身小脚男士九分裤直筒宽松帅气牛子裤', '2020-05-15', '0');
INSERT INTO `app_goods` VALUES ('138', '251697854', '525', '莫代尔短袖t恤男士打底衫V领纯色黑色修身半袖潮夏装衣服运动体恤', '59.00', '', '0.00', '莫代尔短袖t恤男士打底衫V领纯色黑色修身半袖潮夏装衣服运动体恤', '2020-05-18', '0');
INSERT INTO `app_goods` VALUES ('139', '629526284', '501', 'Lenovo/联想 IdeaPad 320-15轻薄便携学生游戏15.6英寸笔记本电脑', '2799.00', '|995|,|997|', '0.00', 'Lenovo/联想&nbsp;IdeaPad&nbsp;320-15轻薄便携学生游戏15.6英寸笔记本电脑', '2020-05-17', '0');
INSERT INTO `app_goods` VALUES ('140', '636258252', '501', 'Hasee/神舟 战神 Z7-KP7D2/GT/S1吃鸡1060独显游戏本I7笔记本电脑', '6499.00', '|993|,|997|', '0.00', 'Hasee/神舟&nbsp;战神&nbsp;Z7-KP7D2/GT/S1吃鸡1060独显游戏本I7笔记本电脑', '2020-05-17', '0');
INSERT INTO `app_goods` VALUES ('141', '643774492', '501', 'Apple/苹果 12 英寸 MacBook 256GB苹果笔记本电脑超薄玫瑰金512G', '5400.00', '|993|,|997|', '0.00', '<div>产品名称:&nbsp;Apple/苹果&nbsp;12&nbsp;英寸&nbsp;MacBook&nbsp;256GB<br />上市时间:&nbsp;2015-03-04厚度:&nbsp;10.0mm(含)-15.0mm(不含)<br />屏幕类型:&nbsp;Retina&nbsp;显示屏是否PC平板二合一:&nbsp;否机身重量（含电池）:&nbsp;0.92kg<br />版本类型:&nbsp;港澳台能效等级:&nbsp;无品牌:&nbsp;Apple/苹果系列:&nbsp;12&nbsp;英寸型号:&nbsp;MacBook&nbsp;256GB<br />屏幕尺寸:&nbsp;12英寸屏幕比例:&nbsp;16:9CPU平台:&nbsp;OS&nbsp;X&nbsp;Yosemite<br />显卡类型:&nbsp;Intel&nbsp;HD&nbsp;Graphics&nbsp;5300显存容量:&nbsp;共享内存容量机械硬盘容量:&nbsp;无机械硬盘固态硬盘:&nbsp;256GB<br />内存容量:&nbsp;主板集成&nbsp;8GB&nbsp;1600MHz&nbsp;LPDDR3&nbsp;内存光驱类型:&nbsp;无光驱适用场景:&nbsp;家庭影音&nbsp;女性定位&nbsp;轻薄便携&nbsp;学生&nbsp;商务办公&nbsp;高清游戏&nbsp;尊贵旗舰&nbsp;家庭娱乐重量:&nbsp;0.92&nbsp;千克&nbsp;(2.03&nbsp;磅)锂电池电芯数量:&nbsp;锂聚合物电池售后服务:&nbsp;店铺三包颜色分类:&nbsp;促销价2015款256G灰色MJY32&nbsp;促销价2015款256G银色MF855&nbsp;促销价2015款256G金色MK4M2&nbsp;促销价2015款512G灰色MJY42&nbsp;促销价2015款512G银色MF865&nbsp;促销价2015款512G金色MK4N2&nbsp;2016款256G银色MLHA2&nbsp;2016款256G深空灰色MLH72&nbsp;2016款256G玫瑰金色MMGL2&nbsp;2016款256G金色MLHE2&nbsp;17款256G灰色MNYF2国行原封&nbsp;17款256G银色MNYH2国行原封&nbsp;17款256G金色MNYK2国行原封&nbsp;17款256G玫瑰金色MNYM2国行原封&nbsp;17款512G灰色MNYG2国行原封&nbsp;17款512G银色MNYJ2国行原封&nbsp;17款512G金色MNYL2国行原封&nbsp;17款512G玫瑰金色MNYN2国行原封&nbsp;17款256G灰色MNYF2&nbsp;17款512G金色MNYL2&nbsp;2015款256G金色MK4M2定制&nbsp;2015款512G灰色MJY42定制操作系统:&nbsp;OS&nbsp;X&nbsp;Yosemite通信技术类型:&nbsp;无线网卡&nbsp;蓝牙&nbsp;红外线输入设备:&nbsp;触摸板&nbsp;全尺寸键盘套餐类型:&nbsp;官方标配是否超极本:&nbsp;否分辨率:&nbsp;其他/other是否触摸屏:&nbsp;非触摸屏&nbsp;</div>', '2020-05-15', '0');
INSERT INTO `app_goods` VALUES ('142', '865867120', '501', 'Apple/苹果 MacBook Pro MPTV2CH/A TR2 15寸定制笔记本电脑2017', '11688.00', '|993|,|997|', '0.00', '<div><br />品名称:&nbsp;Apple/苹果&nbsp;MacBook&nbsp;Pro&nbsp;MPTV2CH/A厚度:&nbsp;15.0mm(含)-18.0mm(不含<br />机身重量（含电池）:&nbsp;1.99kg<br />版本类型:&nbsp;港澳台能效备案号:&nbsp;201610-27-5359-1006875992278<br />能效等级:&nbsp;一级品牌:&nbsp;Apple/苹果系列:&nbsp;MacBook&nbsp;ProMacBook&nbsp;Pro<br />系列型号:&nbsp;MPTV2CH/A<br />屏幕尺寸:&nbsp;15.4英寸屏幕比例:&nbsp;16:10CPU:&nbsp;英特尔&nbsp;酷睿&nbsp;i7-7820HK显卡类型:&nbsp;AMD&nbsp;Radeon&nbsp;RX560&nbsp;(Laptop)<br />显存容量:&nbsp;4G机械硬盘容量:&nbsp;无机械硬盘固态硬盘:&nbsp;512GB内存容量:&nbsp;16g光驱类型:&nbsp;无光驱&nbsp;</div>', '2020-05-15', '0');
INSERT INTO `app_goods` VALUES ('143', '739123405', '501', 'Apple/苹果 12 英寸 MacBook 256GB玫瑰金视网膜苹果笔记本电脑', '5480.00', '|993|,|997|', '20.00', '<div>产品名称:&nbsp;Apple/苹果&nbsp;12&nbsp;英寸&nbsp;MacBook&nbsp;256GB厚度:&nbsp;10.0mm(含)-15.0mm(不含)机身重量（含电池）:&nbsp;0.98kg版本类型:&nbsp;中国大陆能效等级:&nbsp;无品牌:&nbsp;Apple/苹果系列:&nbsp;12&nbsp;英寸&nbsp;MacBook型号:&nbsp;256GB屏幕尺寸:&nbsp;12英寸CPU:&nbsp;OS&nbsp;X&nbsp;El&nbsp;Capitan显卡类型:&nbsp;Intel&nbsp;HD&nbsp;Graphics&nbsp;515&nbsp;图形处理器显存容量:&nbsp;空机械硬盘容量:&nbsp;空固态硬盘:&nbsp;不详内存容量:&nbsp;8GB&nbsp;1866MHz&nbsp;LPDDR3&nbsp;主板集成内存光驱类型:&nbsp;无光驱适用场景:&nbsp;不详重量:&nbsp;0.92&nbsp;千克&nbsp;(2.03&nbsp;磅)售后服务:&nbsp;店铺三包颜色分类:&nbsp;15款灰色&nbsp;256G&nbsp;15款金色&nbsp;256G&nbsp;15款银色&nbsp;256G&nbsp;15款灰色&nbsp;512G&nbsp;15款金色&nbsp;512G&nbsp;15款银色&nbsp;512G&nbsp;16款灰色&nbsp;256G&nbsp;16款金色&nbsp;256G&nbsp;16款银色&nbsp;256G&nbsp;16款粉色&nbsp;256G&nbsp;16款灰色&nbsp;512G&nbsp;16款金色&nbsp;512G&nbsp;16款银色&nbsp;512G&nbsp;16款粉色&nbsp;512G&nbsp;其它型号拍下客服改价格&nbsp;17款M3/8G/256G&nbsp;颜色咨询客服&nbsp;17款&nbsp;I5&nbsp;/8G/512G&nbsp;颜色咨询客服&nbsp;17款&nbsp;I7/16G/512G&nbsp;颜色咨询客服操作系统:&nbsp;OS&nbsp;X&nbsp;El&nbsp;Capitan通信技术类型:&nbsp;3G&nbsp;红外线套餐类型:&nbsp;套餐一分辨率:&nbsp;2304&nbsp;x&nbsp;1440&nbsp;(226&nbsp;ppi)是否触摸屏:&nbsp;非触摸屏&nbsp;</div>', '2020-05-15', '0');
INSERT INTO `app_goods` VALUES ('144', '321776891', '501', '联想ThinkPad E- 570 4WCD商务办公轻薄便携笔记本电脑15.6英寸学', '2799.00', '|995|,|997|', '0.00', '产品名称：便携式计算机3C规格型号：ThinkPad&nbsp;E570********,&nbsp;ThinkPad&nbsp;E570c********,&nbsp;Thi...产品名称：ThinkPad&nbsp;E-&nbsp;5品牌:&nbsp;ThinkPad型号:&nbsp;5屏幕尺寸:&nbsp;15.6英寸CPU:&nbsp;intel赛扬Cel-3865U显卡类型:&nbsp;英特尔&nbsp;HD&nbsp;610显存容量:&nbsp;共享内存容量机械硬盘容量:&nbsp;500G机械内存容量:&nbsp;4G操作系统:&nbsp;windows&nbsp;10<div>&nbsp;</div>', '2020-05-15', '0');
INSERT INTO `app_goods` VALUES ('145', '245777880', '502', 'Mac苹果笔记本Macbook电脑Air13寸保护壳Pro13.3外壳11配件12套15', '58.00', '', '0.00', '品牌: 帝伊工坊炫彩贴颜色: 米黄色+同色键盘膜+塞 奶油粉+同色键盘膜+塞 宁静蓝+同色键盘膜+塞 蔷薇粉+同色键盘膜+塞 薰衣草+同色键盘膜+塞 帝芙尼蓝+同色键盘膜+塞 米黄色+升级豪华同色键盘膜+塞 奶油粉+升级豪华同色键盘膜+塞 宁静蓝+升级豪华同色键盘膜+塞 蔷薇粉+升级豪华同色键盘膜+塞 薰衣草+升级豪华同色键盘膜+塞 帝芙尼蓝+升级豪华同色键盘膜+塞 米黄色+升级快捷键键盘膜+塞 奶油粉+升级快捷键键盘膜+塞 宁静蓝+升级快捷键键盘膜+塞 蔷薇粉+升级快捷键键盘膜+塞 薰衣草+升级快捷键键盘膜+塞 帝芙尼蓝+升级快捷键键盘膜+塞&nbsp;', '2020-05-06', '0');
INSERT INTO `app_goods` VALUES ('146', '628214407', '502', '台式机显示器电脑屏幕保护膜防蓝光护眼27寸15.6笔记本防辐射贴膜', '48.00', '', '0.00', '品牌:&nbsp;百好汇<br />型号:&nbsp;BHH-减蓝光<br />尺寸:&nbsp;17英寸<br />生产企业:&nbsp;Fancy<br />颜色分类:&nbsp;14英寸16:9（310*175mm)&nbsp;15.6英寸16：9（344*194mm）&nbsp;17英寸5:4（340*275mm)&nbsp;17英寸4:3（339*271mm)&nbsp;17英寸16:9（383*215mm）&nbsp;18.5英寸16:9(410*230mm)&nbsp;19英寸4:3(376*300mm)&nbsp;19英寸16:10(410*257mm)&nbsp;19.5英寸（430*240MM)&nbsp;20英寸16:9(443*249mm)&nbsp;21.5英寸16:9(476*268mm)&nbsp;22英寸16:10(475*297mm)&nbsp;23英寸16:9(509*286mm)&nbsp;23.6英寸（522*294mm)&nbsp;23.8英寸16:9（528*296mm）&nbsp;24英寸16:9(532*299mm)&nbsp;24英寸16:10(518*324mm)&nbsp;27英寸16:9（597.6*336mm)&nbsp;32英寸（700*393mm）&nbsp;', '2020-05-06', '0');
INSERT INTO `app_goods` VALUES ('147', '691270703', '502', '埃普up-1笔记本支架便携式电脑支架散热器懒人保护颈椎桌面比目鱼', '49.00', '', '0.00', '品牌:&nbsp;UP/埃普型号:&nbsp;埃普up-1s材质:&nbsp;铝合金款式:&nbsp;折叠式&nbsp;升降式生产企业:&nbsp;宁波精辉机械压铸有限公司颜色分类:&nbsp;UP-1★普通版&nbsp;【亚光黑】（适合平板电脑、15英寸以下笔记本&nbsp;UP-1★普通版&nbsp;【糖果白】（适合平板电脑、15英寸以下笔记本&nbsp;UP-1★普通版【透明青】（适合平板电脑、15英寸以下笔记本)&nbsp;UP-1★普通版【清新绿】（适合平板电脑、15英寸以下笔记本)&nbsp;UP-1S【糖果白】(适合平板电脑、15.6英寸及以下笔记本、手机)&nbsp;UP-1S【亚光黑】(适合平板电脑、15.6英寸及以下笔记本、手机)货号:&nbsp;埃普up-1s&nbsp;', '2020-05-06', '0');
INSERT INTO `app_goods` VALUES ('148', '852883544', '502', 'ETS六代 笔记本抽风式散热器侧吸式戴尔联想电脑风扇17机14寸15.6', '108.00', '', '0.00', '产品名称：IETS IETS GT200/202生产企业: 深证市盛泰瑞科技有限公司颜色分类: USB带显示版本 USB标准版本 电源显示版本 电源标准版本按风扇配置: 1个散热方式: 风冷品牌: IETS型号: IETS GT200/202', '2020-05-01', '0');
INSERT INTO `app_goods` VALUES ('149', '914829807', '502', '联想华硕神舟笔记本贴膜15.6 戴尔宏基HP外壳保护膜电脑贴纸14寸', '28.00', '', '0.00', '品牌:&nbsp;新通路炫彩贴颜色:&nbsp;YA-053&nbsp;YA-317&nbsp;TTS-008&nbsp;YA-399&nbsp;YA-460&nbsp;YA-176&nbsp;YA-577&nbsp;TTS-125&nbsp;YA-335&nbsp;YA-527&nbsp;CM-177&nbsp;YA-086&nbsp;YA-572&nbsp;WX-160&nbsp;XT-386&nbsp;YA-289&nbsp;YA-269&nbsp;RFB-002&nbsp;TTS-036&nbsp;来图定制：三面ABC+键盘贴+同款大鼠标垫&nbsp;图库图案：三面ABC+键盘贴+同款大鼠标垫&nbsp;来图定制：三面ABC+键盘贴&nbsp;图库图案：三面ABC+键盘贴（其他图案）生产企业:&nbsp;郑州彩绘工坊图文制作有限公司风格:&nbsp;中国风&nbsp;', '2020-05-01', '0');
INSERT INTO `app_goods` VALUES ('150', '350587851', '502', '以诺双肩电脑包13.3/14/15.6寸男小米苹果电脑背包商务笔记本包女', '129.00', '', '0.00', '颜色分类:&nbsp;黑色&nbsp;灰色&nbsp;【尺寸说明：此款包14-15.6为同一个尺寸】款式:&nbsp;双肩材质:&nbsp;涤纶尺寸:&nbsp;14寸&nbsp;15寸&nbsp;15.6英寸风格:&nbsp;欧美品牌:&nbsp;以诺', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('151', '658312140', '502', '美国tomtoc13/15寸苹果笔记本macbook时尚商务手提男女电脑包纤薄', '149.00', '', '0.00', '颜色分类:&nbsp;粉色&nbsp;手提粉色时尚商务款&nbsp;手提牡丹花款&nbsp;粉色-黑底&nbsp;手提牡丹花款&nbsp;深红色&nbsp;手提牡丹花款&nbsp;玫红色&nbsp;灰底&nbsp;手提裙裳商务款&nbsp;浅灰色&nbsp;黑色款式:&nbsp;手提公文式材质:&nbsp;涤纶尺寸:&nbsp;13寸&nbsp;14寸&nbsp;13.3英寸&nbsp;15.6英寸风格:&nbsp;欧美品牌:&nbsp;tomtoc/汤姆拓客&nbsp;', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('152', '324461936', '502', '微软ARC TOUCH无线蓝牙鼠标 苹果MAC笔记本创意超薄便携时尚折叠', '341.00', '', '0.00', '<ul class=\\\"attributes-list\\\" style=\\\"margin: 0px; padding-right: 15px; padding-left: 15px; list-style: none; clear: both; color: rgb(0, 0, 0); font-family: tahoma, arial, &quot;Hiragino Sans GB&quot;, 宋体, sans-serif;\\\"><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"Microsoft/微软 Arc Touch Mouse Surface版\\\">产品名称:&nbsp;Microsoft/微软 Arc Touch Mouse Surface版</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"14*7*14CM\\\">包装体积:&nbsp;14*7*14CM</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"Microsoft/微软\\\">品牌:&nbsp;Microsoft/微软</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"Arc Touch Mouse Surface版\\\">微软无线型号:&nbsp;Arc Touch Mouse Surface版</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"官方标配\\\">套餐类型:&nbsp;官方标配</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"0.21\\\">毛重:&nbsp;0.21</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"蓝牙版《美行钛黑》拍下再减30 ARC蓝牙版《美行原封》银灰 ARC无线版《少量简包》黑色 ARC蓝牙版《国行原封》钛黑 ARC蓝牙版《国行原封》银灰 ARC无线版《国行原封》黑色 ARC蓝牙版《少量简包》钛黑 钛黑色就是SURFACE版\\\">颜色分类:&nbsp;蓝牙版《美行钛黑》拍下再减30 ARC蓝牙版《美行原封》银灰 ARC无线版《少量简包》黑色 ARC蓝牙版《国行原封》钛黑 ARC蓝牙版《国行原封》银灰 ARC无线版《国行原封》黑色 ARC蓝牙版《少量简包》钛黑 钛黑色就是SURFACE版</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"15m\\\">无线距离:&nbsp;15m</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"7号电池\\\">电池型号:&nbsp;7号电池</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"支持\\\">无线技术:&nbsp;支持</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"蓝影\\\">工作方式:&nbsp;蓝影</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"3个\\\">按键数:&nbsp;3个</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"1000dpi\\\">光学分辨率:&nbsp;1000dpi</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"触控条\\\">滚轮数:&nbsp;触控条</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"支持\\\">是否支持人体工程学:&nbsp;支持</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"全新\\\">成色:&nbsp;全新</li><li style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"店铺三包\\\">售后服务:&nbsp;店铺三包</li><li data-spm-anchor-id=\\\"2013.1.0.i4.77404ce38VHTf7\\\" style=\\\"margin: 0px 20px 0px 0px; padding: 0px; display: inline; float: left; width: 206px; height: 24px; overflow: hidden; text-indent: 5px; line-height: 24px; white-space: nowrap; text-overflow: ellipsis;\\\" title=\\\"是\\\">是否盒装正品:&nbsp;是</li></ul>', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('153', '323813881', '502', '金属鼠标垫个性定制LOGO大号高贵时尚航空级铝合金圆形游戏办公批', '34.90', '', '0.00', '<h3 class=\\\"tb-main-title\\\" data-spm-anchor-id=\\\"2013.1.iteminfo.i2.500315b9m1e95N\\\" data-title=\\\"金属鼠标垫个性定制LOGO大号高贵时尚航空级铝合金圆形游戏办公批\\\" style=\\\"margin: 0px; padding: 0px; font-size: 16px; min-height: 21px; line-height: 21px; color: rgb(60, 60, 60); font-family: tahoma, arial, &quot;Hiragino Sans GB&quot;, 宋体, sans-serif;\\\">金属鼠标垫个性定制LOGO大号高贵时尚航空级铝合金圆形游戏办公批</h3>', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('155', '670706076', '527', '酷睿i5四核GTX1060独显台式机组装电脑主机整机 绝地求生吃鸡游戏', '4599.00', '', '0.00', '<span style=\\\"font-weight: 700; color: rgb(153, 153, 153); font-family: tahoma, arial, 微软雅黑, sans-serif;\\\">产品参数：</span><ul data-spm-anchor-id=\\\"a220o.1000855.0.i1.1fa764bftkm5UZ\\\" id=\\\"J_AttrUL\\\" style=\\\"margin: 0px; padding-right: 20px; padding-bottom: 18px; padding-left: 20px; list-style: none; zoom: 1; border-top: 1px solid rgb(255, 255, 255); color: rgb(64, 64, 64); font-family: tahoma, arial, 微软雅黑, sans-serif;\\\"><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"2016010901926640\\\">证书编号：2016010901926640</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"有效\\\">证书状态：有效</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"深圳市华万龙科技有限公司\\\">申请人名称：深圳市华万龙科技有限公司</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"深圳市华万龙科技有限公司\\\">制造商名称：深圳市华万龙科技有限公司</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"台式机电脑\\\">产品名称：台式机电脑</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"KYXX(xx由0-99表示，代表不同客户编号，代表yy由0-99表示，不同的产品销售区域，其差异不...\\\">3C产品型号：KYXX(xx由0-99表示，代表不同客户编号，代表yy由0-99表示，不同的产品销售区域，其差异不...</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"KYXX (xx由0-99表示，代表不同客户编号，代表yy由0-99表示，不同的产品销售区域，其差异...\\\">3C规格型号：KYXX (xx由0-99表示，代表不同客户编号，代表yy由0-99表示，不同的产品销售区域，其差异...</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"cooyes/酷耶 CY09\\\">产品名称：cooyes/酷耶 CY09</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"&nbsp;I5四核\\\">型号:&nbsp;I5四核</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"&nbsp;CY09\\\">型号:&nbsp;CY09</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"&nbsp;8GB\\\">内存容量:&nbsp;8GB</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"&nbsp;120GB\\\">硬盘容量:&nbsp;120GB</li><li style=\\\"margin: 10px 15px 0px 0px; padding: 0px; list-style: none; display: inline; float: left; width: 220px; height: 18px; overflow: hidden; line-height: 18px; vertical-align: top; white-space: nowrap; text-overflow: ellipsis; color: rgb(102, 102, 102);\\\" title=\\\"&nbsp;宽屏LED\\\">显示器类型:&nbsp;宽屏LED</li></ul>', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('156', '620902097', '499', '2017重工加厚羽绒服男士户外中长款大毛领韩版保暖衣修身防水外套', '458.00', '|976|,|979|', '0.00', '<div>充绒量:&nbsp;250g(含)-300g(不含)含绒量:&nbsp;70%材质成分:&nbsp;聚酯纤维100%穿搭方式:&nbsp;外穿衣长:&nbsp;中长款货号:&nbsp;201709领型:&nbsp;可脱卸帽颜色:&nbsp;深蓝&nbsp;墨绿&nbsp;浅灰&nbsp;米白&nbsp;军绿&nbsp;星空黑&nbsp;红色&nbsp;黑色尺码:&nbsp;M/170&nbsp;L/175&nbsp;XL/180&nbsp;2XL/185&nbsp;3XL/190面料分类:&nbsp;迷彩布款式细节:&nbsp;多口袋功能性:&nbsp;可脱卸领品牌:&nbsp;其它/other袖型:&nbsp;外观散口内里收口男装-穿着方式:&nbsp;外穿厚薄:&nbsp;加厚图案:&nbsp;纯色材质:&nbsp;锦纶锦纶含量:&nbsp;95%以上填充物:&nbsp;白鸭绒适用场景:&nbsp;其他休闲适用对象:&nbsp;青年基础风格:&nbsp;青春流行细分风格:&nbsp;青春活力服饰工艺:&nbsp;免烫处理&nbsp;</div>', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('157', '700417577', '499', 'PZP mountain baltoro jacket downinsulation雪山联名棉羽绒服', '468.00', '', '0.00', '衣长:&nbsp;常规颜色:&nbsp;爆炸款尺码:&nbsp;S&nbsp;M&nbsp;L&nbsp;XL品牌:&nbsp;其它/other填充物:&nbsp;白鸭绒适用场景:&nbsp;其他休闲基础风格:&nbsp;其他', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('158', '635492201', '499', 'HARSHCRUEL 秋冬男保暖充绒夹棉加厚防风羽绒棉服高领面罩TPU外套', '778.00', '|975|,|980|', '0.00', '衣长: 常规领型: 连帽颜色: 白色 黑色尺码: S 现货 M 现货 L 现货面料分类: 涂层布款式细节: 多口袋品牌: harsh and cruel/桀骜不驯男装-穿着方式: 外穿厚薄: 加厚填充物: 灰鸭绒适用场景: 其他休闲基础风格: 青春流行<div>&nbsp;</div>', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('159', '955533435', '500', '衣长: 常规领型: 连帽颜色: 白色 黑色尺码: S 现货 M 现货 L 现货面料分类: 涂层布款式细节: 多口袋品牌: harsh and cruel/桀骜不驯男装-穿着方式: 外穿厚薄: 加厚填充物: 灰鸭绒适用场景: 其他休闲基础风格: 青春流行', '119.00', '|984|,|987|', '0.00', '上市年份季节:&nbsp;2018年夏季材质成分:&nbsp;棉99.3%&nbsp;聚氨酯弹性纤维(氨纶)0.7%货号:&nbsp;SHUNANM2026销售渠道类型:&nbsp;纯电商(只在线上销售)牛仔面料:&nbsp;常规牛仔布品牌:&nbsp;舒男弹力:&nbsp;无弹厚薄:&nbsp;常规基础风格:&nbsp;青春流行&nbsp;', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('160', '792777211', '500', '牛仔裤男宽松九分裤韩版潮流文艺男生直筒夏季薄裤子百搭学生港风', '78.00', '|984|,|987|', '0.00', '裤长:&nbsp;长裤颜色:&nbsp;深色&nbsp;浅色尺码:&nbsp;S&nbsp;M&nbsp;L&nbsp;XL&nbsp;2XL牛仔面料:&nbsp;常规牛仔布工艺处理:&nbsp;猫须&nbsp;破洞&nbsp;做旧&nbsp;水洗品牌:&nbsp;其它/other款式细节:&nbsp;立体剪裁上市时间:&nbsp;2017年适用季节:&nbsp;春季适用场景:&nbsp;其他休闲适用对象:&nbsp;青少年材质:&nbsp;棉弹力:&nbsp;无弹腰型:&nbsp;中腰裤脚口款式:&nbsp;小直脚裤门襟:&nbsp;拉链洗水工艺:&nbsp;水洗厚薄:&nbsp;常规款式版型:&nbsp;合体直筒基础风格:&nbsp;青春流行细分风格:&nbsp;潮&nbsp;<br /><br />', '2020-04-24', '0');
INSERT INTO `app_goods` VALUES ('164', '126829706', '504', 'Apple/苹果 iPhone 8 Plus', '4999.00', '', '10.00', '                <span style=\\\"color: rgb(102, 102, 102); font-family: tahoma, arial, 微软雅黑, sans-serif;\\\">品牌名称：</span><span class=\\\"J_EbrandLogo\\\" href=\\\"//brand.tmall.com/brandInfo.htm?brandId=30111&amp;type=0&amp;scm=1048.1.1.4\\\" style=\\\"font-family: tahoma, arial, 微软雅黑, sans-serif; margin: 0px; padding: 0px; color: rgb(51, 51, 51);\\\" target=\\\"_blank\\\">Apple/苹果</span><div id=\\\"J_TmpActBanner\\\" style=\\\"margin: 0px; padding: 0px; color: rgb(64, 64, 64); font-family: tahoma, arial, 微软雅黑, sans-serif;\\\">&nbsp;</div><div id=\\\"J_DcTopRightWrap\\\" style=\\\"margin: 0px; padding: 0px; width: 790px; position: relative; overflow: hidden; color: rgb(64, 64, 64); font-family: tahoma, arial, 微软雅黑, sans-serif;\\\">&nbsp;</div><div class=\\\"J_DetailSection tshop-psm tshop-psm-bdetaildes\\\" id=\\\"description\\\" style=\\\"margin: 0px; padding: 0px; width: auto; color: rgb(64, 64, 64); font-family: tahoma, arial, 微软雅黑, sans-serif;\\\"><div class=\\\"content ke-post\\\" style=\\\"margin: 10px 0px 0px; padding: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 1.5; font-family: tahoma, arial, 宋体, sans-serif; width: 790px; overflow: hidden; height: auto;\\\"><div style=\\\"margin: 0px; padding: 0px; width: 790px;\\\"><div style=\\\"margin: 0px; padding: 0px; float: left;\\\"><img border=\\\"0\\\" class=\\\"img-ks-lazyload\\\" data-spm-anchor-id=\\\"a220o.1000855.0.i12.39e3678eJGJZpU\\\" src=\\\"https://img.alicdn.com/imgextra/i2/1917047079/O1CN01rftKD222AEFVidi5b_!!1917047079.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; animation: 350ms linear 0ms 1 normal both running ks-fadeIn; opacity: 1; vertical-align: top;\\\" width=\\\"790\\\"></div><div style=\\\"margin: 6px 0px 0px; padding: 0px; float: left;\\\"><img class=\\\"img-ks-lazyload\\\" data-spm-anchor-id=\\\"a220o.1000855.0.i8.39e3678eJGJZpU\\\" src=\\\"https://img.alicdn.com/imgextra/i1/1917047079/O1CN01NJJJKi22AEK1yEIBI_!!1917047079.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; animation: 350ms linear 0ms 1 normal both running ks-fadeIn; opacity: 1; vertical-align: top;\\\" usemap=\\\"#gg\\\" width=\\\"790\\\"><map name=\\\"gg\\\" style=\\\"margin: 0px; padding: 0px;\\\"><area coords=\\\"215,265,247,287\\\" href=\\\"http://apple.tmall.com/p/rd235705.htm\\\" shape=\\\"rect\\\" style=\\\"margin: 0px; padding: 0px;\\\"></map></div><div style=\\\"margin: 0px; padding: 0px; float: left;\\\"><img class=\\\"img-ks-lazyload\\\" data-spm-anchor-id=\\\"a220o.1000855.0.i11.39e3678eJGJZpU\\\" src=\\\"https://img.alicdn.com/imgextra/i4/1917047079/O1CN01WA9D9V22AEJ9YkiN4_!!1917047079.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; animation: 350ms linear 0ms 1 normal both running ks-fadeIn; opacity: 1; vertical-align: top;\\\" width=\\\"790\\\"></div><div style=\\\"margin: 0px; padding: 0px; float: left;\\\"><img class=\\\"img-ks-lazyload\\\" data-spm-anchor-id=\\\"a220o.1000855.0.i10.39e3678eJGJZpU\\\" src=\\\"https://img.alicdn.com/imgextra/i4/1917047079/O1CN01s4QQgX22AEIfWsN6B_!!1917047079.jpg\\\" style=\\\"margin: 0px; padding: 0px; border: 0px; animation: 350ms linear 0ms 1 normal both running ks-fadeIn; opacity: 1; vertical-align: top;\\\" width=\\\"790\\\"></div></div></div></div><br>        ', '2020-04-20', '0');
INSERT INTO `app_goods` VALUES ('169', '464135956', '496', 'ggg', '11.00', '', '0.00', '                                <img src=\\\"http://localhost:8080/fantastic goods/server/uploadfiles/5ec8d0bf5a1ed.png\\\" alt=\\\"5ec8d0bf5a1ed.png\\\">    ', '2020-05-22', '0');

-- ----------------------------
-- Table structure for app_goodsimgs
-- ----------------------------
DROP TABLE IF EXISTS `app_goodsimgs`;
CREATE TABLE `app_goodsimgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` int(11) NOT NULL DEFAULT '0',
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gid` (`gid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_goodsimgs
-- ----------------------------
INSERT INTO `app_goodsimgs` VALUES ('2', '704407997', '1484283964.jpg');
INSERT INTO `app_goodsimgs` VALUES ('3', '704407997', '1484283967.jpg');
INSERT INTO `app_goodsimgs` VALUES ('4', '704407997', '1484283970.jpg');
INSERT INTO `app_goodsimgs` VALUES ('5', '286026274', '1484283665.jpg');
INSERT INTO `app_goodsimgs` VALUES ('6', '124555205', '1484284030.jpg');
INSERT INTO `app_goodsimgs` VALUES ('7', '124555205', '1484284042.jpg');
INSERT INTO `app_goodsimgs` VALUES ('8', '941801102', '1484284394.jpg');
INSERT INTO `app_goodsimgs` VALUES ('9', '941801102', '1484284406.jpg');
INSERT INTO `app_goodsimgs` VALUES ('10', '252173006', '1484284752.jpg');
INSERT INTO `app_goodsimgs` VALUES ('11', '252173006', '1484284765.jpg');
INSERT INTO `app_goodsimgs` VALUES ('12', '766946433', '1484284949.jpg');
INSERT INTO `app_goodsimgs` VALUES ('13', '766946433', '1484284961.jpg');
INSERT INTO `app_goodsimgs` VALUES ('14', '534523517', '1484285010.jpg');
INSERT INTO `app_goodsimgs` VALUES ('15', '534523517', '1484285026.jpg');
INSERT INTO `app_goodsimgs` VALUES ('16', '330520519', '1484285074.jpg');
INSERT INTO `app_goodsimgs` VALUES ('17', '613094524', '1484288431.jpg');
INSERT INTO `app_goodsimgs` VALUES ('18', '808830978', '1484288512.jpg');
INSERT INTO `app_goodsimgs` VALUES ('19', '827013226', '1484288656.jpg');
INSERT INTO `app_goodsimgs` VALUES ('20', '827013226', '1484288678.jpg');
INSERT INTO `app_goodsimgs` VALUES ('25', '541081261', '1484289248.jpg');
INSERT INTO `app_goodsimgs` VALUES ('26', '541081261', '1484289266.jpg');
INSERT INTO `app_goodsimgs` VALUES ('27', '219746948', '1484289366.jpg');
INSERT INTO `app_goodsimgs` VALUES ('28', '219746948', '1484289384.jpg');
INSERT INTO `app_goodsimgs` VALUES ('29', '482059023', '1484289512.jpg');
INSERT INTO `app_goodsimgs` VALUES ('30', '452529780', '1484289567.jpg');
INSERT INTO `app_goodsimgs` VALUES ('31', '452529780', '1484289583.jpg');
INSERT INTO `app_goodsimgs` VALUES ('32', '224184070', '1484289640.jpg');
INSERT INTO `app_goodsimgs` VALUES ('33', '224184070', '1484289660.jpg');
INSERT INTO `app_goodsimgs` VALUES ('34', '954387826', '1484289899.jpg');
INSERT INTO `app_goodsimgs` VALUES ('35', '774265843', '1484289944.jpg');
INSERT INTO `app_goodsimgs` VALUES ('37', '452502287', '1484283964.jpg');
INSERT INTO `app_goodsimgs` VALUES ('38', '704909428', '1484284042.jpg');
INSERT INTO `app_goodsimgs` VALUES ('39', '684006549', '1484283665.jpg');
INSERT INTO `app_goodsimgs` VALUES ('40', '617862381', '1484284030.jpg');
INSERT INTO `app_goodsimgs` VALUES ('41', '714246965', '1484284030.jpg');
INSERT INTO `app_goodsimgs` VALUES ('42', '714246965', '1484283665.jpg');
INSERT INTO `app_goodsimgs` VALUES ('43', '714246965', '1484284042.jpg');
INSERT INTO `app_goodsimgs` VALUES ('44', '427387635', '1523791025.jpg');
INSERT INTO `app_goodsimgs` VALUES ('45', '427387635', '1523791046.jpg');
INSERT INTO `app_goodsimgs` VALUES ('46', '427387635', '1523791085.jpg');
INSERT INTO `app_goodsimgs` VALUES ('47', '207686760', '1523791809.jpg');
INSERT INTO `app_goodsimgs` VALUES ('48', '207686760', '1523791831.jpg');
INSERT INTO `app_goodsimgs` VALUES ('49', '207686760', '1523791850.jpg');
INSERT INTO `app_goodsimgs` VALUES ('50', '207686760', '1523791869.jpg');
INSERT INTO `app_goodsimgs` VALUES ('51', '396797732', '1523792299.jpg');
INSERT INTO `app_goodsimgs` VALUES ('52', '396797732', '1523792317.jpg');
INSERT INTO `app_goodsimgs` VALUES ('53', '396797732', '1523792340.jpg');
INSERT INTO `app_goodsimgs` VALUES ('54', '552370716', '1523792627.jpg');
INSERT INTO `app_goodsimgs` VALUES ('55', '552370716', '1523792647.jpg');
INSERT INTO `app_goodsimgs` VALUES ('56', '552370716', '1523792664.jpg');
INSERT INTO `app_goodsimgs` VALUES ('57', '183044524', '1523793417.jpg');
INSERT INTO `app_goodsimgs` VALUES ('58', '183044524', '1523793442.jpg');
INSERT INTO `app_goodsimgs` VALUES ('59', '196472467', '1523793715.jpg');
INSERT INTO `app_goodsimgs` VALUES ('60', '196472467', '1523793737.jpg');
INSERT INTO `app_goodsimgs` VALUES ('61', '226362889', '1524553802.png');
INSERT INTO `app_goodsimgs` VALUES ('62', '226362889', '1524553830.jpg');
INSERT INTO `app_goodsimgs` VALUES ('63', '715885098', '1524554025.png');
INSERT INTO `app_goodsimgs` VALUES ('64', '715885098', '1524554067.png');
INSERT INTO `app_goodsimgs` VALUES ('65', '753424386', '1524554164.jpg');
INSERT INTO `app_goodsimgs` VALUES ('66', '785391832', '1524554255.jpg');
INSERT INTO `app_goodsimgs` VALUES ('67', '873512747', '1524554409.jpg');
INSERT INTO `app_goodsimgs` VALUES ('68', '546511537', '1524555818.jpg');
INSERT INTO `app_goodsimgs` VALUES ('69', '205476485', '1524555891.jpg');
INSERT INTO `app_goodsimgs` VALUES ('70', '189918736', '1524555954.jpg');
INSERT INTO `app_goodsimgs` VALUES ('71', '767430600', '1524556026.jpg');
INSERT INTO `app_goodsimgs` VALUES ('72', '944196216', '1524556119.jpg');
INSERT INTO `app_goodsimgs` VALUES ('73', '944196216', '1524556128.jpg');
INSERT INTO `app_goodsimgs` VALUES ('74', '944196216', '1524556140.jpg');
INSERT INTO `app_goodsimgs` VALUES ('75', '417197395', '1524556213.jpg');
INSERT INTO `app_goodsimgs` VALUES ('76', '417197395', '1524556224.jpg');
INSERT INTO `app_goodsimgs` VALUES ('77', '981541541', '1524556315.jpg');
INSERT INTO `app_goodsimgs` VALUES ('78', '981541541', '1524556328.jpg');
INSERT INTO `app_goodsimgs` VALUES ('79', '981541541', '1524556334.jpg');
INSERT INTO `app_goodsimgs` VALUES ('80', '143208071', '1524556409.jpg');
INSERT INTO `app_goodsimgs` VALUES ('81', '143208071', '1524556419.jpg');
INSERT INTO `app_goodsimgs` VALUES ('82', '371226276', '1524556746.png');
INSERT INTO `app_goodsimgs` VALUES ('83', '371226276', '1524556753.png');
INSERT INTO `app_goodsimgs` VALUES ('84', '371226276', '1524556758.jpg');
INSERT INTO `app_goodsimgs` VALUES ('85', '708147276', '1524556835.jpg');
INSERT INTO `app_goodsimgs` VALUES ('86', '708147276', '1524556840.jpg');
INSERT INTO `app_goodsimgs` VALUES ('87', '684004338', '1524557052.jpg');
INSERT INTO `app_goodsimgs` VALUES ('88', '684004338', '1524557058.jpg');
INSERT INTO `app_goodsimgs` VALUES ('89', '984973799', '1524557125.jpg');
INSERT INTO `app_goodsimgs` VALUES ('90', '984973799', '1524557131.jpg');
INSERT INTO `app_goodsimgs` VALUES ('91', '984973799', '1524557139.jpg');
INSERT INTO `app_goodsimgs` VALUES ('92', '179723152', '1524557203.jpg');
INSERT INTO `app_goodsimgs` VALUES ('93', '179723152', '1524557209.png');
INSERT INTO `app_goodsimgs` VALUES ('94', '251697854', '1524557449.jpg');
INSERT INTO `app_goodsimgs` VALUES ('95', '251697854', '1524557453.jpg');
INSERT INTO `app_goodsimgs` VALUES ('96', '629526284', '1524557555.jpg');
INSERT INTO `app_goodsimgs` VALUES ('97', '629526284', '1524557559.png');
INSERT INTO `app_goodsimgs` VALUES ('98', '636258252', '1524557632.jpg');
INSERT INTO `app_goodsimgs` VALUES ('99', '636258252', '1524557636.jpg');
INSERT INTO `app_goodsimgs` VALUES ('100', '636258252', '1524557640.jpg');
INSERT INTO `app_goodsimgs` VALUES ('101', '643774492', '1524557769.jpg');
INSERT INTO `app_goodsimgs` VALUES ('102', '643774492', '1524557773.jpg');
INSERT INTO `app_goodsimgs` VALUES ('103', '643774492', '1524557785.png');
INSERT INTO `app_goodsimgs` VALUES ('104', '865867120', '1524557968.jpg');
INSERT INTO `app_goodsimgs` VALUES ('105', '865867120', '1524557972.jpg');
INSERT INTO `app_goodsimgs` VALUES ('106', '865867120', '1524557976.jpg');
INSERT INTO `app_goodsimgs` VALUES ('107', '739123405', '1524558055.png');
INSERT INTO `app_goodsimgs` VALUES ('108', '739123405', '1524558059.png');
INSERT INTO `app_goodsimgs` VALUES ('109', '739123405', '1524558062.png');
INSERT INTO `app_goodsimgs` VALUES ('110', '321776891', '1524558126.png');
INSERT INTO `app_goodsimgs` VALUES ('111', '321776891', '1524558115.png');
INSERT INTO `app_goodsimgs` VALUES ('112', '321776891', '1524558121.png');
INSERT INTO `app_goodsimgs` VALUES ('113', '245777880', '1524558222.png');
INSERT INTO `app_goodsimgs` VALUES ('114', '245777880', '1524558227.png');
INSERT INTO `app_goodsimgs` VALUES ('115', '245777880', '1524558233.jpg');
INSERT INTO `app_goodsimgs` VALUES ('116', '628214407', '1524558266.jpg');
INSERT INTO `app_goodsimgs` VALUES ('117', '628214407', '1524558269.jpg');
INSERT INTO `app_goodsimgs` VALUES ('118', '691270703', '1524558344.png');
INSERT INTO `app_goodsimgs` VALUES ('119', '691270703', '1524558369.png');
INSERT INTO `app_goodsimgs` VALUES ('120', '691270703', '1524558373.jpg');
INSERT INTO `app_goodsimgs` VALUES ('121', '852883544', '1524558535.jpg');
INSERT INTO `app_goodsimgs` VALUES ('122', '852883544', '1524558538.png');
INSERT INTO `app_goodsimgs` VALUES ('123', '852883544', '1524558543.png');
INSERT INTO `app_goodsimgs` VALUES ('124', '914829807', '1524558607.jpg');
INSERT INTO `app_goodsimgs` VALUES ('125', '914829807', '1524558610.jpg');
INSERT INTO `app_goodsimgs` VALUES ('126', '914829807', '1524558615.jpg');
INSERT INTO `app_goodsimgs` VALUES ('127', '350587851', '1524558775.jpg');
INSERT INTO `app_goodsimgs` VALUES ('128', '350587851', '1524558779.jpg');
INSERT INTO `app_goodsimgs` VALUES ('129', '658312140', '1524558854.png');
INSERT INTO `app_goodsimgs` VALUES ('130', '658312140', '1524558859.png');
INSERT INTO `app_goodsimgs` VALUES ('131', '658312140', '1524558863.jpg');
INSERT INTO `app_goodsimgs` VALUES ('132', '324461936', '1524559415.png');
INSERT INTO `app_goodsimgs` VALUES ('133', '324461936', '1524559427.png');
INSERT INTO `app_goodsimgs` VALUES ('134', '324461936', '1524559435.png');
INSERT INTO `app_goodsimgs` VALUES ('135', '323813881', '1524559781.png');
INSERT INTO `app_goodsimgs` VALUES ('136', '323813881', '1524559788.png');
INSERT INTO `app_goodsimgs` VALUES ('137', '323813881', '1524559793.jpg');
INSERT INTO `app_goodsimgs` VALUES ('139', '670706076', '1524561138.jpg');
INSERT INTO `app_goodsimgs` VALUES ('140', '670706076', '1524561143.png');
INSERT INTO `app_goodsimgs` VALUES ('141', '670706076', '1524561148.jpg');
INSERT INTO `app_goodsimgs` VALUES ('142', '620902097', '1524561479.jpg');
INSERT INTO `app_goodsimgs` VALUES ('143', '620902097', '1524561484.jpg');
INSERT INTO `app_goodsimgs` VALUES ('144', '620902097', '1524561489.jpg');
INSERT INTO `app_goodsimgs` VALUES ('145', '700417577', '1524561560.jpg');
INSERT INTO `app_goodsimgs` VALUES ('146', '700417577', '1524561564.jpg');
INSERT INTO `app_goodsimgs` VALUES ('147', '635492201', '1524561627.png');
INSERT INTO `app_goodsimgs` VALUES ('148', '635492201', '1524561631.png');
INSERT INTO `app_goodsimgs` VALUES ('149', '635492201', '1524561634.jpg');
INSERT INTO `app_goodsimgs` VALUES ('157', '792777211', '1524562387.png');
INSERT INTO `app_goodsimgs` VALUES ('158', '792777211', '1524562393.jpg');
INSERT INTO `app_goodsimgs` VALUES ('159', '792777211', '1524562397.jpg');
INSERT INTO `app_goodsimgs` VALUES ('160', '792777211', '1524562401.jpg');
INSERT INTO `app_goodsimgs` VALUES ('161', '955533435', '1524562438.jpg');
INSERT INTO `app_goodsimgs` VALUES ('162', '955533435', '1524562446.jpg');
INSERT INTO `app_goodsimgs` VALUES ('163', '955533435', '1524562452.jpg');
INSERT INTO `app_goodsimgs` VALUES ('164', '454635074', '1524567287.jpg');
INSERT INTO `app_goodsimgs` VALUES ('165', '454635074', '1524567290.jpg');
INSERT INTO `app_goodsimgs` VALUES ('166', '634071836', '1524567419.jpg');
INSERT INTO `app_goodsimgs` VALUES ('167', '634071836', '1524567425.png');
INSERT INTO `app_goodsimgs` VALUES ('168', '265947808', '1524568819.jpg');
INSERT INTO `app_goodsimgs` VALUES ('169', '265947808', '1524568830.jpg');
INSERT INTO `app_goodsimgs` VALUES ('170', '265947808', '1524568844.jpg');
INSERT INTO `app_goodsimgs` VALUES ('171', '265947808', '1524568859.jpg');
INSERT INTO `app_goodsimgs` VALUES ('172', '265947808', '1524568873.jpg');
INSERT INTO `app_goodsimgs` VALUES ('175', '154695981', '1589442658.png');
INSERT INTO `app_goodsimgs` VALUES ('176', '464135956', '1590133451.png');
INSERT INTO `app_goodsimgs` VALUES ('177', '126829706', '1590139009.jpg');
INSERT INTO `app_goodsimgs` VALUES ('178', '512829844', '1590213139.jpg');
INSERT INTO `app_goodsimgs` VALUES ('179', '646959794', '1590216497.jpg');

-- ----------------------------
-- Table structure for app_order
-- ----------------------------
DROP TABLE IF EXISTS `app_order`;
CREATE TABLE `app_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordernum` int(11) NOT NULL DEFAULT '0',
  `times` varchar(20) DEFAULT NULL,
  `myid` varchar(20) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `iscomm` tinyint(1) NOT NULL DEFAULT '0',
  `addsid` int(11) NOT NULL DEFAULT '0',
  `freight` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `myid` (`myid`) USING BTREE,
  KEY `ordernum` (`ordernum`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1706 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of app_order
-- ----------------------------
INSERT INTO `app_order` VALUES ('1', '655223631', '2020-05-15 16:21:08', '593299496', '2', '1', '1308', '0');
INSERT INTO `app_order` VALUES ('2', '967363448', '2020-05-12 16:40:58', '593299496', '2', '0', '1', '20');
INSERT INTO `app_order` VALUES ('3', '727317874', '2020-05-14 11:05:01', '593299496', '2', '0', '1', '20');
INSERT INTO `app_order` VALUES ('1705', '912863885', '2020-05-23 16:33:35', '593299496', '2', '0', '1309', '0');

-- ----------------------------
-- Table structure for app_orderdesc
-- ----------------------------
DROP TABLE IF EXISTS `app_orderdesc`;
CREATE TABLE `app_orderdesc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordernum` int(11) NOT NULL DEFAULT '0',
  `gid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `amount` int(11) unsigned NOT NULL DEFAULT '0',
  `price` decimal(11,2) unsigned NOT NULL DEFAULT '0.00',
  `freight` decimal(11,2) unsigned NOT NULL DEFAULT '0.00',
  `myid` varchar(20) DEFAULT NULL,
  `param` varchar(255) DEFAULT NULL,
  `isreview` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ordernum` (`ordernum`) USING BTREE,
  KEY `gid` (`gid`) USING BTREE,
  KEY `myid` (`myid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2661 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_orderdesc
-- ----------------------------
INSERT INTO `app_orderdesc` VALUES ('1', '905009645', '143208071', '高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带', '1', '12.80', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1470\",\"title\":\"白色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1475\",\"title\":\"37\"}]}]', '0');
INSERT INTO `app_orderdesc` VALUES ('2', '927753672', '143208071', '高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带', '1', '12.80', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1470\",\"title\":\"白色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1476\",\"title\":\"38\"}]}]', '0');
INSERT INTO `app_orderdesc` VALUES ('3', '843586022', '143208071', '高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带', '1', '12.80', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1470\",\"title\":\"白色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1476\",\"title\":\"38\"}]}]', '0');
INSERT INTO `app_orderdesc` VALUES ('4', '843586022', '981541541', '欧美尖头蝴蝶结拖鞋女夏外穿2018新款绸缎面细跟凉拖半拖鞋穆勒鞋', '1', '25.50', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1485\",\"title\":\"蓝色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1488\",\"title\":\"37\"}]}]', '0');
INSERT INTO `app_orderdesc` VALUES ('5', '843586022', '944196216', '小白鞋女2018春夏季新款韩版百搭平底学生原宿ulzzang帆布鞋板鞋', '1', '288.00', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1177\",\"title\":\"黑色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1180\",\"title\":\"36\"}]}]', '0');
INSERT INTO `app_orderdesc` VALUES ('6', '967363448', '143208071', '高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带', '1', '12.80', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1470\",\"title\":\"白色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1476\",\"title\":\"38\"}]}]', '1');
INSERT INTO `app_orderdesc` VALUES ('7', '967363448', '981541541', '欧美尖头蝴蝶结拖鞋女夏外穿2018新款绸缎面细跟凉拖半拖鞋穆勒鞋', '1', '25.50', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1485\",\"title\":\"蓝色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1488\",\"title\":\"37\"}]}]', '1');
INSERT INTO `app_orderdesc` VALUES ('8', '967363448', '944196216', '小白鞋女2018春夏季新款韩版百搭平底学生原宿ulzzang帆布鞋板鞋', '1', '288.00', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1177\",\"title\":\"黑色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1180\",\"title\":\"36\"}]}]', '1');
INSERT INTO `app_orderdesc` VALUES ('9', '727317874', '143208071', '高跟鞋女2018新款春季单鞋仙女甜美链子尖头防水台细跟女鞋一字带', '1', '12.80', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1470\",\"title\":\"白色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1476\",\"title\":\"38\"}]}]', '1');
INSERT INTO `app_orderdesc` VALUES ('10', '727317874', '981541541', '欧美尖头蝴蝶结拖鞋女夏外穿2018新款绸缎面细跟凉拖半拖鞋穆勒鞋', '1', '25.50', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1485\",\"title\":\"蓝色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1488\",\"title\":\"37\"}]}]', '1');
INSERT INTO `app_orderdesc` VALUES ('11', '727317874', '944196216', '小白鞋女2018春夏季新款韩版百搭平底学生原宿ulzzang帆布鞋板鞋', '1', '288.00', '0.00', '593299496', '[{\"attrid\":\"1034\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"1177\",\"title\":\"黑色\"}]},{\"attrid\":\"1037\",\"title\":\"尺码\",\"param\":[{\"paramid\":\"1180\",\"title\":\"36\"}]}]', '1');
INSERT INTO `app_orderdesc` VALUES ('12', '655223631', '124555205', '韩都衣舍2016秋新款时尚拼接色宽松显瘦气质长款长袖连衣裙', '1', '128.00', '0.00', '593299496', '[{\"attrid\":\"1006\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"861\",\"title\":\"白色\"}]},{\"attrid\":\"1007\",\"title\":\"尺寸\",\"param\":[{\"paramid\":\"864\",\"title\":\"M\"}]}]', '1');
INSERT INTO `app_orderdesc` VALUES ('2660', '912863885', '124555205', '韩都衣舍2016秋新款时尚拼接色宽松显瘦气质长款长袖连衣裙', '1', '128.00', '0.00', '593299496', '[{\"attrid\":\"1006\",\"title\":\"颜色\",\"param\":[{\"paramid\":\"861\",\"title\":\"白色\"}]},{\"attrid\":\"1007\",\"title\":\"尺寸\",\"param\":[{\"paramid\":\"864\",\"title\":\"M\"}]}]', '0');

-- ----------------------------
-- Table structure for app_reviews
-- ----------------------------
DROP TABLE IF EXISTS `app_reviews`;
CREATE TABLE `app_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `myid` varchar(20) DEFAULT NULL,
  `gid` int(11) NOT NULL DEFAULT '0',
  `times` varchar(20) DEFAULT NULL,
  `audit` tinyint(1) NOT NULL DEFAULT '0',
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `myid` (`myid`) USING BTREE,
  KEY `gid` (`gid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=190 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_reviews
-- ----------------------------
INSERT INTO `app_reviews` VALUES ('1', '593299496', '417197395', '2020-05-15 16:34:11', '0', 'asasda ');
INSERT INTO `app_reviews` VALUES ('2', '593299496', '944196216', '2020-05-16 01:44:46', '1', '非常好');
INSERT INTO `app_reviews` VALUES ('3', '593299496', '286026274', '2020-05-16 19:26:52', '0', 'ok');
INSERT INTO `app_reviews` VALUES ('4', '593299496', '124555205', '2020-05-15 17:00:07', '1', 'hhhhh');
INSERT INTO `app_reviews` VALUES ('5', '593299496', '143208071', '2020-05-15 17:15:55', '1', 'hhhh');
INSERT INTO `app_reviews` VALUES ('6', '593299496', '124555205', '2020-05-18 15:24:39', '0', '11212');

-- ----------------------------
-- Table structure for app_specval
-- ----------------------------
DROP TABLE IF EXISTS `app_specval`;
CREATE TABLE `app_specval` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '产品id',
  `attrid` int(11) NOT NULL DEFAULT '0' COMMENT '属性id',
  `value` varchar(100) DEFAULT NULL COMMENT '值id',
  `isparam` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `gid` (`gid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1603 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of app_specval
-- ----------------------------
INSERT INTO `app_specval` VALUES ('800', '452529780', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('801', '452529780', '1009', '黑色', '1');
INSERT INTO `app_specval` VALUES ('802', '452529780', '1009', '白色', '1');
INSERT INTO `app_specval` VALUES ('803', '452529780', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('804', '452529780', '1010', '13', '1');
INSERT INTO `app_specval` VALUES ('805', '452529780', '1010', '15', '1');
INSERT INTO `app_specval` VALUES ('812', '224184070', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('813', '224184070', '1009', '黑色', '1');
INSERT INTO `app_specval` VALUES ('814', '224184070', '1009', '白色', '1');
INSERT INTO `app_specval` VALUES ('815', '224184070', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('816', '224184070', '1010', '14.1', '1');
INSERT INTO `app_specval` VALUES ('817', '224184070', '1010', '13.3', '1');
INSERT INTO `app_specval` VALUES ('818', '219746948', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('819', '219746948', '1009', '银色', '1');
INSERT INTO `app_specval` VALUES ('820', '219746948', '1009', '白色', '1');
INSERT INTO `app_specval` VALUES ('821', '219746948', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('822', '219746948', '1010', '15', '1');
INSERT INTO `app_specval` VALUES ('859', '124555205', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('860', '124555205', '1006', '红色', '1');
INSERT INTO `app_specval` VALUES ('861', '124555205', '1006', '白色', '1');
INSERT INTO `app_specval` VALUES ('862', '124555205', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('863', '124555205', '1007', 'L', '1');
INSERT INTO `app_specval` VALUES ('864', '124555205', '1007', 'M', '1');
INSERT INTO `app_specval` VALUES ('889', '452502287', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('890', '452502287', '1006', '红色', '1');
INSERT INTO `app_specval` VALUES ('891', '452502287', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('892', '452502287', '1007', 'L', '1');
INSERT INTO `app_specval` VALUES ('897', '286026274', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('898', '286026274', '1006', '黑色', '1');
INSERT INTO `app_specval` VALUES ('899', '286026274', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('900', '286026274', '1007', 'ML', '1');
INSERT INTO `app_specval` VALUES ('901', '684006549', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('902', '684006549', '1006', '橙色', '1');
INSERT INTO `app_specval` VALUES ('903', '684006549', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('904', '684006549', '1007', 'XL', '1');
INSERT INTO `app_specval` VALUES ('905', '617862381', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('906', '617862381', '1006', '紫色', '1');
INSERT INTO `app_specval` VALUES ('907', '617862381', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('908', '617862381', '1007', 'M', '1');
INSERT INTO `app_specval` VALUES ('933', '252173006', '1013', null, '0');
INSERT INTO `app_specval` VALUES ('934', '252173006', '1013', '黑色', '1');
INSERT INTO `app_specval` VALUES ('935', '252173006', '1013', '白色', '1');
INSERT INTO `app_specval` VALUES ('936', '252173006', '1014', null, '0');
INSERT INTO `app_specval` VALUES ('937', '252173006', '1014', 'M', '1');
INSERT INTO `app_specval` VALUES ('938', '252173006', '1014', 'L', '1');
INSERT INTO `app_specval` VALUES ('939', '941801102', '1013', null, '0');
INSERT INTO `app_specval` VALUES ('940', '941801102', '1013', '黑色', '1');
INSERT INTO `app_specval` VALUES ('941', '941801102', '1013', '白色', '1');
INSERT INTO `app_specval` VALUES ('942', '941801102', '1014', null, '0');
INSERT INTO `app_specval` VALUES ('943', '941801102', '1014', 'M', '1');
INSERT INTO `app_specval` VALUES ('944', '941801102', '1014', 'L', '1');
INSERT INTO `app_specval` VALUES ('945', '330520519', '1015', null, '0');
INSERT INTO `app_specval` VALUES ('946', '330520519', '1015', '黑色', '1');
INSERT INTO `app_specval` VALUES ('947', '330520519', '1015', '白色', '1');
INSERT INTO `app_specval` VALUES ('948', '330520519', '1016', null, '0');
INSERT INTO `app_specval` VALUES ('949', '330520519', '1016', 'M', '1');
INSERT INTO `app_specval` VALUES ('950', '330520519', '1016', 'L', '1');
INSERT INTO `app_specval` VALUES ('951', '534523517', '1015', null, '0');
INSERT INTO `app_specval` VALUES ('952', '534523517', '1015', '蓝色', '1');
INSERT INTO `app_specval` VALUES ('953', '534523517', '1015', '橙色', '1');
INSERT INTO `app_specval` VALUES ('954', '534523517', '1016', null, '0');
INSERT INTO `app_specval` VALUES ('955', '534523517', '1016', 'M', '1');
INSERT INTO `app_specval` VALUES ('956', '534523517', '1016', 'L', '1');
INSERT INTO `app_specval` VALUES ('957', '766946433', '1015', null, '0');
INSERT INTO `app_specval` VALUES ('958', '766946433', '1015', '黑色', '1');
INSERT INTO `app_specval` VALUES ('959', '766946433', '1015', '白色', '1');
INSERT INTO `app_specval` VALUES ('960', '766946433', '1016', null, '0');
INSERT INTO `app_specval` VALUES ('961', '766946433', '1016', 'M', '1');
INSERT INTO `app_specval` VALUES ('962', '766946433', '1016', 'L', '1');
INSERT INTO `app_specval` VALUES ('963', '827013226', '1017', null, '0');
INSERT INTO `app_specval` VALUES ('964', '827013226', '1017', '蓝色', '1');
INSERT INTO `app_specval` VALUES ('965', '827013226', '1018', null, '0');
INSERT INTO `app_specval` VALUES ('966', '827013226', '1018', 'M', '1');
INSERT INTO `app_specval` VALUES ('967', '827013226', '1018', 'X', '1');
INSERT INTO `app_specval` VALUES ('968', '808830978', '1017', null, '0');
INSERT INTO `app_specval` VALUES ('969', '808830978', '1017', '黑色', '1');
INSERT INTO `app_specval` VALUES ('970', '808830978', '1018', null, '0');
INSERT INTO `app_specval` VALUES ('971', '808830978', '1018', 'X', '1');
INSERT INTO `app_specval` VALUES ('972', '808830978', '1018', 'L', '1');
INSERT INTO `app_specval` VALUES ('973', '613094524', '1017', null, '0');
INSERT INTO `app_specval` VALUES ('974', '613094524', '1017', '黑色', '1');
INSERT INTO `app_specval` VALUES ('975', '613094524', '1018', null, '0');
INSERT INTO `app_specval` VALUES ('976', '613094524', '1018', 'X', '1');
INSERT INTO `app_specval` VALUES ('977', '613094524', '1018', 'L', '1');
INSERT INTO `app_specval` VALUES ('978', '482059023', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('979', '482059023', '1009', '黑色', '1');
INSERT INTO `app_specval` VALUES ('980', '482059023', '1009', '灰色', '1');
INSERT INTO `app_specval` VALUES ('981', '482059023', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('982', '482059023', '1010', '15.6', '1');
INSERT INTO `app_specval` VALUES ('983', '482059023', '1010', '13', '1');
INSERT INTO `app_specval` VALUES ('984', '541081261', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('985', '541081261', '1009', '白色', '1');
INSERT INTO `app_specval` VALUES ('986', '541081261', '1009', '银灰', '1');
INSERT INTO `app_specval` VALUES ('987', '541081261', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('988', '541081261', '1010', '13', '1');
INSERT INTO `app_specval` VALUES ('989', '541081261', '1010', '14', '1');
INSERT INTO `app_specval` VALUES ('990', '774265843', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('991', '774265843', '1019', '银色', '1');
INSERT INTO `app_specval` VALUES ('992', '774265843', '1019', '白色', '1');
INSERT INTO `app_specval` VALUES ('993', '954387826', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('994', '954387826', '1019', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1041', '427387635', '1013', null, '0');
INSERT INTO `app_specval` VALUES ('1042', '427387635', '1013', '红色', '1');
INSERT INTO `app_specval` VALUES ('1043', '427387635', '1013', '绿色', '1');
INSERT INTO `app_specval` VALUES ('1044', '427387635', '1014', null, '0');
INSERT INTO `app_specval` VALUES ('1045', '427387635', '1014', '155/80A/S', '1');
INSERT INTO `app_specval` VALUES ('1046', '427387635', '1014', '160/84A/M', '1');
INSERT INTO `app_specval` VALUES ('1052', '207686760', '1013', null, '0');
INSERT INTO `app_specval` VALUES ('1053', '207686760', '1013', '红色', '1');
INSERT INTO `app_specval` VALUES ('1054', '207686760', '1014', null, '0');
INSERT INTO `app_specval` VALUES ('1055', '207686760', '1014', 'S', '1');
INSERT INTO `app_specval` VALUES ('1056', '207686760', '1014', 'M', '1');
INSERT INTO `app_specval` VALUES ('1057', '396797732', '1013', null, '0');
INSERT INTO `app_specval` VALUES ('1058', '396797732', '1013', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1059', '396797732', '1013', '粉色', '1');
INSERT INTO `app_specval` VALUES ('1060', '396797732', '1014', null, '0');
INSERT INTO `app_specval` VALUES ('1061', '396797732', '1014', 'M 90-115斤', '1');
INSERT INTO `app_specval` VALUES ('1062', '396797732', '1014', 'L 110-135斤', '1');
INSERT INTO `app_specval` VALUES ('1081', '552370716', '1013', null, '0');
INSERT INTO `app_specval` VALUES ('1082', '552370716', '1013', '红色', '1');
INSERT INTO `app_specval` VALUES ('1083', '552370716', '1013', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1084', '552370716', '1014', null, '0');
INSERT INTO `app_specval` VALUES ('1085', '552370716', '1014', 'M', '1');
INSERT INTO `app_specval` VALUES ('1086', '552370716', '1014', 'L', '1');
INSERT INTO `app_specval` VALUES ('1093', '183044524', '1013', null, '0');
INSERT INTO `app_specval` VALUES ('1094', '183044524', '1013', '粉色', '1');
INSERT INTO `app_specval` VALUES ('1095', '183044524', '1013', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1096', '183044524', '1014', null, '0');
INSERT INTO `app_specval` VALUES ('1097', '183044524', '1014', 'M', '1');
INSERT INTO `app_specval` VALUES ('1098', '183044524', '1014', 'L', '1');
INSERT INTO `app_specval` VALUES ('1099', '196472467', '1015', null, '0');
INSERT INTO `app_specval` VALUES ('1100', '196472467', '1015', '蓝色', '1');
INSERT INTO `app_specval` VALUES ('1101', '196472467', '1016', null, '0');
INSERT INTO `app_specval` VALUES ('1102', '196472467', '1016', 'S', '1');
INSERT INTO `app_specval` VALUES ('1103', '196472467', '1016', 'M', '1');
INSERT INTO `app_specval` VALUES ('1104', '226362889', '1015', null, '0');
INSERT INTO `app_specval` VALUES ('1105', '226362889', '1015', '蓝色', '1');
INSERT INTO `app_specval` VALUES ('1106', '226362889', '1016', null, '0');
INSERT INTO `app_specval` VALUES ('1107', '226362889', '1016', 'S', '1');
INSERT INTO `app_specval` VALUES ('1108', '226362889', '1016', 'M', '1');
INSERT INTO `app_specval` VALUES ('1121', '785391832', '1015', null, '0');
INSERT INTO `app_specval` VALUES ('1122', '785391832', '1015', '蓝色', '1');
INSERT INTO `app_specval` VALUES ('1123', '785391832', '1015', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1124', '785391832', '1016', null, '0');
INSERT INTO `app_specval` VALUES ('1125', '785391832', '1016', 'S', '1');
INSERT INTO `app_specval` VALUES ('1126', '785391832', '1016', 'M', '1');
INSERT INTO `app_specval` VALUES ('1133', '753424386', '1015', null, '0');
INSERT INTO `app_specval` VALUES ('1134', '753424386', '1015', '灰色', '1');
INSERT INTO `app_specval` VALUES ('1135', '753424386', '1015', '白色', '1');
INSERT INTO `app_specval` VALUES ('1136', '753424386', '1016', null, '0');
INSERT INTO `app_specval` VALUES ('1137', '753424386', '1016', 'S', '1');
INSERT INTO `app_specval` VALUES ('1138', '753424386', '1016', 'M', '1');
INSERT INTO `app_specval` VALUES ('1139', '873512747', '1015', null, '0');
INSERT INTO `app_specval` VALUES ('1140', '873512747', '1015', '白色', '1');
INSERT INTO `app_specval` VALUES ('1141', '873512747', '1016', null, '0');
INSERT INTO `app_specval` VALUES ('1142', '873512747', '1016', 'S', '1');
INSERT INTO `app_specval` VALUES ('1143', '873512747', '1016', 'M', '1');
INSERT INTO `app_specval` VALUES ('1144', '715885098', '1015', null, '0');
INSERT INTO `app_specval` VALUES ('1145', '715885098', '1015', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1146', '715885098', '1015', '白色', '1');
INSERT INTO `app_specval` VALUES ('1147', '715885098', '1016', null, '0');
INSERT INTO `app_specval` VALUES ('1148', '715885098', '1016', 'S', '1');
INSERT INTO `app_specval` VALUES ('1149', '715885098', '1016', 'M', '1');
INSERT INTO `app_specval` VALUES ('1150', '715885098', '1016', 'L', '1');
INSERT INTO `app_specval` VALUES ('1151', '715885098', '1016', 'XL', '1');
INSERT INTO `app_specval` VALUES ('1152', '546511537', '1034', null, '0');
INSERT INTO `app_specval` VALUES ('1153', '546511537', '1034', '棕色', '1');
INSERT INTO `app_specval` VALUES ('1154', '546511537', '1034', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1155', '546511537', '1037', null, '0');
INSERT INTO `app_specval` VALUES ('1156', '546511537', '1037', '36', '1');
INSERT INTO `app_specval` VALUES ('1157', '546511537', '1037', '37', '1');
INSERT INTO `app_specval` VALUES ('1158', '205476485', '1034', null, '0');
INSERT INTO `app_specval` VALUES ('1159', '205476485', '1034', '粉色', '1');
INSERT INTO `app_specval` VALUES ('1160', '205476485', '1034', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1161', '205476485', '1037', null, '0');
INSERT INTO `app_specval` VALUES ('1162', '205476485', '1037', '36', '1');
INSERT INTO `app_specval` VALUES ('1163', '205476485', '1037', '37', '1');
INSERT INTO `app_specval` VALUES ('1164', '189918736', '1034', null, '0');
INSERT INTO `app_specval` VALUES ('1165', '189918736', '1034', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1166', '189918736', '1037', null, '0');
INSERT INTO `app_specval` VALUES ('1167', '189918736', '1037', '35', '1');
INSERT INTO `app_specval` VALUES ('1168', '189918736', '1037', '36', '1');
INSERT INTO `app_specval` VALUES ('1169', '767430600', '1034', null, '0');
INSERT INTO `app_specval` VALUES ('1170', '767430600', '1034', '黑白', '1');
INSERT INTO `app_specval` VALUES ('1171', '767430600', '1034', '肉色', '1');
INSERT INTO `app_specval` VALUES ('1172', '767430600', '1037', null, '0');
INSERT INTO `app_specval` VALUES ('1173', '767430600', '1037', '35', '1');
INSERT INTO `app_specval` VALUES ('1174', '767430600', '1037', '36', '1');
INSERT INTO `app_specval` VALUES ('1175', '944196216', '1034', null, '0');
INSERT INTO `app_specval` VALUES ('1176', '944196216', '1034', '白色', '1');
INSERT INTO `app_specval` VALUES ('1177', '944196216', '1034', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1178', '944196216', '1037', null, '0');
INSERT INTO `app_specval` VALUES ('1179', '944196216', '1037', '35', '1');
INSERT INTO `app_specval` VALUES ('1180', '944196216', '1037', '36', '1');
INSERT INTO `app_specval` VALUES ('1199', '371226276', '1041', null, '0');
INSERT INTO `app_specval` VALUES ('1200', '371226276', '1041', '天蓝色', '1');
INSERT INTO `app_specval` VALUES ('1201', '371226276', '1041', '淡紫色', '1');
INSERT INTO `app_specval` VALUES ('1202', '371226276', '1045', null, '0');
INSERT INTO `app_specval` VALUES ('1203', '371226276', '1045', 'S', '1');
INSERT INTO `app_specval` VALUES ('1204', '371226276', '1045', 'M', '1');
INSERT INTO `app_specval` VALUES ('1205', '708147276', '1041', null, '0');
INSERT INTO `app_specval` VALUES ('1206', '708147276', '1041', '白色', '1');
INSERT INTO `app_specval` VALUES ('1207', '708147276', '1041', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1208', '708147276', '1045', null, '0');
INSERT INTO `app_specval` VALUES ('1209', '708147276', '1045', 'M', '1');
INSERT INTO `app_specval` VALUES ('1210', '708147276', '1045', 'L', '1');
INSERT INTO `app_specval` VALUES ('1211', '684004338', '1051', null, '0');
INSERT INTO `app_specval` VALUES ('1212', '684004338', '1051', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1213', '684004338', '1051', '浅灰色', '1');
INSERT INTO `app_specval` VALUES ('1214', '684004338', '1055', null, '0');
INSERT INTO `app_specval` VALUES ('1215', '684004338', '1055', '29', '1');
INSERT INTO `app_specval` VALUES ('1216', '684004338', '1055', '30', '1');
INSERT INTO `app_specval` VALUES ('1217', '984973799', '1051', null, '0');
INSERT INTO `app_specval` VALUES ('1218', '984973799', '1051', '浅蓝色', '1');
INSERT INTO `app_specval` VALUES ('1219', '984973799', '1051', '深蓝色', '1');
INSERT INTO `app_specval` VALUES ('1220', '984973799', '1055', null, '0');
INSERT INTO `app_specval` VALUES ('1221', '984973799', '1055', '30', '1');
INSERT INTO `app_specval` VALUES ('1222', '984973799', '1055', '31', '1');
INSERT INTO `app_specval` VALUES ('1223', '179723152', '1051', null, '0');
INSERT INTO `app_specval` VALUES ('1224', '179723152', '1051', '蓝色', '1');
INSERT INTO `app_specval` VALUES ('1225', '179723152', '1051', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1226', '179723152', '1055', null, '0');
INSERT INTO `app_specval` VALUES ('1227', '179723152', '1055', '28', '1');
INSERT INTO `app_specval` VALUES ('1228', '179723152', '1055', '29', '1');
INSERT INTO `app_specval` VALUES ('1229', '251697854', '1041', null, '0');
INSERT INTO `app_specval` VALUES ('1230', '251697854', '1041', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1231', '251697854', '1041', '灰色', '1');
INSERT INTO `app_specval` VALUES ('1232', '251697854', '1045', null, '0');
INSERT INTO `app_specval` VALUES ('1233', '251697854', '1045', 'S', '1');
INSERT INTO `app_specval` VALUES ('1234', '251697854', '1045', 'M', '1');
INSERT INTO `app_specval` VALUES ('1235', '629526284', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('1236', '629526284', '1009', '白色', '1');
INSERT INTO `app_specval` VALUES ('1237', '629526284', '1009', '银灰色', '1');
INSERT INTO `app_specval` VALUES ('1238', '629526284', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('1239', '629526284', '1010', '13', '1');
INSERT INTO `app_specval` VALUES ('1240', '629526284', '1010', '14', '1');
INSERT INTO `app_specval` VALUES ('1241', '636258252', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('1242', '636258252', '1009', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1243', '636258252', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('1244', '636258252', '1010', '15.6', '1');
INSERT INTO `app_specval` VALUES ('1245', '643774492', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('1246', '643774492', '1009', '灰色', '1');
INSERT INTO `app_specval` VALUES ('1247', '643774492', '1009', '玫瑰金', '1');
INSERT INTO `app_specval` VALUES ('1248', '643774492', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('1249', '643774492', '1010', '15.6', '1');
INSERT INTO `app_specval` VALUES ('1250', '865867120', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('1251', '865867120', '1009', '银色', '1');
INSERT INTO `app_specval` VALUES ('1252', '865867120', '1009', '灰色', '1');
INSERT INTO `app_specval` VALUES ('1253', '865867120', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('1254', '865867120', '1010', '15.6', '1');
INSERT INTO `app_specval` VALUES ('1255', '739123405', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('1256', '739123405', '1009', '灰色', '1');
INSERT INTO `app_specval` VALUES ('1257', '739123405', '1009', '金色', '1');
INSERT INTO `app_specval` VALUES ('1258', '739123405', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('1259', '739123405', '1010', '14', '1');
INSERT INTO `app_specval` VALUES ('1260', '739123405', '1010', '15.6', '1');
INSERT INTO `app_specval` VALUES ('1261', '321776891', '1009', null, '0');
INSERT INTO `app_specval` VALUES ('1262', '321776891', '1009', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1263', '321776891', '1010', null, '0');
INSERT INTO `app_specval` VALUES ('1264', '321776891', '1010', '14', '1');
INSERT INTO `app_specval` VALUES ('1265', '321776891', '1010', '15.6', '1');
INSERT INTO `app_specval` VALUES ('1266', '245777880', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('1267', '245777880', '1019', '米黄色', '1');
INSERT INTO `app_specval` VALUES ('1268', '245777880', '1019', '粉色', '1');
INSERT INTO `app_specval` VALUES ('1269', '628214407', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('1270', '628214407', '1019', '蓝色', '1');
INSERT INTO `app_specval` VALUES ('1271', '691270703', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('1272', '691270703', '1019', '哑光黑', '1');
INSERT INTO `app_specval` VALUES ('1273', '691270703', '1019', '糖果白', '1');
INSERT INTO `app_specval` VALUES ('1274', '852883544', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('1275', '852883544', '1019', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1276', '852883544', '1019', '蓝色', '1');
INSERT INTO `app_specval` VALUES ('1277', '914829807', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('1278', '914829807', '1019', '白色', '1');
INSERT INTO `app_specval` VALUES ('1279', '914829807', '1019', '粉色', '1');
INSERT INTO `app_specval` VALUES ('1280', '350587851', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('1281', '350587851', '1019', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1282', '350587851', '1019', '灰色', '1');
INSERT INTO `app_specval` VALUES ('1283', '658312140', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('1284', '658312140', '1019', '粉色', '1');
INSERT INTO `app_specval` VALUES ('1285', '658312140', '1019', '浅灰', '1');
INSERT INTO `app_specval` VALUES ('1286', '324461936', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('1287', '324461936', '1019', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1288', '324461936', '1019', '银黑', '1');
INSERT INTO `app_specval` VALUES ('1289', '323813881', '1019', null, '0');
INSERT INTO `app_specval` VALUES ('1290', '323813881', '1019', '银色', '1');
INSERT INTO `app_specval` VALUES ('1291', '323813881', '1019', '玫瑰金', '1');
INSERT INTO `app_specval` VALUES ('1296', '670706076', '1060', null, '0');
INSERT INTO `app_specval` VALUES ('1297', '670706076', '1060', '银色', '1');
INSERT INTO `app_specval` VALUES ('1298', '670706076', '1060', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1299', '620902097', '1017', null, '0');
INSERT INTO `app_specval` VALUES ('1300', '620902097', '1017', '浅蓝', '1');
INSERT INTO `app_specval` VALUES ('1301', '620902097', '1017', '浅灰', '1');
INSERT INTO `app_specval` VALUES ('1302', '620902097', '1018', null, '0');
INSERT INTO `app_specval` VALUES ('1303', '620902097', '1018', 'S', '1');
INSERT INTO `app_specval` VALUES ('1304', '620902097', '1018', 'M', '1');
INSERT INTO `app_specval` VALUES ('1305', '700417577', '1017', null, '0');
INSERT INTO `app_specval` VALUES ('1306', '700417577', '1017', '爆炸跨', '1');
INSERT INTO `app_specval` VALUES ('1307', '700417577', '1018', null, '0');
INSERT INTO `app_specval` VALUES ('1308', '700417577', '1018', 'M', '1');
INSERT INTO `app_specval` VALUES ('1309', '700417577', '1018', 'L', '1');
INSERT INTO `app_specval` VALUES ('1310', '635492201', '1017', null, '0');
INSERT INTO `app_specval` VALUES ('1311', '635492201', '1017', '白色', '1');
INSERT INTO `app_specval` VALUES ('1312', '635492201', '1017', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1313', '635492201', '1018', null, '0');
INSERT INTO `app_specval` VALUES ('1314', '635492201', '1018', 'L', '1');
INSERT INTO `app_specval` VALUES ('1315', '635492201', '1018', 'XL', '1');
INSERT INTO `app_specval` VALUES ('1346', '792777211', '1011', null, '0');
INSERT INTO `app_specval` VALUES ('1347', '792777211', '1011', '黑灰', '1');
INSERT INTO `app_specval` VALUES ('1348', '792777211', '1011', '浅蓝', '1');
INSERT INTO `app_specval` VALUES ('1349', '792777211', '1012', null, '0');
INSERT INTO `app_specval` VALUES ('1350', '792777211', '1012', '29', '1');
INSERT INTO `app_specval` VALUES ('1351', '792777211', '1012', '30', '1');
INSERT INTO `app_specval` VALUES ('1352', '955533435', '1011', null, '0');
INSERT INTO `app_specval` VALUES ('1353', '955533435', '1011', '蓝色', '1');
INSERT INTO `app_specval` VALUES ('1354', '955533435', '1011', '浅蓝色', '1');
INSERT INTO `app_specval` VALUES ('1355', '955533435', '1012', null, '0');
INSERT INTO `app_specval` VALUES ('1356', '955533435', '1012', '29', '1');
INSERT INTO `app_specval` VALUES ('1357', '955533435', '1012', '30', '1');
INSERT INTO `app_specval` VALUES ('1358', '454635074', '1011', null, '0');
INSERT INTO `app_specval` VALUES ('1359', '454635074', '1011', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1360', '454635074', '1011', '深灰色', '1');
INSERT INTO `app_specval` VALUES ('1361', '454635074', '1012', null, '0');
INSERT INTO `app_specval` VALUES ('1362', '454635074', '1012', 'M', '1');
INSERT INTO `app_specval` VALUES ('1363', '454635074', '1012', 'L', '1');
INSERT INTO `app_specval` VALUES ('1370', '265947808', '1041', null, '0');
INSERT INTO `app_specval` VALUES ('1371', '265947808', '1041', '米白色', '1');
INSERT INTO `app_specval` VALUES ('1372', '265947808', '1045', null, '0');
INSERT INTO `app_specval` VALUES ('1373', '265947808', '1045', 'S', '1');
INSERT INTO `app_specval` VALUES ('1374', '265947808', '1045', 'M', '1');
INSERT INTO `app_specval` VALUES ('1393', '704407997', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('1394', '704407997', '1006', '红色', '1');
INSERT INTO `app_specval` VALUES ('1395', '704407997', '1006', '白色', '1');
INSERT INTO `app_specval` VALUES ('1396', '704407997', '1006', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1397', '704407997', '1006', '黄色', '1');
INSERT INTO `app_specval` VALUES ('1398', '704407997', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('1399', '704407997', '1007', 'S', '1');
INSERT INTO `app_specval` VALUES ('1400', '704407997', '1007', 'M', '1');
INSERT INTO `app_specval` VALUES ('1401', '704407997', '1007', 'L', '1');
INSERT INTO `app_specval` VALUES ('1402', '704407997', '1007', 'XL', '1');
INSERT INTO `app_specval` VALUES ('1467', '143208071', '1034', null, '0');
INSERT INTO `app_specval` VALUES ('1468', '143208071', '1034', '灰色', '1');
INSERT INTO `app_specval` VALUES ('1469', '143208071', '1034', '肉色', '1');
INSERT INTO `app_specval` VALUES ('1470', '143208071', '1034', '白色', '1');
INSERT INTO `app_specval` VALUES ('1471', '143208071', '1034', '紫色', '1');
INSERT INTO `app_specval` VALUES ('1472', '143208071', '1037', null, '0');
INSERT INTO `app_specval` VALUES ('1473', '143208071', '1037', '35', '1');
INSERT INTO `app_specval` VALUES ('1474', '143208071', '1037', '36', '1');
INSERT INTO `app_specval` VALUES ('1475', '143208071', '1037', '37', '1');
INSERT INTO `app_specval` VALUES ('1476', '143208071', '1037', '38', '1');
INSERT INTO `app_specval` VALUES ('1483', '981541541', '1034', null, '0');
INSERT INTO `app_specval` VALUES ('1484', '981541541', '1034', '白色', '1');
INSERT INTO `app_specval` VALUES ('1485', '981541541', '1034', '蓝色', '1');
INSERT INTO `app_specval` VALUES ('1486', '981541541', '1037', null, '0');
INSERT INTO `app_specval` VALUES ('1487', '981541541', '1037', '36', '1');
INSERT INTO `app_specval` VALUES ('1488', '981541541', '1037', '37', '1');
INSERT INTO `app_specval` VALUES ('1489', '417197395', '1034', null, '0');
INSERT INTO `app_specval` VALUES ('1490', '417197395', '1034', '红白色', '1');
INSERT INTO `app_specval` VALUES ('1491', '417197395', '1034', '白蓝色', '1');
INSERT INTO `app_specval` VALUES ('1492', '417197395', '1037', null, '0');
INSERT INTO `app_specval` VALUES ('1493', '417197395', '1037', '36', '1');
INSERT INTO `app_specval` VALUES ('1494', '417197395', '1037', '37', '1');
INSERT INTO `app_specval` VALUES ('1503', '634071836', '1011', null, '0');
INSERT INTO `app_specval` VALUES ('1504', '634071836', '1011', '深灰色', '1');
INSERT INTO `app_specval` VALUES ('1505', '634071836', '1011', '粉红色', '1');
INSERT INTO `app_specval` VALUES ('1506', '634071836', '1012', null, '0');
INSERT INTO `app_specval` VALUES ('1507', '634071836', '1012', '29', '1');
INSERT INTO `app_specval` VALUES ('1508', '634071836', '1012', '30', '1');
INSERT INTO `app_specval` VALUES ('1514', '733047695', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('1515', '733047695', '1006', '红色', '1');
INSERT INTO `app_specval` VALUES ('1516', '733047695', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('1517', '733047695', '1007', '11', '1');
INSERT INTO `app_specval` VALUES ('1522', '154695981', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('1523', '154695981', '1006', '1', '1');
INSERT INTO `app_specval` VALUES ('1524', '154695981', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('1525', '154695981', '1007', '11', '1');
INSERT INTO `app_specval` VALUES ('1536', '714246965', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('1537', '714246965', '1006', '红色', '1');
INSERT INTO `app_specval` VALUES ('1538', '714246965', '1006', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1539', '714246965', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('1540', '714246965', '1007', '36', '1');
INSERT INTO `app_specval` VALUES ('1541', '847277972', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('1542', '847277972', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('1543', '811385652', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('1544', '811385652', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('1565', '126829706', '1067', null, '0');
INSERT INTO `app_specval` VALUES ('1566', '126829706', '1067', 'iPhone8', '1');
INSERT INTO `app_specval` VALUES ('1567', '126829706', '1067', 'iPhone8 plus', '1');
INSERT INTO `app_specval` VALUES ('1568', '126829706', '1068', null, '0');
INSERT INTO `app_specval` VALUES ('1569', '126829706', '1068', '无需合约版', '1');
INSERT INTO `app_specval` VALUES ('1570', '126829706', '1069', null, '0');
INSERT INTO `app_specval` VALUES ('1571', '126829706', '1069', '银色', '1');
INSERT INTO `app_specval` VALUES ('1572', '126829706', '1069', '金色', '1');
INSERT INTO `app_specval` VALUES ('1581', '704909428', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('1582', '704909428', '1006', '白色', '1');
INSERT INTO `app_specval` VALUES ('1583', '704909428', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('1584', '704909428', '1007', 'X', '1');
INSERT INTO `app_specval` VALUES ('1593', '512829844', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('1594', '512829844', '1006', '红色', '1');
INSERT INTO `app_specval` VALUES ('1595', '512829844', '1006', '白色', '1');
INSERT INTO `app_specval` VALUES ('1596', '512829844', '1006', '黑色', '1');
INSERT INTO `app_specval` VALUES ('1597', '512829844', '1006', '紫色', '1');
INSERT INTO `app_specval` VALUES ('1598', '512829844', '1007', null, '0');
INSERT INTO `app_specval` VALUES ('1599', '512829844', '1007', 'L', '1');
INSERT INTO `app_specval` VALUES ('1600', '512829844', '1007', 'XL', '1');
INSERT INTO `app_specval` VALUES ('1601', '464135956', '1006', null, '0');
INSERT INTO `app_specval` VALUES ('1602', '464135956', '1007', null, '0');

-- ----------------------------
-- Table structure for app_user
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qid` varchar(20) DEFAULT NULL,
  `times` varchar(22) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `cellphone` varchar(13) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `head` varchar(20) DEFAULT NULL,
  `gender` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `qid` (`qid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1960 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES ('1', '211601588', '2020-05-04 15:41:55', '684beec6b5af21a88f3ab7e5735aac47', '18360172027', '啊啊啊', '1590219291.jpg', '1');
INSERT INTO `app_user` VALUES ('1959', '139722475', '2020-05-23 17:15:37', '96e79218965eb72c92a549dd5a330112', '13812345432', '会员_3405', null, '0');
INSERT INTO `app_user` VALUES ('2', '868093612', '2020-05-15 14:38:11', '96e79218965eb72c92a549dd5a330112', '13965393198', '会员_2174', '1590132767.png', '0');
INSERT INTO `app_user` VALUES ('4', '593299496', '2020-05-12 14:52:11', '96e79218965eb72c92a549dd5a330112', '13912239330', '菜菜', '382755161.png', '2');
INSERT INTO `app_user` VALUES ('3', '190215229', '2020-05-12 15:02:17', '96e79218965eb72c92a549dd5a330112', '1357698876', 'hhh', '1589266922.jpg', '1');
INSERT INTO `app_user` VALUES ('1958', '149941755', '2020-05-23 15:57:11', '96e79218965eb72c92a549dd5a330112', '19965393198', '会员_5346', '846345269.jpeg', '2');
