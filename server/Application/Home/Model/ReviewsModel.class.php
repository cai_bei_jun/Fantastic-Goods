<?php
namespace Home\Model;
use Think\Model;
use Think\MyModel;
class ReviewsModel extends Model{
	protected $pdo;
	public function __construct(){
		parent::__construct();
		$this->pdo=MyModel::getPdo();
	}

	public function getViewsTotal($gid){
        $sql="select u.nickname,u.head,r.content,r.times from __USER__ u inner join __REVIEWS__ r on u.qid=r.myid where r.gid=?";
        $sql.=" and r.audit='1'";

		$stmt=$this->pdo->prepare(MyModel::parseSql($sql));
		$stmt->execute(array($gid));
		$total=$stmt->rowCount();
		return $total;
	}

	public function getViewsPage($offset,$num,$gid){
		$sql="select u.nickname,u.head,r.content,r.times from __USER__ u inner join __REVIEWS__ r on u.qid=r.myid where r.gid=?";

			$sql.=" and r.audit='1'";
		$sql.=" order by r.times desc limit {$offset},{$num}";
		$stmt=$this->pdo->prepare(MyModel::parseSql($sql));
		$stmt->execute(array($gid));
		while($row=$stmt->fetch()){
			if($row["head"]!=""){
				$head=getHost()."/userfiles/head/".$row["head"];
			}else{
				$head="";
			}
			$data[]=array(
				"nickname"=>urlencode($row["nickname"]),
				"head"=>$head,
				"content"=>urlencode(faceDecode($row["content"])),
				"times"=>$row["times"]
			);
		}
		return $data;
	}

	//提交数据
	public function addReviews($uid,$gid,$content,$ordernum){

					$sql="insert into __REVIEWS__ (myid,gid,content,times) values (?,?,?,?)";
					$stmt=$this->pdo->prepare(MyModel::parseSql($sql));
					$stmt->execute(array($uid,$gid,$content,date("Y-m-d H:i:s")));
        $sql1="update __ORDER__ set iscomm=? where ordernum=?";
        $stmt1=$this->pdo->prepare(MyModel::parseSql($sql1));
        $stmt1->execute(array('1',$ordernum));
					$osql="update __ORDERDESC__ set isreview=? where myid=? and ordernum=?";
					$ostmt=$this->pdo->prepare(MyModel::parseSql($osql));
					$ostmt->execute(array('1',$uid,$ordernum));

					return "谢谢您的评价，我们会尽快审核！";
				}

}
