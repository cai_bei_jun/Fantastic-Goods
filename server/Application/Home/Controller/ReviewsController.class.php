<?php
namespace Home\Controller;
use Think\Controller;
use Common\Controller\CommonController;
use Common\Logic\MsgLogic;
use Common\Org\PageOrg;
class ReviewsController extends CommonController {

	public function index(){
		$gid=get_int($_GET["gid"]);
		if($gid>0){
			$reviews=D("Reviews");
			$pagesize=8;
			$current_page=isset($_GET["page"])?intval($_GET["page"]):1;
			$total=$reviews->getViewsTotal($gid);
			$fpage=new PageOrg($total,$current_page,$pagesize);
			$pageInfo=$fpage->getPageInfo();
			$datalist=$reviews->getViewsPage($pageInfo["row_offset"],$pageInfo["row_num"],$gid);
			if($datalist){
				MsgLogic::success(200,$datalist,array("pagesize"=>"{$pagesize}","page"=>"".$current_page."","pagenum"=>"".$pageInfo["page_num"]."","total"=>$total));
			}else{
				MsgLogic::error(201);
			}
		}else{
				MsgLogic::error(302,urlencode("获取失败"));
		}
	}

	//提交评价
	public function add(){
		$uid=get_str($_POST["uid"]);
		$gid=get_int($_POST["gid"]);
		$content=filterCode(faceEncode($_POST["content"]));
		$ordernum=get_int($_POST["ordernum"]);

            if ($uid != '' && $gid > 0 && $ordernum > 0) {
                if ($content == "" || $content == "undefined" || $content == "null") {
                    MsgLogic::error(302, urlencode("请输入评价内容"));
                }
                $reviews = D("Reviews");
                $msg = $reviews->addReviews($uid, $gid, $content, $ordernum);
                MsgLogic::success(200, urlencode($msg));
            } else {
                MsgLogic::error(302, urlencode("获取失败"));
            }
	}

	//上传图片
	public function upimage(){
		import("Common.Org.UploadFile");
		$uf=new \UploadFile();
		$imgcount=get_int($_POST["imgcount"]);
		if($imgcount>0){
			for($i=1;$i<=$imgcount;$i++){
				$uf->upfileload("image{$i}",'./userfiles/reviews',array("jpg","gif","png","jpeg"),10*1024*1024,"zip");
				$imgarr[]=json_decode($uf->msg,true);
			}
			if(count($imgarr)>0){
				for($i=0;$i<count($imgarr);$i++){
					if($imgarr[$i]["msg"]=='1'){
						$imgs[]=urlencode($imgarr[$i]["msbox"]);
					}
				}
			}
			MsgLogic::success(200,$imgs);
		}else{
			MsgLogic::error(302,urlencode("请上传图片"));
		}
	}

}
