<?php
namespace User\Model;
use Think\Model;
use Think\MyModel;
class ReviewsModel extends Model{
	protected $pdo;
	public function __construct(){
		parent::__construct();
		$this->pdo=MyModel::getPdo();
	}

	public function getReviewsTotal($uid){
		$sql="select r.id from __REVIEWS__ r inner join __CREATEACT__ c on r.actid=c.id where r.myid=?";
		$stmt=$this->pdo->prepare(MyModel::parseSql($sql));
		$stmt->execute(array($uid));
		$total=$stmt->rowCount();
		return $total;
	}

	public function getReviewsPage($offset,$num,$uid){
		$sql="select r.id,c.title,r.actid,r.targetid from __REVIEWS__ r inner join __CREATEACT__ c on r.actid=c.id where r.myid=?";
		$sql.=" order by r.times desc limit {$offset},{$num}";
		$stmt=$this->pdo->prepare(MyModel::parseSql($sql));
		$stmt->execute(array($uid));
	}

}
