<?php
namespace Home\Controller;
use Think\Controller;
use Home\Org\PageOrg;
use Home\Model\LogModel;
class GoodsListController extends IsLoginController{

	public function __construct(){
		parent::__construct();
	}

    public function index(){
        $this->display();
    }

	public function manage(){

		$column=D("Columns");
		$cname=$column->getTitle($this->id);
		$this->assign("cname",$cname);

		$cdata=$column->where("id=%d",array($this->id))->find();
		$this->assign("cdata",$cdata);


		if($this->kwords!=""){
			$strname="&kwords=".$this->kwords;
			$this->assign("strname",$strname);
		}

		$goods=D("Goods");
		$current_page=isset($_REQUEST["page"])?intval($_REQUEST["page"]):1;
		$this->assign("page",$current_page);
        $total=$goods->getGoodsTotal($this->id,urldecode($this->kwords));
		$this->assign("total",$total);
		$fpage=new PageOrg($total,$current_page,12);
		$pageInfo=$fpage->getPageInfo();
        $datalist=$goods->getGoodsPage($pageInfo["row_offset"],$pageInfo["row_num"],$this->id,urldecode($this->kwords));
		if($datalist){
			$this->assign("datalist",$datalist);
			$this->assign("getpage",$fpage->getpage($current_page,__CONTROLLER__."/manage?",$strname."&id={$this->id}"));
		}

		$this->display();
	}

    public function left(){
        $column=D("Columns");
        $cdata=$column->where("id=%d",array($this->id))->find();
        $this->assign("cdata",$cdata);
        $this->display();
    }

	//移动数据
	public function movedata(){
		$column=D("Columns");
		$column->moveColumn();
		$del=@implode(",",$_POST["del"]);
		if($del!=""){
			$goods=M("goods");
			$goods->where("id in ({$del})")->save(array("parentid"=>$colid));
			header("location:".__CONTROLLER__."/manage?id={$this->id}");
			exit;
		}else{
			echo "<script>alert('请选择要移动的数据');history.go(-1)</script>";
		}
	}

	//删除
	public function del(){
        $goods=D("Goods");
        $goods->delgoods($this->id);
	}

	//产品
	public function goods(){
		$column=D("Columns");
        $attr=D("Attr");

		$cname=$column->getTitle($this->id);
		$this->assign("cname",$cname);

        //获取属性
        $oneattr=$attr->goodsAttr($this->id);
        $this->assign("oneattr",$oneattr);

        //获取规格
        $specData=$attr->field("id,name")->where("cid=%d and type=%d",array($this->id,1))->order("id asc")->select();
        $this->assign("specTotal",count($specData));
        $this->assign("specData",$specData);


		if($this->action=='add'){
			$goods=D("Goods");
            $goods->addGoodsData($this->id);
		}

		$this->display();
	}

	//产品修改
	public function editgoods(){
		$column=D("Columns");
        $attr=D("Attr");
        $goods=D("Goods");

		$cname=$column->getTitle($this->cid);
		$this->assign("cname",$cname);

		$cdata=$column->where("id=%d",array($this->cid))->field("id,c_names")->find();
		$this->assign("modselpcls",$column->modgoodslist(0,0,0,$cdata["id"],$cdata["c_names"]));

		$data=$goods->where("qid=%d",array($this->id))->field("title,dates,bodys,price,freight")->find();
		$this->assign("data",$data);
		$this->assign("bodys",htmlspecialchars(stripslashes($data["bodys"])));

        //获取属性
        $oneattr=$attr->goodsAttr($this->cid,$this->id);
        $this->assign("oneattr",$oneattr);

        //产品规格
        $specData=$attr->getSpec($this->cid,$this->id);
        $this->assign("specTotal",count($specData));
        $this->assign("specData",$specData);

		if($this->action=='mod'){
			$column=D("Columns");
			$column->moveColumn();
            $goods->modGoods($this->cid,$this->id,$this->page);

		}
		$this->display();
	}
    //ajax产品图片
    public function ajxgoodsimgs(){
        $gimgs=M("Goodsimgs");
        $gid=get_int($_GET["gid"]);
        $this->assign("gid",$gid);
        $action=get_str($_GET["action"]);
        if($action=="del"){
            $gimgs->where("id=%d",array($this->id))->delete();
        }

        $images=$gimgs->field("id,photo")->where("gid=%d",array($gid))->select();
        $this->assign("images",$images);

        $this->display();
    }

    //属性管理
    public function attr(){
        $type=get_int($_GET["type"]);
        $this->assign("type",$type);

        $column=D("Columns");
        $cname=$column->getTitle($this->id);
        $this->assign("cname",$cname);

        $attr=D("Attr");
        $current_page=isset($_REQUEST["page"])?intval($_REQUEST["page"]):1;
        $this->assign("page",$current_page);
        $total=$attr->getParaTotal($this->id,$type);
        $this->assign("total",$total);
        $fpage=new PageOrg($total,$current_page,12);
        $pageInfo=$fpage->getPageInfo();
        $datalist=$attr->getParaPage($pageInfo["row_offset"],$pageInfo["row_num"],$this->id,$type);
        if($datalist){
            $this->assign("datalist",$datalist);
            $this->assign("getpage",$fpage->getpage($current_page,__ACTION__."?","&id={$this->id}"));
        }

        if($this->action=='add'){
            $attr->addPara($this->id,$type);
        }
        if($this->action=='del'){
            $attr->delPara($this->id,$type);
        }
        $this->display();
    }

    //参数修改
    public function editattr(){
        $type=get_int($_GET["type"]);
        $this->assign("type",$type);
        //标题文章列表
        $column=D("Columns");
        $attr=D("Attr");
        $cname=$column->getTitle($this->cid);
        $this->assign("cname",$cname);

        $getattr=$attr->field("name,id,ftype")->where("id=%d",array($this->id))->find();
        $this->assign("getattr",$getattr);

        //获取值
        $getVal=$attr->getParaVal($this->id);
        $this->assign("getVal",$getVal);

        //修改值
        if($this->action=='mod'){
            $attr->modParaVal($this->cid,$this->id);
        }

        $this->display();
    }

    //ajax删除参数值
    public function ajaxdelpara(){
        $attr=D("Attr");
        //删除
        if($this->id>0){
            $attr->where("id=%d",array($this->id))->delete();
        }

        //获取值
        $getVal=$attr->getParaVal($this->cid);
        $this->assign("getVal",$getVal);


        $this->display();
    }


}
