<?php
namespace Home\Controller;
use Think\Controller;
class ColumnManageController extends IsLoginController{

	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$sname=str_replace("%","\%",$_POST["search"]);
		$sname=str_replace("_","\_",$sname);
		$sname=trim(get_str($sname));

		$column=D("Columns");

		$this->assign("menu",$column->menu(0,$sname));
		$this->assign("total",$column->total);

		$this->display();
	}

	//栏目排序
	public function corder(){
		$num=$_GET["num"];
		$column=D("Columns");
		//上移排序
		if($this->action=='uppaixu'){
			$column->prevorder($this->pid,$this->id,$num);
		}else if($this->action=='downpaixu'){//下移排序
			$column->nextorder($this->pid,$this->id,$num);
		}
		echo $column->menu(0,'');
	}

	//删除
	public function del(){
		$column=D("Columns");
		$column->delcolumn($this->id);
	}

	//编辑
	public function edit(){
		$column=D("Columns");
		if($this->pid==0){
			$cdata=$column->where("id=%d",array($this->id))->field("id,c_names")->find();
			$cnb=$cdata["c_names"];
			$cnid=0;
		}else{
			$cdata=$column->where("id=%d",array($this->pid))->field("id,c_names")->find();
			$cnb=$cdata["c_names"];
			$cnid=$cdata["id"];
		}

		//无限级分类表单
		$this->assign("modselpcls",$column->modselpcls(0,0,0,$cnid,$cnb));

		$data=$column->where("id=%d",array($this->id))->field("c_names,image")->find();
		$this->assign("data",$data);

		$this->display();
	}


	public function mod(){
		$column=D("Columns");
		$column->modColumn($this->id,$this->pid);
	}

	//添加页面
	public function add(){
		$column=M("Columns");
		$data=$column->where("id=%d",array($this->id))->field("id,c_names")->find();
		$this->assign("datas",$data);
		$this->display();
	}

	public function incrData(){
		$column=D("Columns");
		$data=$column->where("id=%d",array($this->id))->field("id,c_names")->find();

		$column->addColumn($this->id,$data);

	}
}
