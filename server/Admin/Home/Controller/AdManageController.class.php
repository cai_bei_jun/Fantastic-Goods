<?php
namespace Home\Controller;
use Think\Controller;
use Home\Org\PageOrg;
use Home\Model\AdminModel;
class AdManageController extends IsLoginController{

	public function __construct(){
		parent::__construct();
	}

    public function index(){

        $this->display();
    }

    public function left(){

        $this->display();
    }

	public function manage(){
		//获取广告分组
		$group=M("AdGroup");
		$gdata=$group->order("id asc")->select();
		$this->assign("gdata",$gdata);

		$ad=M("Ad");
		$current_page=isset($_REQUEST["page"])?intval($_REQUEST["page"]):1;
		$this->assign("page",$current_page);
		$total=$ad->join("inner join __AD_GROUP__ on __AD__.gid=__AD_GROUP__.id")->count();
		$fpage=new PageOrg($total,$current_page,12);
		$pageInfo=$fpage->getPageInfo();
		$datalist=$ad->join("inner join __AD_GROUP__ on __AD__.gid=__AD_GROUP__.id")->field("__AD__.title,__AD__.id,__AD_GROUP__.title as gtitle,__AD__.photo")->order("__AD__.id desc")->limit($pageInfo["row_offset"],$pageInfo["row_num"])->select();
		if($datalist){
			$this->assign("datalist",$datalist);
			$this->assign("getpage",$fpage->getpage($current_page,__CONTROLLER__."/manage?",$strname));
		}

		$this->display();
	}

	public function add(){
		$title=get_str($_POST["title"]);
		if($title!=""){
			$ad=M("Ad");
			$ad->create();
			$ad->add();
			echo "<script>alert('添加成功');location.href='".__CONTROLLER__."/manage'</script>";
			exit;
		}
	}

	public function edit(){
		$group=M("Ad_group");
		$gdata=$group->order("id asc")->field("id,title")->select();
		$this->assign("gdata",$gdata);

		$ad=M("Ad");
		$data=$ad->join("inner join __AD_GROUP__ on __AD__.gid=__AD_GROUP__.id")->where("__AD__.id=%d",array($this->id))->field("__AD__.id,__AD__.title,__AD__.gid,__AD_GROUP__.title as gtitle,__AD__.photo")->find();
		$this->assign("data",$data);
		$this->display();
	}

	public function mod(){
		if($this->id>0){
			$gid=get_int($_POST["gid"]);
			$title=get_str($_POST["title"]);
			$photo=get_str($_POST["photo"]);
			if($gid>0 && $title!="" && $photo!=""){
				$ad=M("Ad");
				$data["gid"]=$gid;
				$data["title"]=$title;
				$data["photo"]=$photo;
				$ad->where("id=%d",array($this->id))->save($data);
				echo "<script>alert('修改成功！');location.href='".__CONTROLLER__."/edit?id={$this->id}'</script>";
				exit;
			}
		}
	}

	public function del(){
		$del=@implode(",",$_POST["del"]);
		if($del!=""){
			$ad=M("Ad");
			$ad->where("id in ({$del})")->delete();
			echo "<script>{alert ('删除成功'); location.href='".__CONTROLLER__."/manage';}</script>";
			exit;
		}else{
			echo "<script>{alert ('请选择要删除的数据'); history.go(-1);}</script>";
		}
	}


}
