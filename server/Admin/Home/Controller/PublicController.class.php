<?php
namespace Home\Controller;
use Think\Controller;
use Home\Org\UploadFileOrg;
use think\Request;
class PublicController extends Controller{
	public $id,$pid,$action,$cid,$parentid,$kwords,$page,$pg,$cardid;
    const baseurl = 'http://localhost:8080/fantastic goods/server/uploadfiles/';
	public function __construct(){
		parent::__construct();
		$this->kwords=get_str((trim($_REQUEST["kwords"])));
		/*$this->kwords=str_replace("%","\%",$this->kwords);
		$this->kwords=str_replace("_","\_",$this->kwords);*/
		$this->kwords=urlencode($this->kwords);
		$this->assign("kwords",$this->kwords);

		$this->action=$_GET["action"];
		$this->assign("action",$this->action);

		$this->id=$_GET["id"];
		$this->assign("id",$this->id);

		$this->pid=$_GET["pid"];
		$this->assign("pid",$this->pid);

		$this->cid=$_GET["cid"];
		$this->assign("cid",$this->cid);

		$this->parentid=$_GET["parentid"];
		$this->assign("parentid",$this->parentid);

		$this->page=get_int($_REQUEST["page"]);
		$this->assign("page",$this->page);

		$this->pg=$_REQUEST["pg"];
		$this->assign("pg",$this->pg);

		$this->cardid=get_int($_GET["cardid"]);
		$this->assign("cardid",$this->cardid);
		//用户名
		$this->assign("adminname",$_SESSION["adminname"]);
	}



	protected function isLogin(){
		if($_SESSION["loginjk"]!=1){
			header("location:".__MODULE__."/Index/");
			exit;
		}
	}

	//上传文件
	public function upload(){
		$this->isLogin();
		$filetype=["doc,pdf,rar"];//获取上传类型
		$arrfiletype=explode(",",$filetype);//类型分隔为数组
		$getfsize=2048000;//获取上传大小
		//图片上传
		if($this->action=='images'){
			$uf=new UploadFileOrg();
			$uf->upfileload('./uploadfiles',array("jpg","gif","png","jpeg"),$getfsize,$this->action);
		}
		//缩略图上传
		if($this->action=='thumb'){
			$uf=new UploadFileOrg();
			$uf->upfileload('./uploadfiles',array("jpg","gif","png","jpeg"),$getfsize,$this->action);
		}
		//视频上传
		if($this->action=='video'){
			$uf=new UploadFileOrg();
			$uf->upfileload('./videofiles',array("mp4"),$getfsize);
		}
		//文件上传
		if($this->action=='down'){
			$uf=new UploadFileOrg();
			$uf->upfileload('./downfiles',$arrfiletype,$getfsize);
		}
		//上传头像
		if($this->action=='head'){
			$uf=new UploadFileOrg();
			$uf->upfileload('./userfiles/head',array("jpg","gif","png","jpeg"),$getfsize,$this->action);
		}

		//会员发布活动图片
		if($this->action=='uimage'){
			$uf=new UploadFileOrg();
			$uf->upfileload('./userfiles/images',array("jpg","gif","png","jpeg"),$getfsize,$this->action);
		}
	}
    public function upload_wang(){
            $upload = new \Think\Upload();// 实例化上传类
            $upload->maxSize   =     3145728 ;// 设置附件上传大小
            $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
            $upload->rootPath  =      './uploadfiles/'; // 设置附件上传根目录

            // 上传单个文件
            $info   =   $upload->uploadOne($_FILES['file']);
            if($info) {// 上传错误提示错误信息
                echo json_encode(['code'=> 0,"msg"=>"", 'data' => [
                    'src' => self::baseurl.$info['savepath'].$info['savename'],
                    'title' => $info['savename']
                ]]);

            }else{// 上传成功 获取上传文件信息
                $this->error($upload->getError());
            }
    }


}
