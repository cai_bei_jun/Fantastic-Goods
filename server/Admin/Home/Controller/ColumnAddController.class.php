<?php
namespace Home\Controller;
use Think\Controller;
class ColumnAddController extends IsLoginController{

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->display();
	}

	public function add(){
		$column=D("Columns");
		if(!$column->create()){
			echo "<script>alert('".$column->getError()."');history.go(-1)</script>";
		}else{
			$column->parent_id=0;
			$column->parentpath="|0|";
			$column->num=date("YmdHis");
			$column->add();
			echo "<script>alert('添加成功！');location.href='".__CONTROLLER__."'</script>";
			exit;
		}
	}

}
