<?php
namespace Home\Model;
use Think\Model;
use Think\MyModel;
class AdminModel extends Model{
	protected $pdo,$prinfo;
	protected $_validate=array(
		array("username","require","请输入您的账号",0),
		array("password","require","请输入您的密码"),
		array("vdcode","require","请输入验证码",0),
		array('vdcode','checkCode','验证码错误!',0,'callback',1),
		array("adminname","require","请输入用户名",0),
		array("password","qpwd","您输入的密码不一致",0,"confirm"),
	);

	protected $_auto=array(
		array("password","md5",3,"function")
	);

	public function __construct(){
		parent::__construct();
		$this->pdo=MyModel::getPdo();
	}

	protected function getDate(){
		return date('Y-m-d H:i:s');
	}

	protected function checkCode($code){
		if(strtoupper($code)!=$_SESSION['imgcode']){
			return false;
		}else{
			return true;
		}
	}

	public function getAdminPage($offset,$num,$kwords){
		$sql="select a.adminname,a.id from __ADMIN__ as a where a.adminname like '%{$kwords}%'";
		$sql.=" order by a.id desc limit {$offset},{$num}";
		$stmt=$this->pdo->prepare(MyModel::parseSql($sql));
		$stmt->execute();
		while($list=$stmt->fetch()){
			$allist[]=array(
				'adminname'=>$list['adminname'],
				'id'=>$list['id'],
				'printf'=>rtrim($printf,",")
			);
			unset($printf);
		}
		return $allist;
	}
}
