<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>后台管理系统</title>
    <link href="/fantastic goods/server/Public/admin/css/defaults.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/layui/css/layui.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/css/xadmin.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/css/admin.css" rel="stylesheet" type="text/css" />
    <script src="/fantastic goods/server/Public/admin/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/defaults.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/xadmin.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.form.js" ></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/formfiles.js" ></script>
</head>

<body>
<div id="header">
	<div id="logo">Fantastic Goods后台管理系统</div>
    <div id="rdiv">
    	<ul class="ulnone">
        	<li class="date"><?php echo ($nowdate); ?> <?php echo ($weekarray); ?> <?php echo ($nowtime); ?> </li>
            <li class="funimg"><a href="javascript:void(0)" onclick="javascript:window.top.frames['main'].document.location.reload();"><img src="/fantastic goods/server/Public/admin/images/refresh.png" title="刷新" border="0" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="javascript:;" onclick="window.open('http://localhost:8081/','_blank','width=375,height=667,top=50,left=230,scrollbars=yes')"><img src="/fantastic goods/server/Public/admin/images/function.png" title="商城" border="0" /></a></li>
        </ul>
        <div id="menu">
            <div id="leftmenu"><img src="/fantastic goods/server/Public/admin/images/menu.png" border="0" />
                <div id="downmenu">
                    <div id="downmenu1"></div>
                    <div id="downmenu2">
                        <div id="showadmin">
                            <div id="adminimg"><img src="/fantastic goods/server/Public/admin/images/myico.png" width="29" height="30" /></div>
                            <div id="name"><?php echo ($adminname); ?></div>
                        </div>
                        <div id="menucon">
                            <ul class="ulnone">
                                <li><a href="/fantastic goods/server/hadmin.php/Home/Default/outlogin" target="_top">●&nbsp;安全退出</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="downmenu3"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="left-nav">
    <div id="side-nav">
        <ul class="layui-nav layui-nav-tree layui-nav-side">
            <li class="layui-nav-item"><a href="/fantastic goods/server/hadmin.php/Home/Default/index">
                <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/19.png"></i>
                首页</a></li>
            <li class="layui-nav-item"><a href="/fantastic goods/server/hadmin.php/Home/User/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/18.png"></i>
                    会员管理</a>
            </li>
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/4.png"></i>分类管理</a>
                <dl class="layui-nav-child">
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/ColumnManage/index">栏目管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/ColumnAdd/index">分类增加</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/GoodsList/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/14.png"></i>
                    商品管理</a>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/Order/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/12.png"></i>
                    订单管理</a>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/Reviews/manage">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/26.png"></i>
                    评价管理</a>
            </li>
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">
                    <i class="iconfont" ><img src="/fantastic goods/server/Public/admin/ico/22.png"></i>
                    其他功能</a>
                <dl class="layui-nav-child">
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/Fav/index">收藏管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/AdManage/index">banner管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/Address/index">收货地址管理</a></dd>
                </dl>
            </li>
        </ul>
    </div>
</div>
<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;
    });
</script>

<script type="text/javascript">
function CheckAll(form)
{
  for (var i=0;i<form.elements.length;i++)
    {
    var e = form.elements[i];
    if (e.Name != "chkAll"&&e.disabled!=true)
       e.checked = form.chkAll.checked;
    }
}
</script>
</head>

<div class="page-content">
<div id="spacemenu"></div>
<div class="alterdiv"></div>
<div id="maindiv">
<form id="form1" name="form1" method="post"  action="/fantastic goods/server/hadmin.php/Home/Reviews/del">
    <div id="navdiv">
        <table width="95%" border="0" cellspacing="0" cellpadding="0" class="gray" align="center">
  <tr>
    <td height="41" class="linkblue">&nbsp;&nbsp;&nbsp;您当前的位置：商品评价</td>
    <td width="59%" class="linkblue">会员帐号：<input type="text" name="kwords" id="kwords" class="htinputcss" /><input type="button" name="button3" id="button3" value="搜索" class="ssbtn" onClick="this.form.action='/fantastic goods/server/hadmin.php/Home/Reviews/manage';this.form.submit()" onFocus="this.blur()" />
    </td>
    </tr>
</table>
    </div>

<table width="95%" border="0" cellspacing="1" cellpadding="0" bgcolor="#e2e2e2" class="backfont">
  <tr bgcolor="#f7f7f7">
    <td width="5%" height="28" align="center"><input type="checkbox" onclick="CheckAll(this.form)" name="chkAll" value="checkbox" /></td>
    <td width="15%" align="center">商品标题</td>
    <td width="25%" align="center">评价内容</td>
    <td align="center">会员手机号</td>
    <td align="center">提交时间</td>
    <td width="8%" align="center">是否审核</td>
    </tr>
<?php if(is_array($datalist)): $i = 0; $__LIST__ = $datalist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr bgcolor="#FFFFFF" onmouseover="this.style.backgroundColor='#f7f7f7'" onmouseout="this.style.background='#FFFFFF'">
    <td height="52" align="center"><input type="checkbox" name="del[]" value="<?php echo ($vo["id"]); ?>" /></td>
    <td align="center"><?php echo ($vo["title"]); ?></td>
    <td align="center"><?php echo ($vo['content']); ?></td>
    <td align="center"><?php echo ($vo["cellphone"]); ?>(<?php echo ($vo['nickname']); ?>)</td>
    <td align="center"><?php echo ($vo['times']); ?></td>
    <td align="center">
    <?php if($vo['audit'] == '1'): ?><font color="#0000FF">已审核</font>
    <?php else: ?>
    	<font color="#FF0000">未审核</font><?php endif; ?>
    </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
  <tr bgcolor="#FFFFFF">
    <td height="52" colspan="8" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="delbtn" id="delbtn" class="delbtn" value="删除" onclick="return confirm('确认要删除吗？')" />&nbsp;&nbsp;<input type="button" name="button4" id="button4" class="orderbtn" value="审核通过" onClick="this.form.action='/fantastic goods/server/hadmin.php/Home/Reviews/audit';this.form.submit();" onFocus="this.blur()" />&nbsp;&nbsp;<input type="button" name="button4" id="button4" class="orderbtn" value="撤销审核" onClick="this.form.action='/fantastic goods/server/hadmin.php/Home/Reviews/unaudit';this.form.submit();" onFocus="this.blur()" /></td>
    </tr>
</table>
</form>
</div>
<div id="pagediv">
    <?php echo ($getpage); ?>
</div>
</div>
</body>
</html>