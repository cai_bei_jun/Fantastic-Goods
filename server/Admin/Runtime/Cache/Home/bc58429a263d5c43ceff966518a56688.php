<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="/ds/server/Public/admin/css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function check(){
	if (document.addform.title.value.match(/^\s*$/)){
		alert ("请输入链接名称");
		document.addform.title.focus();
		return false;
	}
	if (document.addform.webs.value.match(/^\s*$/)){
		alert ("请输入链接地址");
		document.addform.webs.focus();
		return false;
	}
}
</script>
</head>

<body>
<div id="spacemenu">
	<div id="leftarrow"></div>
    <div id="return1"><a href="/ds/server/hadmin.php/Home/Tpl">返回上一级</a></div>
</div>
<div class="alterdiv"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" align="center" valign="top">
    	<div id="leftdiv">
        	<div id="titlediv" class="linkblue">修改模板</div>
            <div id="formdiv">
            <form name="addform" id="addform" method="post" action="/ds/server/hadmin.php/Home/Tpl/mod?id=<?php echo ($id); ?>" onsubmit="return check()">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="green" style="margin-top:40px;">
  <tr>
    <td width="33%" height="40" align="right">功能选择：</td>
    <td width="67%">
      <select name="fun" id="fun">
          <option value="ma" <?php if($tpl["fun"] == 'ma'): ?>selected="selected"<?php endif; ?>>多篇</option>
          <option value="sa" <?php if($tpl["fun"] == 'sa'): ?>selected="selected"<?php endif; ?>>单篇</option>
          <option value="pro" <?php if($tpl["fun"] == 'pro'): ?>selected="selected"<?php endif; ?>>产品</option>
          <option value="lnk" <?php if($tpl["fun"] == 'lnk'): ?>selected="selected"<?php endif; ?>>链接</option>
          <option value="card" <?php if($tpl["fun"] == 'card'): ?>selected="selected"<?php endif; ?>>选项卡</option>
          <option value="pics" <?php if($tpl["fun"] == 'pics'): ?>selected="selected"<?php endif; ?>>图库</option>
          <option value="vo" <?php if($tpl["fun"] == 'vo'): ?>selected="selected"<?php endif; ?>>视频</option>
          <option value="down" <?php if($tpl["fun"] == 'down'): ?>selected="selected"<?php endif; ?>>下载</option>
        </select></td>
  </tr>
  <tr>
    <td width="33%" height="40" align="right">模板名称：</td>
    <td width="67%"><input type="text" name="title" id="title" class="htinputcss" value="<?php echo ($tpl["title"]); ?>" /></td>
  </tr>
  <tr>
    <td height="40">&nbsp;</td>
    <td><input type="submit" name="button" id="button" value="修改" class="addbtn" />&nbsp;&nbsp;
      <input type="reset" name="button2" id="button2" value="重置" class="resbtn" /></td>
  </tr>
</table>
</form>
            </div>
        </div>
    </td>
    
  </tr>
</table>
</body>
</html>