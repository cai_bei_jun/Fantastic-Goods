<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="/ds/server/Public/admin/css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function CheckAll(form)
{
  for (var i=0;i<form.elements.length;i++)
    {
    var e = form.elements[i];
    if (e.Name != "chkAll"&&e.disabled!=true)
       e.checked = form.chkAll.checked;
    }
}
function check(){
	if (document.addform.title.value.match(/^\s*$/)){
		alert ("请输入模板名称");
		document.addform.title.focus();
		return false;
	}
}
</script>
</head>

<body>
<div id="spacemenu"></div>
<div class="alterdiv"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="38%" align="center" valign="top">
    	<div id="leftdiv">
        	<div id="titlediv">添加模板</div>
            <div id="formdiv">
            <form name="addform" id="addform" method="post" action="/ds/server/hadmin.php/Home/Tpl/add" onsubmit="return check()">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="green" style="margin-top:40px;">
  <tr>
    <td width="33%" height="40" align="right">功能选择：</td>
    <td width="67%">
      <select name="fun" id="fun">
          <option value="ma">多篇</option>
          <option value="sa">单篇</option>
          <option value="pro">产品</option>
          <option value="lnk">链接</option>
          <option value="card">选项卡</option>
          <option value="pics">图库</option>
          <option value="vo">视频</option>
          <option value="down">下载</option>
          <option value="inte">积分</option>
        </select></td>
  </tr>
  <tr>
    <td width="33%" height="40" align="right">模板名称：</td>
    <td width="67%"><input type="text" name="title" id="title" class="htinputcss" /></td>
  </tr>
  <tr>
    <td height="40">&nbsp;</td>
    <td><input type="submit" name="button" id="button" value="提交" class="addbtn" />&nbsp;&nbsp;
      <input type="reset" name="button2" id="button2" value="重置" class="resbtn" /></td>
  </tr>
</table>
</form>
            </div>
        </div>
    </td>
    <td width="62%" valign="top">
    	<div id="ldiv_search"></div>
        <div id="lnk_listdiv">
        <form name="form1" id="form1" method="post" action="/ds/server/hadmin.php/Home/Tpl/del">
        	<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="#e2e2e2" class="backfont">
  <tr bgcolor="#f7f7f7">
    <td width="10%" height="28" align="center"><input type="checkbox" onclick="CheckAll(this.form)" name="chkAll" value="checkbox" /></td>
    <td width="16%" align="center">ID</td>
    <td width="38%" align="center">模板名称</td>
    <td width="15%" align="center">所属功能</td>
    <td width="21%" align="center">管理</td>
  </tr>
  <?php if(is_array($tpl)): $i = 0; $__LIST__ = $tpl;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr bgcolor="#FFFFFF" onmouseover="this.style.backgroundColor='#f7f7f7'" onmouseout="this.style.background='#FFFFFF'">
    <td height="52" align="center"><input type="checkbox" name="del[]" value="<?php echo ($vo["id"]); ?>" /></td>
    <td align="center"><?php echo ($vo["id"]); ?></td>
    <td align="center"><?php echo ($vo["title"]); ?></td>
    <td align="center">
    <?php if($vo["fun"] == 'ma'): ?>多篇
    <?php elseif($vo["fun"] == 'sa'): ?>
    	单篇
    <?php elseif($vo["fun"] == 'pro'): ?>
    	产品
    <?php elseif($vo["fun"] == 'lnk'): ?>
    	链接
    <?php elseif($vo["fun"] == 'card'): ?>
    	选项卡
    <?php elseif($vo["fun"] == 'pics'): ?>
    	图库
    <?php elseif($vo["fun"] == 'vo'): ?>
    	视频
    <?php elseif($vo["fun"] == 'down'): ?>
    	下载<?php endif; ?>
    </td>
    <td align="center"><a href="/ds/server/hadmin.php/Home/Tpl/edit?id=<?php echo ($vo["id"]); ?>" class="edit">修改</a></td>
  </tr><?php endforeach; endif; else: echo "" ;endif; ?>
  <tr bgcolor="#FFFFFF">
    <td height="52" colspan="5" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="delbtn" id="delbtn" class="delbtn" value="删除" onclick="return confirm('确认要删除吗？')" /></td>
    </tr>
</table>
</form>
        </div>
    </td>
  </tr>
</table>
</body>
</html>