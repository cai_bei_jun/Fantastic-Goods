<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>后台管理系统</title>
    <link href="/fantastic goods/server/Public/admin/css/defaults.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/layui/css/layui.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/css/xadmin.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/css/admin.css" rel="stylesheet" type="text/css" />
    <script src="/fantastic goods/server/Public/admin/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/defaults.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/xadmin.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.form.js" ></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/formfiles.js" ></script>
</head>

<body>
<div id="header">
	<div id="logo">Fantastic Goods后台管理系统</div>
    <div id="rdiv">
    	<ul class="ulnone">
        	<li class="date"><?php echo ($nowdate); ?> <?php echo ($weekarray); ?> <?php echo ($nowtime); ?> </li>
            <li class="funimg"><a href="javascript:void(0)" onclick="javascript:window.top.frames['main'].document.location.reload();"><img src="/fantastic goods/server/Public/admin/images/refresh.png" title="刷新" border="0" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="javascript:;" onclick="window.open('http://localhost:8081/','_blank','width=375,height=667,top=50,left=230,scrollbars=yes')"><img src="/fantastic goods/server/Public/admin/images/function.png" title="商城" border="0" /></a></li>
        </ul>
        <div id="menu">
            <div id="leftmenu"><img src="/fantastic goods/server/Public/admin/images/menu.png" border="0" />
                <div id="downmenu">
                    <div id="downmenu1"></div>
                    <div id="downmenu2">
                        <div id="showadmin">
                            <div id="adminimg"><img src="/fantastic goods/server/Public/admin/images/myico.png" width="29" height="30" /></div>
                            <div id="name"><?php echo ($adminname); ?></div>
                        </div>
                        <div id="menucon">
                            <ul class="ulnone">
                                <li><a href="/fantastic goods/server/hadmin.php/Home/Default/outlogin" target="_top">●&nbsp;安全退出</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="downmenu3"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="left-nav">
    <div id="side-nav">
        <ul class="layui-nav layui-nav-tree layui-nav-side">
            <li class="layui-nav-item"><a href="/fantastic goods/server/hadmin.php/Home/Default/index">
                <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/19.png"></i>
                首页</a></li>
            <li class="layui-nav-item"><a href="/fantastic goods/server/hadmin.php/Home/User/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/18.png"></i>
                    会员管理</a>
            </li>
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/4.png"></i>分类管理</a>
                <dl class="layui-nav-child">
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/ColumnManage/index">栏目管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/ColumnAdd/index">分类增加</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/GoodsList/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/14.png"></i>
                    商品管理</a>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/Order/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/12.png"></i>
                    订单管理</a>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/Reviews/manage">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/26.png"></i>
                    评价管理</a>
            </li>
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">
                    <i class="iconfont" ><img src="/fantastic goods/server/Public/admin/ico/22.png"></i>
                    其他功能</a>
                <dl class="layui-nav-child">
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/Fav/index">收藏管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/AdManage/index">banner管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/Address/index">收货地址管理</a></dd>
                </dl>
            </li>
        </ul>
    </div>
</div>
<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;
    });
</script>

<script language="javascript" type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#mainbox").height( jQuery(document).height());
	jQuery("#leftbox").height( jQuery("#mainbox").height() );

		jQuery("#dragbutton").click(function(){
		if( jQuery(this).attr("src") == "/fantastic goods/server/Public/admin/images/arrow_left.gif" ){
			jQuery("#leftbox").css({width:"0px"}).hide();
			jQuery(this).attr("src","/fantastic goods/server/Public/admin/images/arrow_right.gif");
			jQuery("#mainbox").css({width:"100%"});
		}else{
			jQuery("#leftbox").css({width:"160px"}).show();
			jQuery(this).attr("src","/fantastic goods/server/Public/admin/images/arrow_left.gif");
			jQuery("#mainbox").css({width:"auto"});
		}
	});

});
</script>

<div class="page-content">
<table width="100%"   border="0" cellpadding="0" cellspacing="0">
    <tr>
    	<td style="width:164px; height:100%; background: #c3efcc;" valign="top" id="leftbox">
        <!--左侧菜单-->
        <iframe name="leftframe" id="leftframe" frameborder="0" width="100%" height="100%" scrolling="auto" style="overflow:visible;" src="/fantastic goods/server/hadmin.php/Home/User/left"></iframe>
        <!--左侧菜单结束-->        </td>
        <td class="spacestyle" valign="middle" id="dragbox"><img src="/fantastic goods/server/Public/admin/images/arrow_left.gif" width="10" height="30" border="0" id="dragbutton" style="cursor:pointer;" /></td>
        <td id="mainbox" valign="top">
        <!--内容主窗口-->
        <iframe name="mainframe" id="mainframe" frameborder="0" width="95%" height="100%" scrolling="auto" style="overflow:visible;" src="/fantastic goods/server/hadmin.php/Home/User/manage"></iframe></td>
    </tr>
</table>
</div>
</body>
</html>