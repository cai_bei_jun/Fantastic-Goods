<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="/ds/server/Public/admin/css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/ds/server/Public/admin/js/jquery.js" ></script>
<script type="text/javascript" src="/ds/server/Public/admin/js/jquery.form.js" ></script>
<script type="text/javascript" src="/ds/server/Public/admin/js/formfiles.js" ></script>
<script type="text/javascript">
function check(){
	if (document.form1.gid.value.match(/^\s*$/)){
		alert ("请选择图片组");
		document.form1.gid.focus();
		return false;
	}
	if (document.form1.title.value.match(/^\s*$/)){
		alert ("请输入标题");
		document.form1.title.focus();
		return false;
	}
}
</script>
</head>

<body>
<div id="spacemenu">
	<div id="leftarrow"></div>
    <div id="return1"><a href="/ds/server/hadmin.php/Home/AdManage/manage">返回上一级</a></div>
</div>
<div class="alterdiv"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" align="center" valign="top">
    	<div id="leftdiv">
        	<div id="titlediv" class="linkblue">修改图片</div>
            <div id="formdiv">
            <form name="form1" id="form1" method="post" action="/ds/server/hadmin.php/Home/AdManage/mod?id=<?php echo ($id); ?>" onsubmit="return check()">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="green" style="margin-top:40px;">
  <tr>
    <td width="33%" height="40" align="right">图片组：</td>
    <td width="67%"><select name="gid" id="gid">
      <option value="<?php echo ($data["gid"]); ?>" ><?php echo ($data["gtitle"]); ?></option>
      <?php if(is_array($gdata)): $i = 0; $__LIST__ = $gdata;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>" ><?php echo ($vo["title"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
      </select></td>
  </tr>
  <tr>
      <td height="40" align="right">标题：</td>
      <td width="67%"><input type="text" name="title" id="title" class="htinputcss" value="<?php echo ($data["title"]); ?>" /></td>
  </tr>
  <tr>
  <td height="40" align="right">图片上传：</td>
  <td><input id="photo" class="inputleft htinputcss" name="photo" value="<?php echo ($data["photo"]); ?>" type="text"><a class="files" href="javascript:void(0);"><input id="FileUpload" onchange="SingleUpload('photo','FileUpload','images','/ds/server/hadmin.php/Home/AdManage/upload')" name="FileUpload" type="file"></a><span class="uploading">正在上传，请稍候...</span></td>
  </tr>
  <tr>
    <td height="40">&nbsp;</td>
    <td><input type="submit" name="button" id="button" value="修改" class="addbtn" />&nbsp;&nbsp;
      <input type="reset" name="button2" id="button2" value="重置" class="resbtn" /></td>
  </tr>
</table>
</form>
            </div>
        </div>
    </td>

  </tr>
</table>
</body>
</html>