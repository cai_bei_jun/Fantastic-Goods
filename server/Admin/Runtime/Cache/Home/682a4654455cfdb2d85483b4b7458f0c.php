<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="/fantastic goods/server/Public/admin/css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.js"></script>
<script type="text/javascript">
function CheckAll(form)
{
  for (var i=0;i<form.elements.length;i++)
    {
    var e = form.elements[i];
    if (e.Name != "chkAll"&&e.disabled!=true)
       e.checked = form.chkAll.checked;
    }
}
var count=1;
$(function(){
	var ftype=$("#ftype");
	var show=$("#show");
	ftype.change(function(){
		if(ftype.val()=='checkbox' || ftype.val()=='select'){
			show.html("<input name='val[]' type='text' class='inputcss' />&nbsp;<font color='#FF0000'>*</font>&nbsp;<input name='btn' id='btn' type='button' onclick='clickbtn()' value='添加' />");
		}else if(ftype.val()=='texts'){
            show.html("文本不需要值");
        }
		$("#count").val(count);
	});
});

//点击添加按钮
function clickbtn(){
	count++;
	$("#show").append("<div class='btndiv'><input name='val[]' type='text' class='inputcss' />&nbsp;<font color='#FF0000'>*</font>&nbsp;<input name='dbtn'  type='button' onclick='delbtn(this)' value='删除' class='dbtn' /></div>");
	$("#count").val(count);
}

//删除表单
function delbtn(self){
	$(self).parent(".btndiv").remove();
	count--
	$("#count").val(count);
}

function check(){
	if (document.form1.attr.value.match(/^\s*$/)){
		alert ("请输入属性");
		document.form1.attr.focus();
		return false;
	}
}

</script>
</head>

<body>

<div class="alterdiv"></div>
<div class="positiontitle gray">您当前的位置：栏目管理 &gt;&gt; <?php echo ($cname); ?> &gt;&gt; <?php if($type == '0'): ?>属性管理<?php else: ?>规格管理<?php endif; ?></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="38%" align="center" valign="top">
    	<div id="leftdiv">
        	<div id="titlediv">添加<?php if($type == '0'): ?>属性<?php else: ?>规格<?php endif; ?></div>
            <div id="formdiv">
            <form name="form1" id="form1" method="post" action="/fantastic goods/server/hadmin.php/Home/GoodsList/attr?id=<?php echo ($id); ?>&action=add&type=<?php echo ($type); ?>" onsubmit="return check()">
            <input type="hidden" name="count" id="count" value="0" />
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="green" style="margin-top:40px;">
  <tr>
    <td width="33%" height="40" align="right"><font color="#FF0000">*</font> <?php if($type == '0'): ?>属性<?php else: ?>规格<?php endif; ?>名称：</td>
    <td width="67%"><input type="text" name="attr" id="attr" class="inputcss" /></td>
  </tr>
  <tr>
    <td height="40" align="right">赋值类型：</td>
    <td><select name="ftype" id="ftype">
        <option value="select">列表</option>
        <option value="checkbox">多选</option>
        <option value="texts">批量文本</option>
      </select></td>
  </tr>
    <tr>
        <td height="40" align="right">值：</td>
        <td>
            <div id="show" class="gray">
                <input name='val[]' type='text' class='inputcss' />&nbsp;<font color='#FF0000'>*</font>&nbsp;<input name='btn' id='btn' type='button' onclick='clickbtn()' value='添加' />
            </div>
        </td>
    </tr>
  <tr>
    <td height="40">&nbsp;</td>
    <td><input type="submit" name="button" id="button" value="提交" class="addbtn" />&nbsp;&nbsp;
      <input type="reset" name="button2" id="button2" value="重置" class="resbtn" /></td>
  </tr>
</table>
</form>
            </div>
        </div>
    </td>
    <td width="62%" valign="top">
        <div id="lnk_listdiv" style="margin-top:30px;">
        <form name="form2" id="form2" method="post" action="/fantastic goods/server/hadmin.php/Home/GoodsList/attr?id=<?php echo ($id); ?>&action=del&type=<?php echo ($type); ?>">
        	<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="#e2e2e2" class="backfont">
  <tr bgcolor="#f7f7f7">
    <td width="10%" height="28" align="center"><input type="checkbox" onclick="CheckAll(this.form)" name="chkAll" value="checkbox" /></td>
    <td width="48%" align="center"><?php if($type == '0'): ?>属性<?php else: ?>规格<?php endif; ?>名称</td>
    <td width="30%" align="center">管理</td>
  </tr>
<?php if(is_array($datalist)): $i = 0; $__LIST__ = $datalist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr bgcolor="#FFFFFF" onmouseover="this.style.backgroundColor='#f7f7f7'" onmouseout="this.style.background='#FFFFFF'">
    <td height="52" align="center"><input type="checkbox" name="del[]" value="<?php echo ($vo[id]); ?>" <?php if($vo[id] == 913 || $vo[id] == 928 || $vo[id] == 932): ?>disabled="disabled"<?php endif; ?> /></td>
    <td align="center"><?php echo ($vo[name]); ?></td>
    <td align="center"><a href="/fantastic goods/server/hadmin.php/Home/GoodsList/editattr?cid=<?php echo ($id); ?>&id=<?php echo ($vo[id]); ?>&type=<?php echo ($type); ?>" class="edit">修改</a></td>
  </tr><?php endforeach; endif; else: echo "" ;endif; ?>
  <tr bgcolor="#FFFFFF">
    <td height="52" colspan="4" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="delbtn" id="delbtn" class="delbtn" value="删除" onclick="return confirm('确认要删除吗？')" />&nbsp;&nbsp;
    </tr>
</table>
</form>
        </div>
        <div id="pagediv">
        	<?php echo ($getpage); ?>
        </div>
    </td>
  </tr>
</table>
</body>
</html>