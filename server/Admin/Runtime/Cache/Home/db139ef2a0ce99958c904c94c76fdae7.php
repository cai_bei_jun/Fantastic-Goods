<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>后台管理系统</title>
    <link href="/fantastic goods/server/Public/admin/css/defaults.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/layui/css/layui.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/css/xadmin.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/css/admin.css" rel="stylesheet" type="text/css" />
    <script src="/fantastic goods/server/Public/admin/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/defaults.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/xadmin.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.form.js" ></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/formfiles.js" ></script>
</head>

<body>
<div id="header">
	<div id="logo">Fantastic Goods后台管理系统</div>
    <div id="rdiv">
    	<ul class="ulnone">
        	<li class="date"><?php echo ($nowdate); ?> <?php echo ($weekarray); ?> <?php echo ($nowtime); ?> </li>
            <li class="funimg"><a href="javascript:void(0)" onclick="javascript:window.top.frames['main'].document.location.reload();"><img src="/fantastic goods/server/Public/admin/images/refresh.png" title="刷新" border="0" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="javascript:;" onclick="window.open('http://localhost:8081/','_blank','width=375,height=667,top=50,left=230,scrollbars=yes')"><img src="/fantastic goods/server/Public/admin/images/function.png" title="商城" border="0" /></a></li>
        </ul>
        <div id="menu">
            <div id="leftmenu"><img src="/fantastic goods/server/Public/admin/images/menu.png" border="0" />
                <div id="downmenu">
                    <div id="downmenu1"></div>
                    <div id="downmenu2">
                        <div id="showadmin">
                            <div id="adminimg"><img src="/fantastic goods/server/Public/admin/images/myico.png" width="29" height="30" /></div>
                            <div id="name"><?php echo ($adminname); ?></div>
                        </div>
                        <div id="menucon">
                            <ul class="ulnone">
                                <li><a href="/fantastic goods/server/hadmin.php/Home/Default/outlogin" target="_top">●&nbsp;安全退出</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="downmenu3"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="left-nav">
    <div id="side-nav">
        <ul class="layui-nav layui-nav-tree layui-nav-side">
            <li class="layui-nav-item"><a href="/fantastic goods/server/hadmin.php/Home/Default/index">
                <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/19.png"></i>
                首页</a></li>
            <li class="layui-nav-item"><a href="/fantastic goods/server/hadmin.php/Home/User/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/18.png"></i>
                    会员管理</a>
            </li>
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/4.png"></i>分类管理</a>
                <dl class="layui-nav-child">
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/ColumnManage/index">栏目管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/ColumnAdd/index">分类增加</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/GoodsList/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/14.png"></i>
                    商品管理</a>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/Order/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/12.png"></i>
                    订单管理</a>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/Reviews/manage">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/26.png"></i>
                    评价管理</a>
            </li>
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">
                    <i class="iconfont" ><img src="/fantastic goods/server/Public/admin/ico/22.png"></i>
                    其他功能</a>
                <dl class="layui-nav-child">
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/Fav/index">收藏管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/AdManage/index">banner管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/Address/index">收货地址管理</a></dd>
                </dl>
            </li>
        </ul>
    </div>
</div>
<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;
    });
</script>

<div class="page-content">
<div id="spacemenu">
	<div id="leftarrow"></div>
    <div id="return1"><a href="/fantastic goods/server/hadmin.php/Home/Order?page=<?php echo ($page); ?>&kwords=<?php echo ($kwords); ?>">返回上一级</a></div>
</div>
<div class="alterdiv"></div>
<div id="posdiv">
	<table width="94%" border="0" cellspacing="0" cellpadding="0" class="gray" align="center">
  <tr>
    <td height="46">您当前的位置：订单详情</td>
  </tr>
</table>
</div>
<div id="maindiv">
<table width="95%" border="0" cellspacing="0" cellpadding="0" class="green">
<tr>
    <td width="36%" height="40" align="right">订单编号：</td>
    <td width="64%" class="gray"><?php echo ($odata['ordernum']); ?></td>
  </tr>
<tr>
    <td width="36%" height="40" align="right">订购时间：</td>
    <td width="64%" class="gray"><?php echo ($odata['times']); ?></td>
  </tr>
  <tr>
  <td height="40" align="right">订单状态：</td>
  <td width="64%" class="gray">
  <?php if($odata['status'] == '0'): ?>待付款
<?php elseif($odata['status'] == '1'): ?>
    <font color="#009966">已付款</font>
<?php elseif($odata['status'] == '-1'): ?>
    <font color="#FF0000">取消订单</font>
<?php elseif($odata['status'] == '2'): ?>
    	<font color="#FF00FF">确认收货</font><?php endif; ?>
</if>
  </td>
  </tr>
  <tr>
  <td height="40" align="right">是否评价：</td>
  <td width="64%" class="gray">
  <?php if($odata['iscomm'] == '1'): ?><font color="#0000FF">已评价</font>
<?php else: ?>
    <font color="#FF0000">未评价</font><?php endif; ?>
  </td>
  </tr>
  <tr>
    <td width="36%" height="40" align="right">会员手机号：</td>
    <td width="64%" class="gray"><?php echo ($udata['cellphone']); ?></td>
  </tr>
  <tr>
  <td height="40" align="right">收货人：</td>
  <td class="gray"><?php echo ($addsData['name']); ?></td>
  </tr>
  <tr>
  <td height="40" align="right">收货人电话：</td>
  <td class="gray"><?php echo ($addsData['cellphone']); ?></td>
  </tr>
    <tr>
        <td height="40" align="right">收货地址：</td>
        <td class="gray"><?php echo ($addsData['province']); echo ($addsData["city"]); echo ($addsData["area"]); echo ($addsData["address"]); ?></td>
    </tr>
  <?php if(is_array($gdata)): $i = 0; $__LIST__ = $gdata;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
  <td height="40" align="right">商品名称：</td>
  <td class="gray"><?php echo ($vo['title']); ?></td>
  </tr>
  <tr>
  <td height="40" align="right">价格：</td>
  <td class="gray"><?php echo ($vo['price']); ?>元</td>
  </tr>
  <tr>
  <td height="40" align="right">数量：</td>
  <td class="gray"><?php echo ($vo['amount']); ?></td>
  </tr><?php endforeach; endif; else: echo "" ;endif; ?>
  <tr>
  <tr>
    <td width="36%" height="40" align="right">运费：</td>
    <td width="64%" class="gray"><?php echo ($odata['freight']); ?></td>
  </tr>
  <td height="40" align="right">总价：</td>
  <td class="gray"><?php echo ($total); ?>元</td>
  </tr>
</table>
</div>
</div>
</body>
</html>