<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>后台管理系统</title>
    <link href="/fantastic goods/server/Public/admin/css/defaults.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/layui/css/layui.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/css/xadmin.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/css/admin.css" rel="stylesheet" type="text/css" />
    <script src="/fantastic goods/server/Public/admin/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/defaults.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/xadmin.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.form.js" ></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/formfiles.js" ></script>
</head>

<body>
<div id="header">
	<div id="logo">Fantastic Goods后台管理系统</div>
    <div id="rdiv">
    	<ul class="ulnone">
        	<li class="date"><?php echo ($nowdate); ?> <?php echo ($weekarray); ?> <?php echo ($nowtime); ?> </li>
            <li class="funimg"><a href="javascript:void(0)" onclick="javascript:window.top.frames['main'].document.location.reload();"><img src="/fantastic goods/server/Public/admin/images/refresh.png" title="刷新" border="0" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="javascript:;" onclick="window.open('http://localhost:8081/','_blank','width=375,height=667,top=50,left=230,scrollbars=yes')"><img src="/fantastic goods/server/Public/admin/images/function.png" title="商城" border="0" /></a></li>
        </ul>
        <div id="menu">
            <div id="leftmenu"><img src="/fantastic goods/server/Public/admin/images/menu.png" border="0" />
                <div id="downmenu">
                    <div id="downmenu1"></div>
                    <div id="downmenu2">
                        <div id="showadmin">
                            <div id="adminimg"><img src="/fantastic goods/server/Public/admin/images/myico.png" width="29" height="30" /></div>
                            <div id="name"><?php echo ($adminname); ?></div>
                        </div>
                        <div id="menucon">
                            <ul class="ulnone">
                                <li><a href="/fantastic goods/server/hadmin.php/Home/Default/outlogin" target="_top">●&nbsp;安全退出</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="downmenu3"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="left-nav">
    <div id="side-nav">
        <ul class="layui-nav layui-nav-tree layui-nav-side">
            <li class="layui-nav-item"><a href="/fantastic goods/server/hadmin.php/Home/Default/index">
                <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/19.png"></i>
                首页</a></li>
            <li class="layui-nav-item"><a href="/fantastic goods/server/hadmin.php/Home/User/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/18.png"></i>
                    会员管理</a>
            </li>
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/4.png"></i>分类管理</a>
                <dl class="layui-nav-child">
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/ColumnManage/index">栏目管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/ColumnAdd/index">分类增加</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/GoodsList/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/14.png"></i>
                    商品管理</a>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/Order/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/12.png"></i>
                    订单管理</a>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/Reviews/manage">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/26.png"></i>
                    评价管理</a>
            </li>
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">
                    <i class="iconfont" ><img src="/fantastic goods/server/Public/admin/ico/22.png"></i>
                    其他功能</a>
                <dl class="layui-nav-child">
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/Fav/index">收藏管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/AdManage/index">banner管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/Address/index">收货地址管理</a></dd>
                </dl>
            </li>
        </ul>
    </div>
</div>
<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;
    });
</script>

<script type="text/javascript">
function CheckAll(form)
{
  for (var i=0;i<form.elements.length;i++)
    {
    var e = form.elements[i];
    if (e.Name != "chkAll"&&e.disabled!=true)
       e.checked = form.chkAll.checked;
    }
}
</script>
<div class="page-content">
<div id="spacemenu"></div>
<div class="alterdiv"></div>
<div id="maindiv">
<form id="form1" name="form1" method="post"  action="/fantastic goods/server/hadmin.php/Home/Address/del">
    <div id="navdiv">
        <table width="94%" border="0" cellspacing="0" cellpadding="0" class="gray" align="center">
  <tr>
    <td height="41" class="linkblue">&nbsp;&nbsp;&nbsp;您当前的位置：收货地址</td>
    <td width="59%" class="linkblue"></td>
    </tr>
</table>
    </div>

<table width="95%" border="0" cellspacing="1" cellpadding="0" bgcolor="#e2e2e2" class="backfont">
  <tr bgcolor="#f7f7f7">
    <td height="28" align="center"><input type="checkbox" onclick="CheckAll(this.form)" name="chkAll" value="checkbox" /></td>
      <td  align="center">会员手机号</td>
    <td align="center">收货人姓名</td>
    <td align="center">收货人手机号</td>
    <td align="center">所在地区</td>
      <td width="30%" align="center">详细地址</td>
      <td align="center">是否为默认地址</td>
    </tr>
<?php if(is_array($datalist)): $i = 0; $__LIST__ = $datalist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr bgcolor="#FFFFFF" onmouseover="this.style.backgroundColor='#f7f7f7'" onmouseout="this.style.background='#FFFFFF'">
    <td height="52" align="center"><input type="checkbox" name="del[]" value="<?php echo ($vo["id"]); ?>" /></td>
      <td align="center"><?php echo ($vo['ucellphone']); ?></td>
    <td align="center"><?php echo ($vo['name']); ?></td>
    <td align="center"><?php echo ($vo['cellphone']); ?></td>
    <td align="center"><?php echo ($vo['province']); ?></td>
      <td align="center"><?php echo ($vo['address']); ?></td>
      <td align="center">
          <?php if($vo['isdefault'] == '1'): ?>是
          <?php else: ?>
              否<?php endif; ?>
      </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
  <tr bgcolor="#FFFFFF">
    <td height="52" colspan="7" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="delbtn" id="delbtn" class="delbtn" value="删除" onclick="return confirm('确认要删除吗？')" />&nbsp;&nbsp;</td>
    </tr>
</table>
</form>
</div>
<div id="pagediv">
    <?php echo ($getpage); ?>
</div>
</div>
</body>
</html>