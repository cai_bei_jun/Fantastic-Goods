<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>后台管理系统</title>
    <link href="/fantastic goods/server/Public/admin/css/defaults.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/layui/css/layui.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/css/xadmin.css" rel="stylesheet" type="text/css" />
    <link href="/fantastic goods/server/Public/admin/css/admin.css" rel="stylesheet" type="text/css" />
    <script src="/fantastic goods/server/Public/admin/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/defaults.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/xadmin.js"></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/jquery.form.js" ></script>
    <script type="text/javascript" src="/fantastic goods/server/Public/admin/js/formfiles.js" ></script>
</head>

<body>
<div id="header">
	<div id="logo">Fantastic Goods后台管理系统</div>
    <div id="rdiv">
    	<ul class="ulnone">
        	<li class="date"><?php echo ($nowdate); ?> <?php echo ($weekarray); ?> <?php echo ($nowtime); ?> </li>
            <li class="funimg"><a href="javascript:void(0)" onclick="javascript:window.top.frames['main'].document.location.reload();"><img src="/fantastic goods/server/Public/admin/images/refresh.png" title="刷新" border="0" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="javascript:;" onclick="window.open('http://localhost:8081/','_blank','width=375,height=667,top=50,left=230,scrollbars=yes')"><img src="/fantastic goods/server/Public/admin/images/function.png" title="商城" border="0" /></a></li>
        </ul>
        <div id="menu">
            <div id="leftmenu"><img src="/fantastic goods/server/Public/admin/images/menu.png" border="0" />
                <div id="downmenu">
                    <div id="downmenu1"></div>
                    <div id="downmenu2">
                        <div id="showadmin">
                            <div id="adminimg"><img src="/fantastic goods/server/Public/admin/images/myico.png" width="29" height="30" /></div>
                            <div id="name"><?php echo ($adminname); ?></div>
                        </div>
                        <div id="menucon">
                            <ul class="ulnone">
                                <li><a href="/fantastic goods/server/hadmin.php/Home/Default/outlogin" target="_top">●&nbsp;安全退出</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="downmenu3"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="left-nav">
    <div id="side-nav">
        <ul class="layui-nav layui-nav-tree layui-nav-side">
            <li class="layui-nav-item"><a href="/fantastic goods/server/hadmin.php/Home/Default/index">
                <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/19.png"></i>
                首页</a></li>
            <li class="layui-nav-item"><a href="/fantastic goods/server/hadmin.php/Home/User/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/18.png"></i>
                    会员管理</a>
            </li>
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/4.png"></i>分类管理</a>
                <dl class="layui-nav-child">
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/ColumnManage/index">栏目管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/ColumnAdd/index">分类增加</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/GoodsList/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/14.png"></i>
                    商品管理</a>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/Order/index">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/12.png"></i>
                    订单管理</a>
            </li>
            <li class="layui-nav-item">
                <a href="/fantastic goods/server/hadmin.php/Home/Reviews/manage">
                    <i class="iconfont"><img src="/fantastic goods/server/Public/admin/ico/26.png"></i>
                    评价管理</a>
            </li>
            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">
                    <i class="iconfont" ><img src="/fantastic goods/server/Public/admin/ico/22.png"></i>
                    其他功能</a>
                <dl class="layui-nav-child">
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/Fav/index">收藏管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/AdManage/index">banner管理</a></dd>
                    <dd><a href="/fantastic goods/server/hadmin.php/Home/Address/index">收货地址管理</a></dd>
                </dl>
            </li>
        </ul>
    </div>
</div>
<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;
    });
</script>

<div class="page-content">
<div id="spacemanage"></div>
<div id="desmenu">
	<div id="movediv">
    	<div id="fold"><a href="javascript:;" id="menu_co">全部折叠</a></div>
        <div id="searchdiv">
        <form id="cform" name="cform" method="post" action="/fantastic goods/server/hadmin.php/Home/ColumnManage/">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gray">
  <tr>
    <td>检索一级目录：<input type="text" name="search" id="search" class="htinputcss" /><input type="button" name="addbtn" id="addbtn" class="ssbtn" value="搜索" onClick="this.form.action='/fantastic goods/server/hadmin.php/Home/ColumnManage';this.form.submit()" onFocus="this.blur()"  /></td>
    <td>&nbsp;</td>
  </tr>
</table>
	</form>
        </div>
        <div id="column" class="gray"><img src="/fantastic goods/server/Public/admin/images/addcon.png" alt="内容管理" />:内容管理&nbsp;&nbsp;<img src="/fantastic goods/server/Public/admin/images/addcol.png" alt="栏目添加" />:栏目添加&nbsp;&nbsp;<img src="/fantastic goods/server/Public/admin/images/editcol.png" alt="栏目修改" />:栏目修改&nbsp;&nbsp;<img src="/fantastic goods/server/Public/admin/images/delcol.png" alt="栏目删除" />:栏目删除&nbsp;</div>
    </div>
</div>
<div id="topmenu">
	<div id="movediv" class="gray">
    	<div style="width:50%;height:100%;float:left;">栏目名称</div>
        <div style="width:20%;height:100%;float:left;">栏目管理</div>
        <div style="width:25%;height:100%;float:left;">栏目排序</div>
        <div style="width:5%;height:100%;float:left;">栏目删除</div>
    </div>
</div>
<div id="colmain" class="backfont">
<?php if($total > 0): ?><div id="showmenu"><?php echo ($menu); ?></div>
<?php else: ?>
    <center>没有找到相关栏目，请输入一级栏目的名称。</center><?php endif; ?>
</div>
</div>
<script type="text/javascript">

//栏目排序
function ajxorder(action,pid,id,num){
	$.get("/fantastic goods/server/hadmin.php/Home/ColumnManage/corder",{action:action,pid:pid,id:id,num:num,date:new Date().getTime()},function(data){
			$("#showmenu").html(data);
	})
}

	$("#menu_co").click(function(){
		if($(".pdiv").attr("pvar")=='1'){
			$(".pdiv").css("display","none").attr("pvar",'0');
			$(this).html('全部展开');
			$(".pdiv").prev('div[onclick]').children(".movediv").children("div").children('img').attr("src","/fantastic goods/server/Public/admin/images/plus.jpg");
			$(".pdiv").prev('div[onclick]').children(".movediv").children("div").children("div").children('img').attr("src","/fantastic goods/server/Public/admin/images/plus.jpg");
			$(".pdiv").prev().removeClass("outerdiv2").addClass("outerdiv");
			$(".pdiv").prev().css("background-color","");
		}else{
			$(".pdiv").css("display","block").attr("pvar",'1');
			$(this).html('全部折叠');
			$(".pdiv").prev('div[onclick]').children(".movediv").children("div").children('img').attr("src","/fantastic goods/server/Public/admin/images/minsign.jpg");
			$(".pdiv").prev('div[onclick]').children(".movediv").children("div").children("div").children('img').attr("src","/fantastic goods/server/Public/admin/images/minsign.jpg");
			$(".pdiv").prev().removeClass("outerdiv").addClass("outerdiv2");

		}
	});

	//鼠标滑过变色
	var oColor=null;
	function overColumn(my){
		oColor=$(my).css("background-color");
		$(my).css("background-color","#e5e4e4");

	}
	function outColumn(my){
		my.style.background=oColor;
		$(my).css("background-color",oColor);
	}

	//点击栏目折叠展开
	function oncard(id){
		var cid=$("#c"+id);
		if(cid.next().is(":visible")){//隐藏
			cid.css("background-color","#FFF");
			cid.next().css("display","none");
			cid.children(".movediv").children("div").children("img").attr('src','/fantastic goods/server/Public/admin/images/plus.jpg');
			cid.children(".movediv").children("div").children("div").children("img").attr('src','/fantastic goods/server/Public/admin/images/plus.jpg');
			cid.removeClass("outerdiv2").addClass("outerdiv");
		}else{//显示
			cid.next().css("display","block");
			cid.children(".movediv").children("div").children("img").attr('src','/fantastic goods/server/Public/admin/images/minsign.jpg');
			cid.children(".movediv").children("div").children("div").children("img").attr('src','/fantastic goods/server/Public/admin/images/minsign.jpg');
			cid.removeClass("outerdiv").addClass("outerdiv2");
		}
	}
</script>
</body>
</html>